package digillence.streettreat.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class ForgotPassword extends  Activity implements View.OnClickListener{
    TextInputLayout tilEmail,tilPassword;
    EditText edtEmail,edtMobile;
    //    private ProgressDialog progress;
    ImageView headerbar_back_toolbar;
    TextView textView,forgotpassimage,txtImgNotification,Headerbartitle,textView2;

    Button btnGetPassword;
    Context context;
    JSONArray jsonchangePassword;
    String mobileNUmber;
    private static Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        init();
        setFont();
        toast = new Toast(this);

        Headerbartitle=(TextView) findViewById(R.id.Headerbartitle);

        headerbar_back_toolbar=(ImageView)findViewById(R.id.headerbar_back_toolbar);

        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(ForgotPassword.this,Login.class);
                startActivity(in);
                finish();
                if (toast != null)
                    toast.cancel();

            }
        });

        Headerbartitle.setVisibility(View.INVISIBLE);

    }


    private void setFont() {
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(),"font/Raleway-Regular.ttf");
        Typeface  font3 = Typeface.createFromAsset(getAssets(),"font/fontello.ttf");
        textView2.setTypeface(font);
        btnGetPassword.setTypeface(font);
        forgotpassimage.setTypeface(font3);
        forgotpassimage.setText("\ue039");
        edtMobile.setTypeface(font2);

    }



    private void init()

    {
        context=this;
        tilEmail=(TextInputLayout) findViewById(R.id.tilEmail);
        tilPassword=(TextInputLayout) findViewById(R.id.tilPassword);
        txtImgNotification = (TextView) findViewById(R.id.txtImgNotification);
        forgotpassimage=(TextView) findViewById(R.id.forgotpassimage);
        edtEmail=(EditText) findViewById(R.id.edtEmail);
        edtMobile=(EditText) findViewById(R.id.edtMobile);
        forgotpassimage=(TextView) findViewById(R.id.forgotpassimage);
        textView2 = (TextView) findViewById(R.id.textView2);
        btnGetPassword=(Button) findViewById(R.id.btnGetPassword);
        btnGetPassword.setOnClickListener(this);

    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void onClick(View v)
    {

        switch (v.getId())
        {
            case R.id.btnGetPassword:

                hideSoftKeyboard(ForgotPassword.this);

                if(edtMobile.getText().toString().trim().length()<1)
                {
                  //  toast.makeText(context,"Please enter mobile",Toast.LENGTH_LONG).show();

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Please enter mobile", Toast.LENGTH_LONG);
                    toast.show();



                }

                else if(edtMobile.getText().toString().trim().length()>0)
                {
                    if(edtMobile.getText().toString().trim().length()==10)
                    {
                        mobileNUmber=  edtMobile.getText().toString().trim();
                        new  forgotPassword().execute();

                    }else
                    {
                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(), "please enter proper mobile", Toast.LENGTH_LONG);
                        toast.show();


                    }
                }
                else {

                    Intent ivertify = new Intent(this, StoreDetailsActivity.class);
                    startActivity(ivertify);
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(ForgotPassword.this,Login.class);
        startActivity(in);
        finish();
        if (toast != null)
            toast.cancel();


    }

    class forgotPassword extends AsyncTask<String, String, JSONArray> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("mobile", mobileNUmber));

            jsonchangePassword = jParser.getJSONFromUrl(Urls.forgotPassword, params);

            return jsonchangePassword;
        }
        protected void onPostExecute(JSONArray jsonchangePassword) {
            try {
                pDialog.dismiss();
                for(int i=0; i<jsonchangePassword.length(); i++)
                {
                    JSONObject obj = jsonchangePassword.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {

                        customDialogue();
                    }
                    else
                    {
                       // toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(),obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();


                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    EditText enterotp;
    TextView resend,vrfyPwd,otpNotRcvd,timmer;

    public void customDialogue() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.verify_pwd_dialogue);
        dialog.setCanceledOnTouchOutside(false);
        Button verify = (Button) dialog.findViewById(R.id.verify);
        final ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
        enterotp = (EditText) dialog.findViewById(R.id.enterotp);
        otpNotRcvd= (TextView) dialog.findViewById(R.id.otpNotRcvd);
        vrfyPwd= (TextView) dialog.findViewById(R.id.vrfyPwd);
        timmer = (TextView) dialog.findViewById(R.id.timmer);


        resend = (TextView) dialog.findViewById(R.id.resend);
        long maxTimeInMilliseconds = 1*60*1000;
        startTimer(maxTimeInMilliseconds, 1000);

        Typeface font = Typeface.createFromAsset(getAssets(),"font/Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(),"font/Raleway-Regular.ttf");

        vrfyPwd.setTypeface(font);
        verify.setTypeface(font);
        enterotp.setTypeface(font2);
        timmer.setTypeface(font2);
        resend.setTypeface(font2);
        otpNotRcvd.setTypeface(font2);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard(ForgotPassword.this);

                if( enterotp.getText().toString().trim().length()<4)
                {
                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(),"please enter proper otp code", Toast.LENGTH_LONG);
                    toast.show();

                }
                else
                {
                    new verifyOtp_().execute();

                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(ForgotPassword.this);
                new  forgotPasswordResend().execute();

            }
        });

        dialog.show();
    }
    CountDownTimer t;
    public void startTimer(final long finish, long tick)
    {
        t = new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                timmer.setText("" + (remainedSecs / 60) + ":" + (remainedSecs % 60));// manage it accordign to you
            }

            public void onFinish() {
                timmer.setText("");
                cancel();
            }
        }.start();
    }

    class forgotPasswordResend extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("mobile", mobileNUmber));
            jsonchangePassword = jParser.getJSONFromUrl(Urls.forgotPassword, params);
            return jsonchangePassword;
        }
        protected void onPostExecute(JSONArray jsonchangePassword) {
            try {
                pDialog.dismiss();
                for(int i=0; i<jsonchangePassword.length(); i++)
                {
                    JSONObject obj = jsonchangePassword.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        //  customDialogue();
                        long maxTimeInMilliseconds = 1*60*1000;
                        t.cancel();
                        startTimer(maxTimeInMilliseconds, 1000);

                    }
                    else
                    {

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(),obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    class verifyOtp_ extends AsyncTask<String, String, JSONArray>
    {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("mobile", mobileNUmber));
            params.add(new BasicNameValuePair("otp_key", enterotp.getText().toString().trim()));

            jsonchangePassword = jParser.getJSONFromUrl(Urls.verifyOTP, params);

            return jsonchangePassword;

        }
        protected void onPostExecute(JSONArray jsonchangePassword) {
            try {

                pDialog.dismiss();
                for(int i=0; i<jsonchangePassword.length(); i++)
                {
                    JSONObject obj = jsonchangePassword.getJSONObject(i);
                    String status = obj.getString("status");

                    if(status.equals("1")) {
                        pDialog.dismiss();
                        SharedPreferences sh_= getSharedPreferences("Token", Context.MODE_PRIVATE);
                        String token_=sh_.getString("Token", "NA");

                        String token = obj.getJSONObject("items").getString("token");
                        SharedPreferences sh = getSharedPreferences("Token", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = sh.edit();
                        edit.putString("Token", token);
                        edit.commit();

                        Intent in = new Intent(ForgotPassword.this, ConfirmPwdActivity.class);
                        startActivity(in);
                    }
                    else
                    {
                       // toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(),obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }


}