package digillence.streettreat.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import digillence.streettreat.R;


public class ActivityReview extends TabActivity {

    private static final String All_SPEC = "All";
    private static final String HAPPY_SPEC = "HAPPY";
    private static final String NOT_SO_HAPPY_SPEC = "NOT SO HAPPY";
    ImageView headerbar_back_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);

        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(ActivityReview.this,TestActivity.class);
                startActivity(in);
            }
        });

        TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);

        TabSpec allvari = tabHost.newTabSpec(All_SPEC);
        Intent ActivityIntent = new Intent(this, All.class);
        allvari.setIndicator("All");
        allvari.setContent(ActivityIntent);
        tabHost.addTab(allvari);

        TabSpec happyvari = tabHost.newTabSpec(HAPPY_SPEC);
        Intent HappyIntent = new Intent(this, Happy.class);
        happyvari.setContent(HappyIntent);
        happyvari.setIndicator("HAPPY");
        tabHost.addTab(happyvari);

        TabSpec NotSoSadVari = tabHost.newTabSpec(NOT_SO_HAPPY_SPEC);
        Intent SadIntent = new Intent(this, NotSoSad.class);
        NotSoSadVari.setContent(SadIntent);
        NotSoSadVari.setIndicator("NOT SO HAPPY");

        tabHost.addTab(NotSoSadVari);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(ActivityReview.this,TestActivity.class);
        startActivity(in);
    }
}
