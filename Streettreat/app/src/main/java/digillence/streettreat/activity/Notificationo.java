package digillence.streettreat.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import digillence.streettreat.R;
import digillence.streettreat.adapter.SendNotiAdapter;
import digillence.streettreat.adapter.SendPrevNotiAdapter;
import digillence.streettreat.model.NotificationsGet;
import digillence.streettreat.model.Notificattion;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class Notificationo extends Activity
{
    ListView lv,lvprevNoti;
    JSONArray jsonNoti;
    SendNotiAdapter _SendNotiAdapter;
    SendPrevNotiAdapter _SendPrevNotiAdapter;
    Button sendNotiBtn;
    JSONArray jsonSendNoti;
    String strDate,logId,templateId,storeName;
    ArrayList<Notificattion> al= new ArrayList<Notificattion>();
    ArrayList<Notificattion> alrefreshed= new ArrayList<Notificattion>();
    Dialog suggestdialog ;
    ImageView headerbar_back_toolbar;


    TextView sgstNwTmplt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        lv= (ListView)findViewById(R.id.lv);
        lvprevNoti= (ListView)findViewById(R.id.lvprevNoti);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(Notificationo.this,Home.class);
                startActivity(in);
            }

        });


        sendNotiBtn=(Button)findViewById(R.id.sendNotiBtn);
        sgstNwTmplt= (TextView)findViewById(R.id.sgstNwTmplt);
        sgstNwTmplt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(Notificationo.this,"Poup Will Invoke",Toast.LENGTH_SHORT).show();

                customDialogue();

               // new NotificationTemplates().execute(); will refresh the upper listview

            }
        });
        SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
         logId= sharedPreferences.getString("log_id", "NA");

        SharedPreferences templateIdPrefAl=getSharedPreferences("TemplateId", Context.MODE_PRIVATE);

        Set<String> set_ = new HashSet<String>();
        set_ = templateIdPrefAl.getStringSet("TemplateId", null);

       // lvprevNoti.setAdapter(_SendNotiAdapter);

        new NotificationTemplates().execute();

        sendNotiBtn.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
             // new  getNotificationsPreviuously().execute();// for refreshing Listview.

                String templateId="";
                SharedPreferences templateIdPrefAl=getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                Set<String> set_ = new HashSet<String>();
                set_ = templateIdPrefAl.getStringSet("TemplateId", null);
               try {
                    templateId = set_.toString().replaceAll("[\\s\\[\\]]", "");
               }catch(NullPointerException ne)
               {}
                if(templateId.equals(""))
                {
                    Toast.makeText(Notificationo.this,"PLease select any template",Toast.LENGTH_LONG).show();
                }
           else {
                    new sendNotification().execute();
                }
            }

        });
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        strDate = sdfDate.format(now);
    }
String enterTemp;

    public void customDialogue()
    {
        final EditText enterTemplt;
        suggestdialog = new Dialog(Notificationo.this);
        suggestdialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        suggestdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        suggestdialog.setContentView(R.layout.suggest_noti_dialogue);
        suggestdialog.setCanceledOnTouchOutside(false);

        final ImageView cancel = (ImageView) suggestdialog.findViewById(R.id.cancel);
        enterTemplt= (EditText)suggestdialog.findViewById(R.id.enterTemplt);

        Button submit = (Button) suggestdialog.findViewById(R.id.verify);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggestdialog.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterTemp=   enterTemplt.getText().toString();
                if (enterTemplt.getText().toString().length() < 1) {
                    Toast.makeText(Notificationo.this, "PLease Enter", Toast.LENGTH_LONG).show();

                }
                else{


                    new suggestNotificationTemplate().execute();
                }

            }
        });

        suggestdialog.show();

    }

    class suggestNotificationTemplate extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {

        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("template", enterTemp));

            jsonNoti = jParser.getJSONFromUrl(Urls.suggestNotificationTemplate, params);

            return jsonNoti;
        }

        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                // pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        suggestdialog.dismiss();


                     // new  RefreshNotificationTemplates().execute();

                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                    else
                    {

                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }



    class RefreshNotificationTemplates extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notificationo.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();



            jsonNoti = jParser.getJSONFromUrl(Urls.notificationTemplates, params);

            return jsonNoti;
        }
        //        ArrayList<Notificattion> al= new ArrayList<Notificattion>();
        //Notificattion noti= new Notificattion();
        String  description,id;
        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                //  pDialog.dismiss();

                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String     storeName= sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName= getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName=sharedPrefUserName.getString("user_name","");

                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Notificattion noti= new Notificattion();
                            description =  objj.getString("description");
                            id= objj.getString("id");
                            noti.setCode(id);


                            String replaced;
                            if(description.contains("{user}") || description.contains("{store}"))
                            {
                                replaced   =  description.replace("{user}",userName);
                                replaced=  replaced.replace("{store}",storeName);
                                noti.setName(replaced);
                            }
                            else if(description.contains("{store}"))
                            {
                                replaced=  description.replace("{store}",storeName);

                                noti.setName(replaced);
                            }

                            else
                            {
                                description=description;
                                noti.setName(description);
                            }
//                            noti.setName(description);
                            alrefreshed.add(noti);

                        }

                    }

                    else
                    {
                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                _SendNotiAdapter = new SendNotiAdapter(Notificationo.this, alrefreshed);
                lv.setAdapter(_SendNotiAdapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    class sendNotification extends AsyncTask<String, String, JSONArray> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notificationo.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            SharedPreferences templateIdPrefAl=getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
            Set<String> set_ = new HashSet<String>();
            set_ = templateIdPrefAl.getStringSet("TemplateId", null);
            String templateId = set_.toString().replaceAll("[\\s\\[\\]]", "");

            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("date_time", strDate));
            params.add(new BasicNameValuePair("templateid", templateId));
            jsonSendNoti = jParser.getJSONFromUrl(Urls.sendNotifi, params);
            return jsonSendNoti;
        }

        protected void onPostExecute(JSONArray jsonSendNoti) {
            try {

                // pDialog.dismiss();
                for(int i=0; i<jsonSendNoti.length(); i++)
                {
                    JSONObject obj = jsonSendNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        _SendNotiAdapter.notifyDataSetChanged();
                        lv.invalidateViews();
                 Toast.makeText(Notificationo.this,"Notification has been sent to favourite customers",Toast.LENGTH_LONG).show();
                    }

                    else
                    {
                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notificationo.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            jsonNoti = jParser.getJSONFromUrl(Urls.notificationTemplates, params);

            return jsonNoti;
        }
//        ArrayList<Notificattion> al= new ArrayList<Notificattion>();
        //Notificattion noti= new Notificattion();
        String  description,id;
        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                pDialog.dismiss();

                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        new getNotificationsPreviuously().execute();

                         SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
                         String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName= getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName=sharedPrefUserName.getString("user_name","");

                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Notificattion noti= new Notificattion();
                            description =  objj.getString("description");
                            id= objj.getString("id");
                            noti.setCode(id);


                            String replaced;
                            if(description.contains("{user}") || description.contains("{store}"))
                            {
                                replaced   =  description.replace("{user}",userName);
                                replaced=  replaced.replace("{store}",storeName);
                                noti.setName(replaced);
                            }
                            else if(description.contains("{store}"))
                            {
                                replaced=  description.replace("{store}",storeName);
                                noti.setName(replaced);
                            }

                            else
                            {
                                description=description;
                                noti.setName(description);
                            }
//                           noti.setName(description);
                            al.add(noti);
                        }

                    }

                    else
                    {
                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                _SendNotiAdapter = new SendNotiAdapter(Notificationo.this, al);
                lv.setAdapter(_SendNotiAdapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException iij)
            {}

        }
    }

    class getNotificationsPreviuously extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notificationo.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
           // pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("orderdir", "DESC"));
            params.add(new BasicNameValuePair("orderby", "a.created_on"));
            params.add(new BasicNameValuePair("limit", "5"));

            jsonNoti = jParser.getJSONFromUrl(Urls.getNotifications, params);

            return jsonNoti;
        }
        String description, id;
        ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();

        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                //  pDialog.dismiss();

                for (int i = 0; i < jsonNoti.length(); i++) {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if (status.equals("1")) {
                        SharedPreferences sharedPrefStoreName = getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName = getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName = sharedPrefUserName.getString("user_name", "");

                        JSONArray js = obj.getJSONArray("items");
                        for (int j = 0; j < js.length(); j++) {

                            JSONObject objj = js.getJSONObject(j);
                            NotificationsGet noti = new NotificationsGet();
                            description = objj.getString("description");
                            id = objj.getString("id");
                            noti.setCode(id);
                            String replaced;
                            if (description.contains("{user}") || description.contains("{store}")) {
                                replaced = description.replace("{user}", userName);
                                replaced = replaced.replace("{store}", storeName);
                                noti.setName(replaced);
                            } else if (description.contains("{store}")) {
                                replaced = description.replace("{store}", storeName);

                                noti.setName(replaced);
                            } else {
                                description = description;
                                noti.setName(description);
                            }
                            //noti.setName(description);
                            sendPrevious.add(noti);

                        }

                    } else {
                        Toast.makeText(Notificationo.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                _SendPrevNotiAdapter = new SendPrevNotiAdapter(Notificationo.this, sendPrevious);
                lvprevNoti.setAdapter(_SendPrevNotiAdapter);

                lvprevNoti.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String id_ = sendPrevious.get(position).getCode().toString();
                        //Toast.makeText(Notificationo.this,id_,Toast.LENGTH_LONG).show();

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException sd)
            {

            }



        }
    }
}
