package digillence.streettreat.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import digillence.streettreat.adapter.ExpandableListAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import digillence.streettreat.R;
import digillence.streettreat.util.Constant;

public class StoreDetailsActivity extends Activity implements View.OnClickListener {


TextView FabText,FabText2;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    ViewPager viewPager,vpExibhistion;
    int[] mResources;
  //  private ArrayList<CollectionModel> collectionList= new ArrayList<>();

    int currentPage;
    Timer swipeTimer;
    Context context;
    TextView txtImgNotification;
    RecyclerView rvDeals,rvCollection;
 //   DealsAdapter dealsAdapter;
   // CollectionAdapter collectionAdapter;
   // ArrayList<DealsModel> dealList= new ArrayList<>();
  //  private ArrayList<ExhibitionModel> exhibitionList= new ArrayList<>();
    digillence.streettreat.adapter.ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;



    private SeekBar volumeControl = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_details);

        expandableListmethod();
        init();
        setSlideShow();

        volumebar();
    }

    private void volumebar() {


        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 1000;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;
                if (progress <= 50000) {
                    setProgressBarColor(volumeControl, Color.rgb(255 - (255 / 100 * (100 - progress * 2)), 255, 0));

                } else {
                    setProgressBarColor(volumeControl, Color.rgb(255, 255 - (255 / 100 * (progress - 50) * 2), 0));

                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(StoreDetailsActivity.this, "Budget :" + progressChanged,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void setProgressBarColor(SeekBar seakBar, int newColor){
        LayerDrawable ld = (LayerDrawable) seakBar.getProgressDrawable();
        ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(R.id.progressshape);
        d1.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);

    }
    private void expandableListmethod()
    {

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Buy 3 Get 1 Free (WOMEN)");
        listDataHeader.add("5% Off<500 (WOMEN)");
        listDataHeader.add("5% Off<500 (KIDS)");
        listDataHeader.add("5% Off<500 (KIDS)");
        listDataHeader.add("5% Off<500 (KIDS)");
        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
    }











        private void setFont() {

            txtImgNotification.setTypeface(Constant.imgFont(context));
        }

        private void init() {
            context = this;
            FabText= new TextView(this);
            volumeControl = (SeekBar) findViewById(R.id.volume_bar);
            viewPager = (ViewPager) findViewById(R.id.view_pager);
            vpExibhistion = (ViewPager) findViewById(R.id.vpExibhistion);
            txtImgNotification = (TextView) findViewById(R.id.txtImgNotification);
            rvDeals = (RecyclerView) findViewById(R.id.rvDeals);
            rvCollection = (RecyclerView) findViewById(R.id.rvCollection);
           fab = (FloatingActionButton)findViewById(R.id.fab);
         //   fab1 = (FloatingActionButton)findViewById(R.id.fab1);
         //   fab2 = (FloatingActionButton)findViewById(R.id.fab2);
            fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fb_open);
            fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
          rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forword);
            rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
          fab.setOnClickListener(this);
         //   fab1.setOnClickListener(this);
         //   fab2.setOnClickListener(this);


            setSlideShow();


        }




        private void setSlideShow() {
            mResources = new int[]{
                    R.drawable.promo1,
                    R.drawable.promo2,
                    R.drawable.promo3,
                    R.drawable.promo4,

            };
            CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(this);
            viewPager.setAdapter(customPagerAdapter);
            CirclePageIndicator circlePageIndicator = (CirclePageIndicator)findViewById(R.id.titles);
            circlePageIndicator.setViewPager(viewPager);
            slideShow();
        }

        private void slideShow() {
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    currentPage = viewPager.getCurrentItem();
                    if (currentPage == 3) {
                        viewPager.setCurrentItem(0, true);
                    }else{
                        viewPager.setCurrentItem(currentPage+1, true);
                    }
                }
            };

            swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 100, 4000);
        }


        class CustomPagerAdapter extends PagerAdapter {

            Context mContext;
            LayoutInflater mLayoutInflater;

            public CustomPagerAdapter(Context context) {
                mContext = context;
                mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            @Override
            public int getCount() {
                return mResources.length;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == ((LinearLayout) object);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View itemView = mLayoutInflater.inflate(R.layout.pager_items, container, false);

                ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
                imageView.setImageResource(mResources[position]);

                container.addView(itemView);

                return itemView;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((LinearLayout) object);
            }
        }

        public void onClick(View v) {
            switch (v.getId())
            {

                case R.id.fab:

                    animateFAB();
                    Log.d("STREE", "Fab 1");
                    break;
               /* case R.id.fab1:

                    Log.d("STREE", "Fab 1");
                    break;
                case R.id.fab2:

                    Log.d("STREE", "Fab 2");
                    break;*/

            }
        }

        public void animateFAB() {

            if (isFabOpen) {

                fab.startAnimation(rotate_backward);
                //fab1.startAnimation(fab_close);
               // fab2.startAnimation(fab_close);
               // fab1.setClickable(false);
              //  fab2.setClickable(false);
                LinearLayout ll=new LinearLayout(this);
                ll.setOrientation(LinearLayout.VERTICAL);
ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT));
                FabText.setTextSize(14);

                FabText.startAnimation(fab_close);
                FabText.setClickable(false);
                isFabOpen = false;
                setContentView(ll);
                Log.d("Raj", "close");

            } else {
                LinearLayout ll=new LinearLayout(this);
                ll.setOrientation(LinearLayout.VERTICAL);
                ll.setLayoutParams(new  LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                fab.startAnimation(rotate_forward);
                FabText.setText("OPPEN");
                FabText.startAnimation(fab_open);
                FabText.setClickable(true);
              //  fab1.startAnimation(fab_open);
              //  fab2.startAnimation(fab_open);
              //  fab1.setClickable(true);
               // fab2.setClickable(true);
               isFabOpen = true;
                setContentView(ll);
                Log.d("Raj", "open");

            }
        }
    }






