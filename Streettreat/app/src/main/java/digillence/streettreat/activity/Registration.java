package digillence.streettreat.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import digillence.streettreat.R;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 19/12/16.
 */
public class Registration extends Activity {

    TextView Reg_title,Detail_title,Person_Detail_title;
    ImageView headerbar_back_toolbar;
    EditText storeName,storeAdd,name,mobile,email;
    private ProgressDialog mProgressDialog;
    Button regsubmit;
    Context c = Registration.this;
    // private ProgressDialog progress;
    String storeNamestr,storeAddstr,namestr,mobilestr,emailstr;
    private static Toast toast;
    JSONArray jsonReg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        headerbar_back_toolbar = (ImageView) findViewById(R.id.headerbar_back_toolbar);
        storeName= (EditText) findViewById(R.id.storeName);
        storeAdd= (EditText) findViewById(R.id.storeAdd);
        name= (EditText) findViewById(R.id.name);
        mobile= (EditText) findViewById(R.id.mobile);
        email= (EditText) findViewById(R.id.email);
        regsubmit=(Button) findViewById(R.id.regsubmit);
        Reg_title=(TextView) findViewById(R.id.Reg_title);
        Detail_title=(TextView) findViewById(R.id.Detail_title    );
        Person_Detail_title=(TextView) findViewById(R.id.Person_Detail_title);
        toast = new Toast(this);


        Typeface font = Typeface.createFromAsset(getAssets(),"font/Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(),"font/Raleway-Regular.ttf");

        Reg_title.setTypeface(font);
        Detail_title.setTypeface(font);
        regsubmit.setTypeface(font);
        storeName.setTypeface(font2);
        storeAdd.setTypeface(font2);
        name.setTypeface(font2);
        mobile.setTypeface(font2);
        email.setTypeface(font2);

        storeName.setHint(Html.fromHtml("STORE NAME<big>&nbsp;&#42;</big>"));

        storeAdd.setHint(Html.fromHtml("STORE ADDRESS <big>&nbsp;&#42;</big>"));

        name.setHint(Html.fromHtml("FULL NAME<big>&nbsp;&#42;</big>"));

        mobile.setHint(Html.fromHtml("MOBILE NUMBER<big>&nbsp;&#42;</big>"));

        email.setHint(Html.fromHtml("EMAIL ID<big>&nbsp;&#42;</big>"));



        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    // your action here
                    checkUniquenessEmail();
                }
            }
        });

        storeAdd.setRawInputType(InputType.TYPE_CLASS_TEXT);
        storeAdd.setImeActionLabel(getResources().getString(R.string.Next), EditorInfo.IME_ACTION_DONE);
        storeAdd.setImeOptions(EditorInfo.IME_ACTION_DONE);

        storeAdd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        name.requestFocus();
                    } else if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        name.requestFocus();

                    } else if (actionId == EditorInfo.IME_ACTION_GO) {
                    } else {
                        return false;
                    }
                } else if (actionId == EditorInfo.IME_NULL) {

                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    } else {
                        return true;
                    }
                } else {

                    return false;
                }
                return true;
            }
        });

        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(c,Login.class);
                startActivity(in);
                finish();


                if (toast != null)
                    toast.cancel();


            }
        });
        regsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                storeNamestr= storeName.getText().toString();
                storeAddstr= storeAdd.getText().toString();
                namestr=name.getText().toString();
                mobilestr=mobile.getText().toString();
                emailstr=email.getText().toString().trim();


                if (storeName.getText().toString().trim().length() < 1) {

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Store name is required", Toast.LENGTH_LONG);
                    toast.show();

                   // Toast.makeText(Registration.this, "Store name is required", Toast.LENGTH_LONG).show();
                } else if (storeAdd.getText().toString().trim().length() < 1) {

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Store address is required", Toast.LENGTH_LONG);
                    toast.show();
                    //Toast.makeText(Registration.this, "Store address is required", Toast.LENGTH_LONG).show();
                } else if (name.getText().toString().trim().length() < 1) {
                 //   Toast.makeText(Registration.this, "Name is required", Toast.LENGTH_LONG).show();

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Name is required", Toast.LENGTH_LONG);
                    toast.show();

                } else if (mobile.getText().toString().trim().length() < 10) {
                   // Toast.makeText(Registration.this, "Mobile number is required", Toast.LENGTH_LONG).show();

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Mobile number is required", Toast.LENGTH_LONG);
                    toast.show();


                }else if (email.getText().toString().trim().length() < 1) {
                   // Toast.makeText(Registration.this, "Email is required", Toast.LENGTH_LONG).show();


                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Email is required", Toast.LENGTH_LONG);
                    toast.show();

                }
                else if (!Constant.isEmailValid(email.getText())) {
                   // Toast.makeText(Registration.this, "Email is required", Toast.LENGTH_LONG).show();

                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(), "Email is required", Toast.LENGTH_LONG);
                    toast.show();
                }

                else {

                    new  userReg().execute();


                }
            }
        });
    }

    class userReg extends AsyncTask<String, String, JSONArray> {


        Dialog mProgressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog =  new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar);
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setContentView(R.layout.loader_gif);
            final ImageView  animatedGifImageView=(ImageView) mProgressDialog.findViewById(R.id.animatedGifImageView);
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(animatedGifImageView);
            Glide.with(Registration.this).load(R.drawable.loader).into(imageViewTarget);
            mProgressDialog.show();
        }
      @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("store_name",storeNamestr));
            params.add(new BasicNameValuePair("store_address", storeAddstr));
            params.add(new BasicNameValuePair("name", namestr));
            params.add(new BasicNameValuePair("mobile", mobilestr));
            params.add(new BasicNameValuePair("device_os", "android"));
            params.add(new BasicNameValuePair("email", emailstr));

            jsonReg = jParser.getJSONFromUrl(Urls.registration, params);

            return jsonReg;
        }
        protected void onPostExecute(JSONArray jsonReg) {
            try {

                mProgressDialog.dismiss();
                for(int i=0; i<jsonReg.length(); i++)
                {
                    JSONObject obj = jsonReg.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {


                        Intent in= new Intent(Registration.this,Login.class);
                        startActivity(in);


                    }

                    else
                    {
                       // Toast.makeText(Registration.this, obj.getString("message"), Toast.LENGTH_LONG).show();


                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();




                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(Registration.this,Login.class);
        startActivity(in);
        finish();

        if (toast != null)
            toast.cancel();



    }

    private void checkUniquenessEmail() {



        mProgressDialog = (ProgressDialog) new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar);
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setContentView(R.layout.loader_gif);

        final ImageView  animatedGifImageView=(ImageView) mProgressDialog.findViewById(R.id.animatedGifImageView);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(animatedGifImageView);
        Glide.with(Registration.this).load(R.drawable.loader).into(imageViewTarget);
        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.checkUniqueness,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progress.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("1")) {
                                // checkUniquenessMobile();
                            } else {

                              //  Toast.makeText(Registration.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();


                                if (toast != null)
                                    toast.cancel();
                                toast = Toast.makeText(getBaseContext(), jsonObject.getString("message"), Toast.LENGTH_LONG);
                                toast.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // progress.dismiss();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progress.dismiss();

                       // Toast.makeText(Registration.this, error.toString(), Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(), error.toString(), Toast.LENGTH_LONG);
                        toast.show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("unique_field", "email");
                params.put("value[0]", email.getText().toString().trim());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }




}
