package digillence.streettreat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import digillence.streettreat.R;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 21/12/16.
 */
public class AboutUs extends Activity
{

   TextView tv,Headerbartitle;
    ImageView headerbar_back_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_aboutus);
        aboutUs();
        tv = (TextView) findViewById(R.id.aboutustext);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(AboutUs.this,Home.class);
                startActivity(in);
            }
        });

        Headerbartitle= (TextView)findViewById(R.id.Headerbartitle);
        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");
        Headerbartitle.setText(store_namestr);




    }
    private void aboutUs(){
        StringRequest sr = new StringRequest(Request.Method.POST, Urls.aboutus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                      String content=  jsonObject.getJSONObject("items").getString("content");

                        tv.setText(Html.fromHtml(content));


                    } else {
                        //  progress.dismiss();
                        Toast.makeText(AboutUs.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    //  progress.dismiss();

                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();

                Toast.makeText(AboutUs.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title","about us");
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(sr);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(AboutUs.this,Home.class);
        startActivity(in);

    }
}

