package digillence.streettreat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import digillence.streettreat.R;

/**
 * Created by devashree on 28/12/16.
 */
public class ActivityFAQ extends Activity {
    WebView webView;
    ImageView headerbar_back_toolbar;
    ProgressDialog prDialog;
    TextView Headerbartitle;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i1 = new Intent(ActivityFAQ.this, Home.class);
        startActivity(i1);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        webView = (WebView) findViewById(R.id.FAQ_WEBVIEW);
        webView.setWebViewClient(new MyWebViewClient());
        String url = "http://www.web.streettreat.in/faq-s?tmpl=component";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(url);
        headerbar_back_toolbar = (ImageView) findViewById(R.id.headerbar_back_toolbar);

        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ActivityFAQ.this, Home.class);
                startActivity(in);
            }
        });
        Headerbartitle= (TextView)findViewById(R.id.Headerbartitle);
        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");
        Headerbartitle.setText(store_namestr);




    }
       private class MyWebViewClient extends WebViewClient {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                prDialog = new ProgressDialog(ActivityFAQ.this);
                prDialog.setMessage("Please wait ...");
                prDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(prDialog!=null){
                    prDialog.dismiss();
                }
            }
        }



}































































































