package digillence.streettreat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;

import digillence.streettreat.R;

public class Contact extends Activity  implements View.OnClickListener{
    WebView mapview;
    ImageView back_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
init();
        getId();

    }

    private void init() {

        back_toolbar=(ImageView) findViewById(R.id.back_toolbar);

        back_toolbar.setOnClickListener(this);
    }

    public void onClick(View v)
    {
        switch (v.getId())
        {




            case R.id.back_toolbar:
                finish();
                break;
        }

    }

    void getId()
    {

        mapview = (WebView) findViewById(R.id.mapview);
        mapview.setWebChromeClient(new MyWebViewClient());
        //	progressbar = (ProgressBar) findViewById(R.id.progressBar_contactus);
        //progressbar.setMax(100);
        mapview.getSettings().setJavaScriptEnabled(true);
        mapview.loadUrl("https://www.google.com/maps/place/Digillence+Rolson+Digital+LLP/@19.1144711,73.012315,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7c0c2310d2bf7:0xbb029fa2ca1cfec7!8m2!3d19.114466!4d73.014509?hl=en");
      //  mapview.loadUrl("https://www.google.com/maps/d/embed?mid=15-P1v84K75xnnVfymDwa4f1XRic");

       // ContactUs.this.progressbar.setProgress(0);
    }
    private class MyWebViewClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            Contact.this.setValue(newProgress);
            super.onProgressChanged(view, newProgress);



            Runnable progressRunnable = new Runnable() {

                @Override
                public void run() {

                    //progressBar_certificate.setVisibility(View.GONE);
                }
            };

            Handler pdCanceller = new Handler();
            pdCanceller.postDelayed(progressRunnable, 3000);
        }


    }

    public void setValue(int progress) {
        //this.progressbar.setProgress(progress);

        // if (progress>=100) // code to be added
        // progressbar.setVisibility(View.GONE);  // code to be added
    }
}
