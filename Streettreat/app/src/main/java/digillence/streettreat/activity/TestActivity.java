package digillence.streettreat.activity;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import digillence.streettreat.R;
import digillence.streettreat.fragment.AboutUsFragment;
import digillence.streettreat.fragment.ActivityFAQFragment;
import digillence.streettreat.fragment.ActivityReportFragment;
import digillence.streettreat.fragment.ActivityReviewFragment;
import digillence.streettreat.fragment.ContactUsFragment;
import digillence.streettreat.fragment.DashBoardFragment;
import digillence.streettreat.fragment.HomeFragment;
import digillence.streettreat.fragment.MyFavFragment;
import digillence.streettreat.fragment.NewsandEventsFragment;
import digillence.streettreat.fragment.NotificationListFragment;
import digillence.streettreat.fragment.TermsAndConditionsFragment;
import digillence.streettreat.fragment.sendNotiFragment;

public class TestActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView mNavigationView;
    TextView myCoupans_icon,Report_icon,Dashboard_icon,Review_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_layout_test);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        drawerLayout= (DrawerLayout)findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        myCoupans_icon= (TextView)findViewById(R.id.myCoupans_icon);
        Report_icon= (TextView)findViewById(R.id.Report_icon);
        Dashboard_icon= (TextView)findViewById(R.id.Dashboard_icon);
        Review_icon= (TextView)findViewById(R.id.Review_icon);
        setFont();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        mNavigationView.setItemIconTintList(null);
//       myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
//        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in= new Intent(TestActivity.this,Login.class);
//                startActivity(in);
//
//            }
//        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.app, // nav drawer open - description for accessibility
                R.string.app // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {

                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                //   invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {

                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

//        android.support.v4.app.Fragment fragment = null;
//        fragment = new HomeFragment();
//        if (fragment != null) {
//            FragmentManager fragmentManager = getFragmentManager();
//            getSupportFragmentManager().popBackStack();
//
//
//            fragmentManager.beginTransaction()
//                    .replace(R.id.content_frame, fragment).addToBackStack(null).commit();
//
//            drawerLayout.closeDrawer(mNavigationView);
//
//
//        } else {
//            // error in creating fragment
//            // Log.e("MainActivity", "Error in creating fragment");
//        }

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HomeFragment fragment = new HomeFragment();
        fragmentTransaction.replace(R.id.content_frame, fragment).addToBackStack("");
        fragmentTransaction.commit();

        myCoupans_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Fragment fragment = null;
//                fragment = new HomeFragment();
//                if (fragment != null) {
//                    FragmentManager fragmentManager = getFragmentManager();
//                    getSupportFragmentManager().popBackStack();
//
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, fragment).addToBackStack(null).commit();



                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                HomeFragment fragment = new HomeFragment();
                fragmentTransaction.replace(R.id.content_frame, fragment).addToBackStack("");
                fragmentTransaction.commit();




                    drawerLayout.closeDrawer(mNavigationView);


//                } else {
//                    // error in creating fragment
//                    // Log.e("MainActivity", "Error in creating fragment");
//                }

            }
        });

        Report_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Fragment fragment = null;
                fragment = new ActivityReportFragment();
                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, fragment).commit();

                    drawerLayout.closeDrawer(mNavigationView);


                } else {
                    // error in creating fragment
                    // Log.e("MainActivity", "Error in creating fragment");
                }

            }
        });

        Review_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                Intent in= new Intent(TestActivity.this, ActivityReview.class);
//                startActivity(in);

//                android.support.v4.app.Fragment fragment = null;
//               Fragment frag =null;
//                fragment = new ActivityReviewFragment();
//                if (fragment != null) {
//                    FragmentManager fragmentManager = getFragmentManager();
//                    getSupportFragmentManager().popBackStack();
//
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame,frag).addToBackStack(null).commit();



                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ActivityReviewFragment fragment = new ActivityReviewFragment();
                fragmentTransaction.replace(R.id.content_frame, fragment).addToBackStack("");
                fragmentTransaction.commit();

                drawerLayout.closeDrawer(mNavigationView);



//            }
//
//            else
//
//            {
//
//            }

        }
    });

        Dashboard_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                Fragment fragment = null;
                fragment = new DashBoardFragment();
                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    getSupportFragmentManager().popBackStack();

                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, fragment).addToBackStack(null).commit();

                    drawerLayout.closeDrawer(mNavigationView);

                } else {
                    // error in creating fragment
                    // Log.e("MainActivity", "Error in creating fragment");
                }

            }
        });


        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                Fragment fragment = null;
                switch (item.getItemId()) {

                    case R.id.aboutus:

                        fragment = new AboutUsFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();

                            drawerLayout.closeDrawer(mNavigationView);


                        } else {

                        }
                        break;

                    case R.id.sendNoti:

                        fragment = new sendNotiFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();

                            drawerLayout.closeDrawer(mNavigationView);


                        } else {
                            // error in creating fragment
                            // Log.e("MainActivity", "Error in creating fragment");
                        }
                        break;

//                        Intent in= new Intent(TestActivity.this,RadioTestActivity.class);
//                        startActivity(in);

                    //  break;


                    case R.id.myfav:
                        fragment = new MyFavFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();
                            drawerLayout.closeDrawer(mNavigationView);


                        } else {
                        }
                        break;

                    case R.id.faqs:

                        fragment = new ActivityFAQFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();
                            drawerLayout.closeDrawer(mNavigationView);


                        } else {

                        }
                        break;


                    case R.id.tc:

                        fragment = new TermsAndConditionsFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();

                            drawerLayout.closeDrawer(mNavigationView);


                        } else {

                        }
                        break;

                    case R.id.contactUs:
                        fragment = new ContactUsFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();

                            drawerLayout.closeDrawer(mNavigationView);


                        } else {

                        }
                        break;

                    case R.id.help:

                        fragment = new NewsandEventsFragment();
                        if (fragment != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment).addToBackStack("").commit();

                            drawerLayout.closeDrawer(mNavigationView);


                        } else {

                        }
                        break;


                    case R.id.logout: {

                        SharedPreferences sharedPreferences = getSharedPreferences("log_id", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("log_id", "NA");
                        edit.commit();

                        Intent intent = new Intent(TestActivity.this, Login.class);
                        intent.putExtra("finish", true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                        startActivity(intent);
                        finish();

                    }

                }
                return false;
            }


        } );

}
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // toggle nav drawer on selecting action bar app icon/title
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            //case R.id.action_settings:
            //  return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items

        //menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setFont()
    {   Typeface font = Typeface.createFromAsset(getAssets(), "font/Raleway-Bold.ttf");
        Typeface font3 = Typeface.createFromAsset(getAssets(),"font/fontello.ttf");

        myCoupans_icon.setTypeface(font3);
        myCoupans_icon.setText("\ue028");
        myCoupans_icon.setTextColor(Color.GRAY);

        Report_icon.setTypeface(font3);
        Report_icon.setText("\ue070");
        Report_icon.setTextColor(Color.GRAY);

        Dashboard_icon.setTypeface(font3);
        Dashboard_icon.setText("\ue030");
        Dashboard_icon.setTextColor(Color.GRAY);

        Review_icon.setTypeface(font3);
        Review_icon.setText("\ue071");
        Review_icon.setTextColor(Color.GRAY);
    }

    MenuItem item,menu_title;
    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu_main, menu);

        item  = menu.findItem(R.id.menu_item);
        MenuItem Notificationbell= menu.findItem(R.id.Notificationbell);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                drawerLayout.openDrawer(GravityCompat.END);
                return false;
            }
        });

        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String menuTitle=sharedPrefStoreName.getString("store_name","");

        menu_title=menu.findItem(R.id.menu_title);
        menu_title.setTitle(menuTitle);

        SpannableString s = new SpannableString(menu_title.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, s.length(), 0);
        menu_title.setTitle(s);

        Notificationbell.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Fragment fragment = null;
                fragment = new NotificationListFragment();
                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, fragment).addToBackStack("").commit();
                    drawerLayout.closeDrawer(mNavigationView);

                } else {

                }

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void onBackPressed() {


        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

}
