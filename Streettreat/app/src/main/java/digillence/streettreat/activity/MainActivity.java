package digillence.streettreat.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import digillence.streettreat.R;
import digillence.streettreat.adapter.CollectionAdapter;
import digillence.streettreat.adapter.DealsAdapter;
import digillence.streettreat.adapter.ExibhitionAdapter;
import digillence.streettreat.model.CollectionModel;
import digillence.streettreat.model.DealsModel;
import digillence.streettreat.model.ExhibitionModel;
import digillence.streettreat.util.Constant;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager,vpExibhistion;
    int[] mResources;
    private ArrayList<CollectionModel> collectionList= new ArrayList<>();

    int currentPage;
    Timer swipeTimer;
    Context context;
    TextView txtImgNotification;
    RecyclerView rvDeals,rvCollection;
    DealsAdapter dealsAdapter;
    CollectionAdapter collectionAdapter;
     ArrayList<DealsModel> dealList= new ArrayList<>();
    private ArrayList<ExhibitionModel> exhibitionList= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setFont();
       // getLocation();
    }



    private void setFont() {

        txtImgNotification.setTypeface(Constant.imgFont(context));
    }

    private void init() {
        context = this;
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        vpExibhistion = (ViewPager) findViewById(R.id.vpExibhistion);
        txtImgNotification = (TextView) findViewById(R.id.txtImgNotification);
        rvDeals = (RecyclerView) findViewById(R.id.rvDeals);
        rvCollection = (RecyclerView) findViewById(R.id.rvCollection);


        setSlideShow();
        setRv();
        setRvCollection();
        setExibhition();

    }

    private void setExibhition() {
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1,"Fashion Expo","Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1,"Fashion Expo","Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        ExibhitionAdapter exibhitionAdapter = new ExibhitionAdapter(this,exhibitionList);
        vpExibhistion.setAdapter(exibhitionAdapter);
    }

    private void setRv() {
        dealsAdapter= new DealsAdapter(dealList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rvDeals.setLayoutManager(mLayoutManager);
        rvDeals.setItemAnimator(new DefaultItemAnimator());
        rvDeals.setAdapter(dealsAdapter);
        callWebDeals();
    }
 private void setRvCollection() {
     collectionAdapter= new CollectionAdapter(collectionList);
     GridLayoutManager gridLayoutManager= new GridLayoutManager(context,2);
        rvCollection.setLayoutManager(gridLayoutManager);
     rvCollection.setHasFixedSize(true);
     rvCollection.setItemAnimator(new DefaultItemAnimator());
     rvCollection.setAdapter(collectionAdapter);
        callWebCollection();
    }

    private void callWebCollection() {
        collectionList.add(new CollectionModel("WINTER COLLECTION"));
        collectionList.add(new CollectionModel("HOLI COLLECTION"));
        collectionList.add(new CollectionModel("SUMMUR COLLECTION"));
        collectionList.add(new CollectionModel("WINTER COLLECTION"));
        collectionList.add(new CollectionModel("SUMMUR SALES"));
        collectionAdapter.notifyDataSetChanged();
    }

    private void callWebDeals() {
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealList.add(new DealsModel("20","Mens T-Shirt"));
        dealList.add(new DealsModel("35","Kids jeans"));
        dealList.add(new DealsModel("15","Girls T-Shirt"));
        dealsAdapter.notifyDataSetChanged();

    }

    private void setSlideShow() {
        mResources = new int[]{
                R.drawable.promo1,
                R.drawable.promo2,
                R.drawable.promo3,
                R.drawable.promo4,

        };
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(this);
        viewPager.setAdapter(customPagerAdapter);
        CirclePageIndicator circlePageIndicator = (CirclePageIndicator)findViewById(R.id.titles);
        circlePageIndicator.setViewPager(viewPager);
        slideShow();
    }

    private void slideShow() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                currentPage = viewPager.getCurrentItem();
                if (currentPage == 3) {
                    viewPager.setCurrentItem(0, true);
                }else{
                    viewPager.setCurrentItem(currentPage+1, true);
                }
            }
        };

        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 4000);
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_items, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(mResources[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }  }

