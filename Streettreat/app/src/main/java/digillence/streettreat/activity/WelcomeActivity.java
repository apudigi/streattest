//package digillence.streettreat.activity;
//
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.ProgressDialog;
//import android.app.SearchManager;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.res.Configuration;
//import android.graphics.drawable.ColorDrawable;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.view.MenuItemCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v4.widget.SimpleCursorAdapter;
//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.widget.SearchView;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.ExpandableListView;
//import android.widget.ExpandableListView.OnChildClickListener;
//import android.widget.Toast;
//
//import org.apache.http.message.BasicNameValuePair;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import digillence.streettreat.R;
//import digillence.streettreat.fragment.ContactUsFragment;
//import digillence.streettreat.util.ConnectionDetector;
//import digillence.streettreat.util.JSONParser;
//
//
//public class Welcome extends ActionBarActivity
//{
//
//    //URL to get JSON Array
//    private static String urlGetMenus = Config.url+"GetMenus";
//    JSONArray jsonGetMenus;
//
//    //JSON Node Names
//    private static final String TAG_GetMenusMenuId = "MenuId";
//    private static final String TAG_GetMenusMenuName = "MenuName";
//    private static final String TAG_GetMenusMenuType = "MenuType";
//    private static final String TAG_GetMenusMenuParentId = "MenuParentId";
//    private static final String TAG_GetMenusListmenu = "Listmenu";
//
//    ArrayList<String> arrMenuTypeName;
//    List<String> arrMenuTypeId;
//    ArrayList<String> arrMenuImageName;
//
//    ArrayList<String> arrSubMenuTypeName;
//    List<String> arrSubMenuTypeId;
//
//    ArrayList<Object> childItem = new ArrayList<Object>();
//
//    private DrawerLayout drawer;
//    private ExpandableListView drawerList;
//    private ActionBarDrawerToggle actionBarDrawerToggle;
//
//    // used to store app title
//    private CharSequence mTitle;
//    // nav drawer title
//    private CharSequence mDrawerTitle;
//
//    private SharedPreferences prefsuserid,prefNotification,flagMenuListMenuValue;
//    String prefuserid;
//    public static final String KEY_USERID = "KEY_USERID";
//    public static final String KEY_MOBILENUMBER = "KEY_MOBILENUMBER";
//
//    private SharedPreferences prefsmenuid;
//    public static final String KEY_MenuID = "KEY_MENUID";
//
//    public static final String KEY_flagAccepted = "KEY_FLAGACCEPTED";
//    private static String urlFetchEntireArticle = Config.url+"FetchEntireArticle";
//
//    private static String urlCheckIfUpdatesAvailableformenu = Config.url+"CheckIfUpdatesAvailableformenu";
//
//    private static final String TAG_ResponseText = "ResponseText";
//
//
//
//    private static final String TAG_GetArticlesId = "ArticleID";
//    private static final String TAG_GetArticlesMenuID = "ArtMenuID";
//    private static final String TAG_GetArticlesPath = "ArtPath";
//    private static final String TAG_GetArticlesFile = "ArtFile";
//    private static final String TAG_GetArticlesRd = "ArtRd";
//    private static final String TAG_GetArticlesTitle = "ArtTitle";
//    private static final String TAG_GetArticlesCommentCount = "ArtCommentCount";
//    private static final String TAG_GetArticlesDate = "ArtDate";
//    private static final String TAG_GetArticlesReadCount = "ArtReadCount";
//    private static final String TAG_GetArticlesZipPath = "ArtZipPath";
//    private static final String TAG_GetArticlesDesc = "ArtDesc";
//    private static final String TAG_GetArticlesType = "ArtType";
//    String ResponseText;
//    JSONArray CheckIfUpdatesAvailableformenu;
//    DatabaseHandler db = new DatabaseHandler(this);
//    private SimpleCursorAdapter mAdapter;
//
//    String[] SUGGESTIONS;
//    //    String[] SUGGESTIONS = {
////            "Bauru", "Sao Paulo", "Rio de Janeiro",
////            "Bahia", "Mato Grosso", "Minas Gerais",
////            "Tocantins", "Rio Grande do Sul"
////    };
//    List<String> articleData_;
//    ArrayList<Articles> articles;
//    ArrayList<Articles> ArticlesListTemp;
//    ArrayList<String> articleId;
//    JSONArray jsonGetArticlesByMenuId;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_drawer_layout_test);
//
//
//        prefsuserid = getSharedPreferences(NavigationDrawerActivity.KEY_USERID, Context.MODE_PRIVATE);
//        prefuserid = prefsuserid.getString(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_USERID, "NA");
//
//
//        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawerList = (ExpandableListView) findViewById(R.id.left_drawer);
//        //drawer.closeDrawer(drawerList);
//        mTitle = mDrawerTitle = getTitle();
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
//                .getColor(R.color.menubar_homepage)));
//
//        // enabling action bar app icon and behaving it as toggle button
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        //getSupportActionBar.setDisplayUseLogoEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setIcon(R.drawable.scalpel_icon);
//
//        //   articleData_=db.getAllArticleData();
//
//        //  SUGGESTIONS= articleData_.toArray(new String[articleData_.size()]);
//
//
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer,
//                R.string.app_name, // nav drawer open - description for accessibility
//                R.string.app_name // nav drawer close - description for accessibility
//        ) {
//            public void onDrawerClosed(View view) {
//
//                //getSupportActionBar().setTitle("  "  +mTitle);
//
//                // calling onPrepareOptionsMenu() to show action bar icons
//
//                InputMethodManager inputMethodManager = (InputMethodManager)
//                        getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//                //   invalidateOptionsMenu();
//            }
//
//            public void onDrawerOpened(View drawerView) {
//
//                //getSupportActionBar().setTitle(mDrawerTitle);
//                InputMethodManager inputMethodManager = (InputMethodManager)
//                        getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//                // calling onPrepareOptionsMenu() to hide action bar icons
//                // invalidateOptionsMenu();
//            }
//        };
//        drawer.setDrawerListener(actionBarDrawerToggle);
//
//
//        ArrayList<Menus> menudata = db.getAllMenusData();
//
//        if (menudata.size() != 0)
//        {
//
//            //  new CheckIfUpdatesAvailableformenu().execute();
//
//
//            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//            Boolean isInternetPresent = cd.isConnectingToInternet();
//            if (isInternetPresent) {
//
//                new CheckIfUpdatesAvailableformenu().execute();
//
//
//            } else {
//                Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
//            }
//
//
//            ///////////check if data is updated or not?
//            arrMenuTypeId = new ArrayList<String>();
//            arrMenuTypeName = new ArrayList<String>();
//            arrMenuImageName = new ArrayList<String>();
//
//            for (Menus menu : menudata) {
//                arrMenuTypeId.add(menu.getMenuId());
//                arrMenuTypeName.add(menu.getMenuName());
//                arrMenuImageName.add(menu.getMenuImage());
//
//                arrSubMenuTypeId = new ArrayList<String>();
//                arrSubMenuTypeName = new ArrayList<String>();
//
//                ArrayList<Menus> submenudata = db.getAllSubMenusDataByMenuId(menu.getMenuId());//  SELECT  * FROM MenusData WHERE MenusParentId = '1'
//                for (Menus submenu : submenudata) {
//                    arrSubMenuTypeId.add(submenu.getMenuId());
//                    arrSubMenuTypeName.add(submenu.getMenuName());
//                }
//                childItem.add(arrSubMenuTypeName);
//            }
//            initDrawer();
//        } else {
//            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//            Boolean isInternetPresent = cd.isConnectingToInternet();
//            if (isInternetPresent) {
//
//                new JSONParseGetMenus().execute();
//
//                //  new JSONParseFetchEntireArticle().execute();
//
//
//            } else {
//                Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
//            }
//        }
//
//        groupClick();
//        childClick();
//
//
//        if (savedInstanceState == null) {
//            // on first time display view for first nav item
//            displayView(1);
//        }
//
//        drawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            int previousItem = -1;
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if (groupPosition != previousItem)
//                    drawerList.collapseGroup(previousItem);
//                previousItem = groupPosition;
//            }
//        });
//
//
//    }
//
//    private void groupClick() {
//        drawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
//
//        {
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                //Toast.makeText(getApplicationContext(), "Clicked On MainMenu" + v.getTag(),
//                //       Toast.LENGTH_SHORT).show();
//
//                //Toast.makeText(getApplicationContext(), "Group Position " + groupPosition,
//                //       Toast.LENGTH_SHORT).show();
//
//                //    Log.e("aaaaaaaa tag", "" + v.getTag());
//                setTitle("" + v.getTag());
//                // mTitle = mDrawerTitle = getTitle();
//
//                String menuId = arrMenuTypeId.get(groupPosition);// menuid= 10, group position 9.Disscusionforum
//                //  Log.e("menuId", menuId);
//
//                prefsmenuid = getSharedPreferences(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, Context.MODE_PRIVATE);
//                SharedPreferences.Editor edit = prefsmenuid.edit();
//
//                edit.putString(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, menuId);
//                edit.commit();
//
//                prefsmenuid = getSharedPreferences(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, Context.MODE_PRIVATE);
//                // Log.e("prefsmenuid", prefsmenuid.getString(NavigationDrawerActivity.KEY_MenuID, "NA"));
//
//                displayView(Integer.parseInt(menuId));   //menuid=10;
//                hideKeyboard(v);
//                return false;
//
//            }
//
//            private void hideKeyboard(View view) {
//                // TODO Auto-generated method stub
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            }
//        });
//    }
//
//    private void childClick() {
//        drawerList.setOnChildClickListener(new OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//
//                //Toast.makeText(getApplicationContext(), "Clicked On Child" + v.getTag(),
//                //      Toast.LENGTH_SHORT).show();
//
//                //Toast.makeText(getApplicationContext(), "Group Position " + groupPosition + " Child Position " + childPosition,
//                //       Toast.LENGTH_SHORT).show();
//
//                // Log.e("aaaaaaaa tag", "" + v.getTag());
//                setTitle("" + v.getTag());
//                mTitle = mDrawerTitle = getTitle();// group position =9, childosition= 0;
//
//                String menuId = db.getMenuIdByMenuName("" + v.getTag());
//                //Log.e("aaaaaaaa menuId", "" + menuId);
//
//                prefsmenuid = getSharedPreferences(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, Context.MODE_PRIVATE);
//                SharedPreferences.Editor edit = prefsmenuid.edit();
//
//                edit.putString(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, menuId);
//                edit.commit();
//
//                prefsmenuid = getSharedPreferences(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.KEY_MenuID, Context.MODE_PRIVATE);
//                // Log.e("prefsmenuid", prefsmenuid.getString(NavigationDrawerActivity.KEY_MenuID, "NA"));
//
//                if (menuId.equals("33")) {
//                    Fragment fragment = new UploadYourVideoFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//
//                        drawerList.setItemChecked(childPosition, true);
//                        drawerList.setSelection(childPosition);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//
//                    }
//                } else {
//                    Fragment fragment = new SubMenusFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(childPosition, true);
//                        drawerList.setSelection(childPosition);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//
//                    }
//                }
//                hideKeyboard(v);
//                return true;
//            }
//
//            private void hideKeyboard(View view) {
//                // TODO Auto-generated method stub
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            }
//        });
//    }
//
//    private void initDrawer()
//    {
//        drawerList.setAdapter(new NewAdapter(this, arrMenuTypeName, arrMenuImageName, childItem));
//
//    }
//    MenuItem item;
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        // Inflate menu to add items to action bar if it is present.
//        inflater.inflate(R.menu.drawer_layout_test, menu);
//        SharedPreferences prefsmenuclicked;
//        // Associate searchable configuration with the SearchView
//
//
//
//        item  = menu.findItem(R.id.action_search);
//
//
//
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        //SearchView searchView =(SearchView) menu.findItem(R.id.action_search).getActionView();
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//        prefsmenuclicked = getApplicationContext().getSharedPreferences(HomeFragment.KEY_MenuClicked, Context.MODE_PRIVATE);
//        String prefmenuclicked = prefsmenuclicked.getString(HomeFragment.KEY_MenuClicked, "NA");
//
//        searchView.setIconified(true);
//
//
//        final ArrayList<Menus> submenu = db.getAllSubMenusDataByMenuId(prefmenuclicked);
//
//
//
//        //searchView.setSuggestionsAdapter(mAdapter);
//
//
//        searchView.setQueryHint("");
//
//
//
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                // Toast.makeText(getApplicationContext(), "***************",Toast.LENGTH_SHORT).show();
//                // populateAdapter(s);
//
//
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                //Toast.makeText(getApplicationContext(), "gfgf",Toast.LENGTH_SHORT).show();
//                //      populateAdapter(s);
//
//                return false;
//            }
//        });
//
//
//
//        searchView.setOnSearchClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //   Toast.makeText(getApplicationContext(), "&&&&&&&&&&&&", Toast.LENGTH_SHORT).show();
//
////                Intent in= new Intent(getApplicationContext(), SearchActivity.class);
////                startActivity(in);
//
//
//                Fragment fragment = new SearchFragment();
//
//                if (fragment != null) {
//                    FragmentManager fragmentManager = getFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, fragment).commit();
//                } else {
//                    // error in creating fragment
//                }
//
//
//            }
//        });
//
//        searchView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                //   Toast.makeText(getApplicationContext(), "Ontouch", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });
//
//
//
//
//        return super.onCreateOptionsMenu(menu);
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item)
//    {
//        // toggle nav drawer on selecting action bar app icon/title
//        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }
//        // Handle action bar actions click
//        switch (item.getItemId()) {
//            //case R.id.action_settings:
//            //  return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        // if nav drawer is opened, hide the action items
//        boolean drawerOpen = drawer.isDrawerOpen(drawerList);
//        //menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
//        return super.onPrepareOptionsMenu(menu);
//    }
//
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//        // Sync the toggle state after onRestoreInstanceState has occurred.
//
//        actionBarDrawerToggle.syncState();
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        // Pass any configuration change to the drawer toggls
//        actionBarDrawerToggle.onConfigurationChanged(newConfig);
//    }
//
//    /**
//     * Diplaying fragment view for selected nav drawer list item
//     * */
//    private void displayView(int position) {// 10
//
//
//        int newPos= position;
//
//        flagMenuListMenuValue = getSharedPreferences("flagMenuListMenuValue_Pref", Context.MODE_PRIVATE);
//
//        String strflagMenuListMenuValue= flagMenuListMenuValue.getString("flagMenuListMenuValue_KEY", "NA");
////        String MenulistMenu;
//        if(strflagMenuListMenuValue.equals("99999"))
//        {
//
//            position= 99999;
//            flagMenuListMenuValue = getSharedPreferences("flagMenuListMenuValue_Pref", Context.MODE_PRIVATE);
//            SharedPreferences.Editor edit1 = flagMenuListMenuValue.edit();
//            edit1.putString("flagMenuListMenuValue_KEY", "1");
//            edit1.commit();
//
//
//        }
//        else {
//            position=newPos;
//
//        }
//
//        String MenulistMenu = db.getListMenuByMenuId("" + position);
//        // Log.e("MenulistMenu", MenulistMenu);
//
//        // update the main content by replacing fragments
//        Fragment fragment = null;
//        switch (position)
//        {
//            case 1:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//
//                else
//                {
//
//                    fragment = new HomeFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        // Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 2:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 3:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //   Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 4:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 5:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 6:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        // Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 7:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 8:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 9:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //   Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 10:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //   Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 11:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//
//                    fragment = new SurveyFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //   Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 12:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new ReferAFriendFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 13:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new FavouriteFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //     Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 14:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new DisclaimerFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //   Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 15:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//                    fragment = new ContactUsFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            case 99999:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else
//                {
//
//
//                    flagMenuListMenuValue = getSharedPreferences("flagMenuListMenuValue_Pref", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor edit1 = flagMenuListMenuValue.edit();
//                    edit1.putString("flagMenuListMenuValue_KEY", "1");
//                    edit1.commit();
//
//
//
//                    fragment = new UpdateFrangment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//            default:
//                if(MenulistMenu.equals("1"))
//                {
//                    childClick();
//                }
//                else {
//                    fragment = new SubMenusFragment();
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//
//                        // update selected item and title, then close the drawer
//                        drawerList.setItemChecked(position, true);
//                        drawerList.setSelection(position);
//                        drawer.closeDrawer(drawerList);
//                    } else {
//                        // error in creating fragment
//                        //  Log.e("MainActivity", "Error in creating fragment");
//                    }
//                    break;
//                }
//        }
//    }
//    public void setActionBarTitle(String title){
//
//
//        getSupportActionBar().setTitle(title);
//    }
//
//    private class JSONParseGetMenus extends AsyncTask<String, String, JSONArray>
//    {
//        private ProgressDialog pDialog;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            pDialog = new ProgressDialog(cwt.cirrius.scalpel.Activity.NavigationDrawerActivity.this);
//            pDialog.setMessage("Getting Data ...");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            // pDialog.show();
//        }
//
//        @Override
//        protected JSONArray doInBackground(String... args) {
//
//            JSONParser jParser = new JSONParser();
//
//            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
//
//            params.add(new BasicNameValuePair("MenuId", "0"));
//            params.add(new BasicNameValuePair("UsrID", prefuserid));
//
//            // Getting JSON from URL
//            jsonGetMenus = jParser.getJSONFromUrl(urlGetMenus ,params);
//
//            return jsonGetMenus;
//        }
//        @Override
//        protected void onPostExecute(JSONArray jsonGetMenus) {
//            // pDialog.dismiss();
//            try {
//                int deletedmenu = db.deleteMenusData();
//                // Log.e("deletedmenu", ""+deletedmenu);
//
//                // Getting JSON Array from URL
//                arrMenuTypeId = new ArrayList<String>();
//                arrMenuTypeName = new ArrayList<String>();
//                arrMenuImageName = new ArrayList<String>();
//                //ArrayList<String> submenu = new ArrayList<String>();;
//                for(int i=0; i<jsonGetMenus.length(); i++)
//                {
//                    arrSubMenuTypeId = new ArrayList<String>();
//                    arrSubMenuTypeName = new ArrayList<String>();
//
//                    JSONObject obj = jsonGetMenus.getJSONObject(i);
//
//                    System.out.println(" --- response ---- " + jsonGetMenus);
//
//                    // Storing  JSON item in a Variable
//                    String MenuId = obj.getString(TAG_GetMenusMenuId);
//                    String MenuName = obj.getString(TAG_GetMenusMenuName);
//                    String MenuType = obj.getString(TAG_GetMenusMenuType);
//                    String MenuParentId = obj.getString(TAG_GetMenusMenuParentId);
//                    String MenuListMenu = obj.getString(TAG_GetMenusListmenu);
//                    JSONArray ListMenu = obj.getJSONArray(TAG_GetMenusListmenu);
//                    if(ListMenu.length() == 0)
//                    {
//                        db.addMenus(new Menus(MenuId, MenuName, "menu_"+MenuId, MenuType, MenuParentId, "0"));
//
//                    }
//                    else
//                    {
//                        db.addMenus(new Menus(MenuId, MenuName, "menu_"+MenuId, MenuType, MenuParentId, "1"));
//                    }
//
//                    for(int j=0; j<ListMenu.length(); j++)
//                    {
//                        JSONObject objlist = ListMenu.getJSONObject(j);
//                        String SubMenuId = objlist.getString(TAG_GetMenusMenuId);
//                        String SubMenuName = objlist.getString(TAG_GetMenusMenuName);
//                        String SubMenuType = objlist.getString(TAG_GetMenusMenuType);
//                        String SubMenuParentId = objlist.getString(TAG_GetMenusMenuParentId);
//                        String SubListmenu = objlist.getString(TAG_GetMenusListmenu);
//
//                        db.addMenus(new Menus(SubMenuId, SubMenuName, "submenu_"+SubMenuId, SubMenuType, SubMenuParentId, "0"));
//                    }
//
//                }
//
//                ArrayList<Menus> menudata = db.getAllMenusData();
//
//                if(menudata.size() !=0)
//                {
//                    arrMenuTypeId = new ArrayList<String>();
//                    arrMenuTypeName = new ArrayList<String>();
//                    arrMenuImageName = new ArrayList<String>();
//
//                    for(Menus menu : menudata)
//                    {
//                        arrMenuTypeId.add(menu.getMenuId());
//                        arrMenuTypeName.add(menu.getMenuName());
//                        arrMenuImageName.add(menu.getMenuImage());
//
//                        arrSubMenuTypeId = new ArrayList<String>();
//                        arrSubMenuTypeName = new ArrayList<String>();
//
//                        ArrayList<Menus> submenudata = db.getAllSubMenusDataByMenuId(menu.getMenuId());
//                        for(Menus submenu : submenudata)
//                        {
//                            arrSubMenuTypeId.add(submenu.getMenuId());
//                            arrSubMenuTypeName.add(submenu.getMenuName());
//                        }
//                        childItem.add(arrSubMenuTypeName);
//                    }
//                    initDrawer();
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "No Records Found", Toast.LENGTH_SHORT).show();
//
//                }
//
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            catch (NullPointerException e) {
//                // TODO: handle exception
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    //*****************************
//
//    private class JSONParseFetchEntireArticle extends AsyncTask<String, String, JSONArray>
//    {
//        private ProgressDialog pDialog;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            pDialog = new ProgressDialog(NavigationDrawerActivity.this);
//            pDialog.setMessage("Fetching data, Please Wait ...");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(true);
//            pDialog.show();
//        }
//
//        @Override
//        protected JSONArray doInBackground(String... args) {
//
//            JSONParser jParser = new JSONParser();
//
//            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
//
//            params.add(new BasicNameValuePair("UserId", prefuserid));
//            // params.add(new BasicNameValuePair("MenuId", prefmenuid));
//            // Getting JSON from URL
//            jsonGetArticlesByMenuId = jParser.getJSONFromUrl(urlFetchEntireArticle, params);
//            return jsonGetArticlesByMenuId;
//        }
//
//        @Override
//        protected void onPostExecute(JSONArray jsonGetArticlesByMenuId) {
//            pDialog.dismiss();
//            try {
//                articles = new ArrayList<Articles>();
//                ArticlesListTemp = new ArrayList<Articles>();
//                articleId = new ArrayList<String>();
//                // Getting JSON Array from URL
//                //arrMenuTypeId = new ArrayList<String>();
//                // arrMenuTypeName = new ArrayList<String>();
//
//                db.deleteArticleData();
//
//                for (int i = 0; i < jsonGetArticlesByMenuId.length(); i++) {
//                    //arrSubMenuTypeId = new ArrayList<String>();
//                    //arrSubMenuTypeName = new ArrayList<String>();
//
//                    JSONObject obj = jsonGetArticlesByMenuId.getJSONObject(i);
//
//                    System.out.println(" --- response ---- " + jsonGetArticlesByMenuId);
//
//                    // Storing  JSON item in a Variable
//                    String ArticlesID = obj.getString(TAG_GetArticlesId);
//                    String ArticlesMenuID = obj.getString(TAG_GetArticlesMenuID);
//                    String ArticlesPath = obj.getString(TAG_GetArticlesPath);
//                    String ArticlesFile = obj.getString(TAG_GetArticlesFile);
//                    String ArticlesRd = obj.getString(TAG_GetArticlesRd);
//                    String ArticlesTitle = obj.getString(TAG_GetArticlesTitle);
//                    String ArticlesCommentCount = obj.getString(TAG_GetArticlesCommentCount);
//                    String ArticlesDate = obj.getString(TAG_GetArticlesDate);
//                    String ArticlesReadCount = obj.getString(TAG_GetArticlesReadCount);
//                    String ArticlesZipPath = obj.getString(TAG_GetArticlesZipPath);
//                    String ArticlesDesc = obj.getString(TAG_GetArticlesDesc);
//                    String ArticlesType = obj.getString(TAG_GetArticlesType);
//                    String ArticlesFile_ = obj.getString("ArtFile");
//                    String ArticleAporved= obj.getString("ArtApprTime");
//                    String newFormat = null;
//                    if (!ArticleAporved.equals("")) {
//
//                        String date = ArticleAporved;
//                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS");
//                        Date testDate = null;
//                        try {
//                            testDate = sdf.parse(date);
//                        } catch (Exception ex) {
//                            ex.printStackTrace();
//                        }
//                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//                        newFormat  = formatter.format(testDate);
//                        System.out.println(".....Date..." + newFormat);
//                    }
//                    Articles art = new Articles(ArticlesMenuID, ArticlesID, ArticlesTitle, ArticlesType, ArticlesDate, ArticlesReadCount,
//                            ArticlesCommentCount, ArticlesPath, ArticlesPath, ArticlesZipPath, "0", "0", "0", "0", ArticlesDesc,ArticlesFile_,newFormat);
//
//                    articles.add(art);
//                    //   Toast.makeText(getActivity(), "data inserted", Toast.LENGTH_SHORT).show();
//
//
//                    db.addArticles(art);
//
//                }
//
//
//
//                SharedPreferences prefFetchEntire= getApplicationContext().getSharedPreferences("FetchEntireArticle", Context.MODE_PRIVATE);
//                SharedPreferences.Editor edit2=prefFetchEntire.edit();
//                edit2.putString("FetchEntireArticle", "true");
//                edit2.commit();
//
//
//
//                new JSONParseGetMenus().execute();
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (NullPointerException e) {
//                // TODO: handle exception
//                e.printStackTrace();
//            }
//        }
//    }
//    //*********************************
//
//
//    public class CheckIfUpdatesAvailableformenu extends AsyncTask<String, String, JSONArray> {
//
//        @Override
//        protected JSONArray doInBackground(String... args) {
//
//            JSONParser jParser = new JSONParser();
//            CheckIfUpdatesAvailableformenu = jParser.getJSONFromUrl(urlCheckIfUpdatesAvailableformenu);
//            return CheckIfUpdatesAvailableformenu;
//        }
//        protected void onPostExecute(JSONArray CheckIfUpdatesAvailableformenu) {
//            try {
//                for (int i = 0; i < CheckIfUpdatesAvailableformenu.length(); i++)
//                {
//                    JSONObject obj = CheckIfUpdatesAvailableformenu.getJSONObject(i);
//                    System.out.println(" --- response ---- " + CheckIfUpdatesAvailableformenu);
//                    ResponseText = obj.getString(TAG_ResponseText);
//
//                }
//                if(ResponseText.equals("No Updates"))
//                {}
//                else {
//                    new JSONParseGetMenus().execute();
//
//
//
//                }
//
//            } catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
//            this.drawer.closeDrawer(GravityCompat.START);
//        }
//
//        else
//        {
//            super.onBackPressed();
////            Intent in= new Intent(Intent.ACTION_MAIN);
////            in.addCategory(Intent.CATEGORY_HOME);
////            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            startActivity(in);
//            finish();
//
//        }
//
//    }
//
////    @Override
////
////    public void onBackPressed() {
////        super.onBackPressed();
////
////        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
////            this.drawer.closeDrawer(GravityCompat.START);
////        }
////
////       else if (getFragmentManager().getBackStackEntryCount() > 0) {
////            getFragmentManager().popBackStack();
////        } else {
////            super.onBackPressed();
////            Intent intent = new Intent(Intent.ACTION_MAIN);
////            intent.addCategory(Intent.CATEGORY_HOME);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            startActivity(intent);
////        }
////    }
//
//
//
//
//
//
//
//}