package digillence.streettreat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import digillence.streettreat.R;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 27/12/16.
 */
public class ContactUs extends Activity {
    WebView webView;
    TextView Text_View_contact,Text_phone,Text_fax,Text_email,Text_website,Headerbartitle;
    Typeface font;
    ImageView headerbar_back_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        webView = (WebView) findViewById(R.id.contactuswebview1);
        Text_View_contact = (TextView) findViewById(R.id.Text_View_contact);
        Text_phone = (TextView) findViewById(R.id.Text_phone);
        Text_fax = (TextView) findViewById(R.id.Text_fax);
        Text_email = (TextView) findViewById(R.id.Text_email);
        Text_website = (TextView) findViewById(R.id.Text_website);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(ContactUs.this,Home.class);
                startActivity(in);
            }

        });



        Headerbartitle= (TextView)findViewById(R.id.Headerbartitle);
        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");
        Headerbartitle.setText(store_namestr);

        font = Typeface.createFromAsset(getAssets(),"font/fontello.ttf");

        TextView telephone = (TextView) findViewById(R.id.Telephone_TV);
        telephone.setTypeface(font);
        telephone.setText("\ue027");
        telephone.setTextColor(Color.BLACK);

        TextView fax = (TextView) findViewById(R.id.Fax_Tv);
        fax.setTypeface(font);
        fax.setText("\ue037");
        fax.setTextColor(Color.BLACK);

        TextView email = (TextView) findViewById(R.id.Email_Tv);
        email.setTypeface(font);
        email.setText("\ue035");
        email.setTextColor(Color.BLACK);

        // webView = (WebView) findViewById(R.id.contactuswebview1);

       // webView.loadUrl("https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d241197.78692788864!2d72.81099994921877!3d19.163930307959962!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xacafb7b209ebc48a!2sSTREET+TREAT+TECHNOWORKS+PVT+LTD!5e0!3m2!1sen!2sin!4v1479374442422");
        String html = "<iframe  width=\"100%\" height=\"300\" style=\"border: 1px solid #cccccc;\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d241197.78692788864!2d72.81099994921877!3d19.163930307959962!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xacafb7b209ebc48a!2sSTREET+TREAT+TECHNOWORKS+PVT+LTD!5e0!3m2!1sen!2sin!4v1479374442422\" ></iframe>";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadData(html, "text/html", null);
        contactUs();
    }

    private void contactUs() {

        StringRequest str = new StringRequest(Request.Method.POST, Urls.Contact_Us, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {

                       // Toast.makeText(ContactUs.this, jsonObject.getString("Message"), Toast.LENGTH_LONG).show();
                        String address = jsonObject.getJSONObject("items").getString("address");
                        String telephone = jsonObject.getJSONObject("items").getString("telephone");
                        String fax = jsonObject .getJSONObject("items").getString("fax");
                        String email = jsonObject .getJSONObject("items").getString("email");
                        String website = jsonObject .getJSONObject("items").getString("website");
                        Text_View_contact.setText(address);
                        Text_phone.setText(telephone);
                        Text_fax.setText(fax);
                        Text_email.setText(email);
                        Text_website.setText(website);


                    } else {
                        Toast.makeText(ContactUs.this, "", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();

                Toast.makeText(ContactUs.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
              //  params.put("title", "about us");
                return params;
            }
        };

        RequestQueue requestq = Volley.newRequestQueue(this);
        requestq.add(str);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(ContactUs.this,Home.class);
        startActivity(in);
    }
}
