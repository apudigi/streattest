package digillence.streettreat.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import digillence.streettreat.R;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class Login extends AppCompatActivity implements View.OnClickListener {


    private static final int RC_SIGN_IN = 0;
    //  TextView imgtxtFacebook;
    TextView Login_Text,Register_Text,forgotpass;
    ScrollView sc1,sc2;
    Context context;
    TextInputLayout tilEmail, tilPassword;
    EditText edtEmail, edtPassword;
    LinearLayout lnrLogin, lnrRegister, lnrRegisterDetail, lnrLoginDetail;
    Button btnSubmit, btnsubmit_register;
    View dividerRegister, dividerLogin;
    TextInputLayout tilFirstname_register, tilLastname_register, tilmobileno_register,tilPassword_register, tilConfirmpassword_register;
    EditText edtFirstname_register, edtLastname_register, edtmobileno_register,edtPassword_register, edtConfirmpassword_register, edtEmail_register;
    ToggleSwitch toggleSwitch;
    String gender = Constant.male;
    LinearLayout lnrGoogle;
    CallbackManager callbackManager;
    LoginButton loginButton;
    private GoogleApiClient mGoogleApiClient;
    private boolean signedInUser;
    private boolean mIntentInProgress;
    Dialog mProgressDialog;
    private ConnectionResult mConnectionResult;
    //  private ProgressDialog mProgressDialog;
    RelativeLayout parentLayout;
    CustomScrollVIew myScrollView;
    JSONArray jsonLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_login);
        boolean finish = getIntent().getBooleanExtra("finish", false);
        if (finish) {
            startActivity(new Intent(Login.this, Login.class));
            finish();
            return;
        }
        init();
        setFont();


        edtEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                myScrollView.setEnableScrolling(true); // enabling scrolling
                return false;
            }

        });


        edtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                myScrollView.setEnableScrolling(true); // enabling scrolling


                return false;
            }
        });



        myScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = myScrollView.getRootView().getHeight() - myScrollView.getHeight();

                if (heightDiff > 100) {
                    //Log.e("MyActivity", "keyboard opened");
                   // Toast.makeText(Login.this,"keyboard opened",Toast.LENGTH_LONG).show();
                    myScrollView.setEnableScrolling(true);


                }

                if (heightDiff < 150) {
                    // Log.e("MyActivity", "keyboard closed");
                  //  Toast.makeText(Login.this,"keyboard closed",Toast.LENGTH_LONG).show();
                    myScrollView.setEnableScrolling(false);


                }
            }
        });


    }

    private void setToggle() {

        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                // Write your code ...
                if (position == 0) {
                    gender = Constant.male;
                } else {
                    gender = Constant.female;
                }

            }
        });
    }


    @Override
    public void onBackPressed() {
        Login.this.finish();
        super.onBackPressed();

    }

    public void backPressed(View view) {
        myScrollView.setEnableScrolling(false);

        myScrollView.setFocusableInTouchMode(true);
        myScrollView.requestFocus();
        myScrollView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    myScrollView.setEnableScrolling(false);

                    return true;
                }
                return false;
            }
        });
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(),"font/Raleway-Regular.ttf");
        Login_Text.setTypeface(font);
        Register_Text.setTypeface(font);
        forgotpass.setTypeface(font);
        btnSubmit.setTypeface(font);
        edtPassword.setTypeface(font2);
        edtEmail.setTypeface(font2);
        tilEmail.setTypeface(font2);
        tilPassword.setTypeface(font2);
    }

    private void init()
    {
        context = this;
        edtEmail = (EditText) findViewById(R.id.edtEmail);


//    Typeface customFont = Typeface.createFromAsset(this.getAssets(),"font/Raleway-Bold.ttf");
        Login_Text = (TextView) findViewById(R.id.Login_Text);
//    Login_Text.setTypeface(font);
        Register_Text = (TextView) findViewById(R.id.Register_Text);
        forgotpass = (TextView) findViewById(R.id.forgotpass);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        tilEmail = (TextInputLayout) findViewById(R.id.tilEmail);
        tilPassword = (TextInputLayout) findViewById(R.id.tilPassword);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        // lnrFbLogin = (LinearLayout) findViewById(R.id.lnrFbLogin);
        dividerRegister = (View) findViewById(R.id.dividerRegister);
        dividerLogin = (View) findViewById(R.id.dividerLogin);
        lnrLogin = (LinearLayout) findViewById(R.id.lnrLogin);
        lnrRegister = (LinearLayout) findViewById(R.id.lnrRegister);
        // lnrRegisterDetail = (LinearLayout) findViewById(R.id.lnrRegisterDetail);
        lnrLoginDetail = (LinearLayout) findViewById(R.id.lnrLoginDetail);
        btnsubmit_register = (Button) findViewById(R.id.btnsubmit_register);
        myScrollView = (CustomScrollVIew) findViewById(R.id.scrollView1);

        SharedPreferences sh= getSharedPreferences("Token", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit= sh.edit();
        edit.remove("Token");
        //edit.putString("Token","");
        edit.commit();
        lnrLogin.setOnClickListener(this);
        lnrRegister.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        mProgressDialog =  new Dialog(Login.this,android.R.style.Theme_Translucent_NoTitleBar);
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setContentView(R.layout.loader_gif);
        final ImageView animatedGifImageView=(ImageView) mProgressDialog.findViewById(R.id.animatedGifImageView);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(animatedGifImageView);
        Glide.with(Login.this).load(R.drawable.loader).into(imageViewTarget);
        myScrollView.setEnableScrolling(false); // disable scrolling


    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void ForgotPassword(View v) {
        Intent intent1 = new Intent(Login.this, ForgotPassword.class);
        startActivity(intent1);
        finish();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnrLogin:
                setLogin();
                break;
            case R.id.lnrRegister:
                setRegister();
                break;
            case R.id.btnSubmit:
                new  userLogin().execute();
                break;
            case R.id.btnsubmit_register:

                if (edtFirstname_register.getText().toString().trim().length() < 1) {
                    Toast.makeText(context, "please enter first name", Toast.LENGTH_LONG).show();

                } else if (edtLastname_register.getText().toString().trim().length() < 1) {
                    Toast.makeText(context, "please enter last name", Toast.LENGTH_LONG).show();
                } else if (edtmobileno_register.getText().toString().trim().length() < 10) {
                    Toast.makeText(context, "please enter proper mobile number", Toast.LENGTH_LONG).show();
                } else if (!Constant.isEmailValid(edtEmail_register.getText())) {
                    Toast.makeText(context, "please enter proper email", Toast.LENGTH_LONG).show();
                } else if (edtPassword_register.getText().toString().trim().length() < 1) {
                    Toast.makeText(context, "please enter password", Toast.LENGTH_LONG).show();
                } else if (edtConfirmpassword_register.getText().toString().trim().length() < 1) {
                    Toast.makeText(context, "please enter confirm password", Toast.LENGTH_LONG).show();
                } else if (!edtConfirmpassword_register.getText().toString().trim().equals(edtPassword_register.getText().toString())) {
                    Toast.makeText(context, "password does not match", Toast.LENGTH_LONG).show();
                } else {
                  /*  Intent intent= new Intent(Login.this,Home.class);
                    startActivity(intent);
                    finish();*/
                    checkUniquenessEmail();
                }

                break;
        }
    }

    private void setRegister() {

        Intent i = new Intent(Login.this,Registration.class);
        this.startActivity(i);
        finish();
    }

    private void setLogin() {
        lnrLoginDetail.setVisibility(View.VISIBLE);
        dividerLogin.setVisibility(View.VISIBLE);
    }


    private void checkUniquenessEmail() {

        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.checkUniqueness,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("1")) {
                                checkUniquenessMobile();
                            } else {

                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProgressDialog.dismiss();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();

                        Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("unique_field", "email");
                params.put("value[0]", edtEmail_register.getText().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void checkUniquenessMobile() {




        mProgressDialog =  new Dialog(Login.this,android.R.style.Theme_Translucent_NoTitleBar);
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setContentView(R.layout.loader_gif);
        final ImageView animatedGifImageView=(ImageView) mProgressDialog.findViewById(R.id.animatedGifImageView);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(animatedGifImageView);
        Glide.with(Login.this).load(R.drawable.loader).into(imageViewTarget);
        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.checkUniqueness,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            mProgressDialog.dismiss();

                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("1")) {
                                userRegistration();
                                //checkUniquenessMobile();
                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } else {

                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProgressDialog.dismiss();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();

                        Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("unique_field", "mobile");
                params.put("value[0]", edtmobileno_register.getText().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void userRegistration() {


        mProgressDialog =  new Dialog(Login.this,android.R.style.Theme_Translucent_NoTitleBar);
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setContentView(R.layout.loader_gif);
        final ImageView animatedGifImageView=(ImageView) mProgressDialog.findViewById(R.id.animatedGifImageView);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(animatedGifImageView);
        Glide.with(Login.this).load(R.drawable.loader).into(imageViewTarget);
        mProgressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.registration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("1")) {
                                mProgressDialog.dismiss();

                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } else {
                                mProgressDialog.dismiss();

                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            mProgressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        mProgressDialog.dismiss();

                        Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", edtFirstname_register.getText().toString().trim());
                params.put("last_name", edtLastname_register.getText().toString());
                params.put("gender", gender);
                params.put("mobile", edtmobileno_register.getText().toString());
                params.put("email", edtEmail_register.getText().toString());
                params.put("password", edtPassword_register.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    class userLogin extends AsyncTask<String, String, JSONArray> {




        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            mProgressDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("username", edtEmail.getText().toString().trim()));
            params.add(new BasicNameValuePair("password", edtPassword.getText().toString()));

            jsonLogin = jParser.getJSONFromUrl(Urls.loginuser, params);
            return jsonLogin;
        }

        protected void onPostExecute(JSONArray jsonLogin) {
            try {

//

                for(int i=0; i<jsonLogin.length(); i++)
                {
                    JSONObject obj = jsonLogin.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {


                        String loginId=   obj.getJSONObject("items").getString("log_id");
                        String store_name= obj.getJSONObject("items").getString("store_name");
                        String user_name=   obj.getJSONObject("items").getString("name");

                        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editStoreName= sharedPrefStoreName.edit();
                        editStoreName.putString("store_name",store_name);
                        editStoreName.commit();



                        SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit= sharedPreferences.edit();
                        edit.putString("log_id", loginId);
                        edit.commit();

                        SharedPreferences sharedPrefUserName= getSharedPreferences("user_name",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editUsername=sharedPrefUserName.edit();
                        editUsername.putString("user_name",user_name);
                        editUsername.commit();



                        SharedPreferences templateIdPrefAl = getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

                        Set<String> set = new HashSet<String>();
                        editemplateIdPrefAl.clear();
                        editemplateIdPrefAl.remove("TemplateId");
                        editemplateIdPrefAl.commit();

                        Intent in= new Intent(Login.this,TestActivity.class);
                        startActivity(in);


                    }

                    else
                    {

                        mProgressDialog.dismiss();
                        Toast.makeText(Login.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                }
            } catch (JSONException e)
            {
                mProgressDialog.dismiss();

                e.printStackTrace();
            } catch (NullPointerException e)
            {
                mProgressDialog.dismiss();

                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException d)
            {}


        }
    }



}