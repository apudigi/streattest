package digillence.streettreat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.ReviewAllNotiAdapter;
import digillence.streettreat.model.Reviews;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 27/12/16.
 */
public class NotSoSad extends Activity {



    ListView lv;
    JSONArray jsonNoti;
    ReviewAllNotiAdapter _SendNotiAdapter;
    Button submit;
    JSONArray jsonSendNoti;
    String strDate,logId,templateId,storeName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_not_so_sad);
        lv= (ListView)findViewById(R.id.lv);
       // submit=(Button)findViewById(R.id.submit);

        SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId= sharedPreferences.getString("log_id", "NA");
        new NotificationTemplates().execute();
    }

    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(NotSoSad.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("log_id", logId));

            jsonNoti = jParser.getJSONFromUrl(Urls.ViewReviews, params);

            return jsonNoti;
        }
        ArrayList<Reviews> al= new ArrayList<Reviews>();

        String  description,id;
        protected void onPostExecute(JSONArray jsonNoti) {
            try {
                //  pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {
                            int rate;
                            JSONObject objj=js.getJSONObject(j);
                            Reviews noti= new Reviews();
                            rate = Integer.parseInt(objj.getString("rating"));
                            if(rate<3){
                                description =  objj.getString("comments");
                                id= objj.getString("id");
                                noti.setName(description);
                                al.add(noti);
                            }

                        }

                    }

                    else
                    {
                        Toast.makeText(NotSoSad.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                _SendNotiAdapter = new ReviewAllNotiAdapter(NotSoSad.this, al);
                lv.setAdapter(_SendNotiAdapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }



    }
}
