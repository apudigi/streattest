package digillence.streettreat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.FavAdapter;
import digillence.streettreat.adapter.SendNotiAdapter;
import digillence.streettreat.model.FavModel;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 5/1/17.
 */
public class MyFav extends Activity {

    ListView lv;
    JSONArray jsonNoti;
    FavAdapter Fav_Adapter;
    Button submit;
    JSONArray jsonSendNoti;
    String strDate,logId,templateId,storeName;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);
        lv= (ListView)findViewById(R.id.lv);
        imageView = (ImageView) findViewById(R.id.imageView);
        // submit=(Button)findViewById(R.id.submit);

        SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId= sharedPreferences.getString("log_id", "NA");
        new NotificationTemplates().execute();
    }

    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MyFav.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("log_id", logId));

            jsonNoti = jParser.getJSONFromUrl(Urls.ViewFav, params);

            return jsonNoti;
        }
        ArrayList<FavModel> al= new ArrayList<FavModel>();
        ArrayList<String> All_List = new ArrayList();
        ArrayList<String> Happy_List = new ArrayList();
        ArrayList<String> Sad_List = new ArrayList();
        String  description,image;
        Integer rate;
        protected void onPostExecute(JSONArray jsonNoti) {

            try {
                //  pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
//                    String rating = obj.getString("rating");

                    if(status.equals("1") )
                    {
                        JSONArray js= obj.getJSONArray("items");

                        for(int j=0; j<js.length();j++)
                        {
//                          int rate;
                            JSONObject objj=js.getJSONObject(j);
                            FavModel noti= new FavModel();
//                            rate = Integer.parseInt(objj.getString("rating"));
//                            if(rate<3) {
//                                Happy_List.add(String.valueOf(objj));
                            description =  objj.getString("user_name");
                            image = objj.getString("profile_pic");
                            noti.setName(description);
                            noti.setIamge(image);
                            al.add(noti);


                        }

                    }

                    else
                    {
                        Toast.makeText(MyFav.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                Fav_Adapter = new FavAdapter(MyFav.this, al);
                lv.setAdapter(Fav_Adapter);


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }



    }

}
