package digillence.streettreat.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import digillence.streettreat.R;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.Urls;

public class Profile extends AppCompatActivity {
    Context context;
    private ProgressDialog progress;
    TextView txtName,txtMail,txtMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        getUser();
        //showDialog();

    }



    private void init() {
        context=this;
        txtName= (TextView) findViewById(R.id.txtName);
        txtMail= (TextView) findViewById(R.id.txtMail);
        txtMobile= (TextView) findViewById(R.id.txtMobile);
        progress = new ProgressDialog(this);
        progress.setMessage("Wait");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void showDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_color_picker);
        TextView txtImgOne = (TextView) dialog.findViewById(R.id.txtImgOne);
        TextView txtImgTwo = (TextView) dialog.findViewById(R.id.txtImgTwo);
        TextView txtImgThree = (TextView) dialog.findViewById(R.id.txtImgThree);
        TextView txtImgFour = (TextView) dialog.findViewById(R.id.txtImgFour);
        TextView txtImgFive = (TextView) dialog.findViewById(R.id.txtImgFive);
        TextView txtImgSix = (TextView) dialog.findViewById(R.id.txtImgSix);
        TextView txtImgSeven = (TextView) dialog.findViewById(R.id.txtImgSeven);
        TextView txtImgeight = (TextView) dialog.findViewById(R.id.txtImgeight);
        TextView txtImgnine = (TextView) dialog.findViewById(R.id.txtImgnine);
        TextView txtImgTen = (TextView) dialog.findViewById(R.id.txtImgTen);
        TextView txtImgEleven = (TextView) dialog.findViewById(R.id.txtImgEleven);
        TextView txtImgTwelve = (TextView) dialog.findViewById(R.id.txtImgTwelve);
        txtImgOne.setTypeface(Constant.imgFont(context));
        txtImgTwo.setTypeface(Constant.imgFont(context));
        txtImgThree.setTypeface(Constant.imgFont(context));
        txtImgFour.setTypeface(Constant.imgFont(context));
        txtImgFive.setTypeface(Constant.imgFont(context));
        txtImgSix.setTypeface(Constant.imgFont(context));
        txtImgSeven.setTypeface(Constant.imgFont(context));
        txtImgeight.setTypeface(Constant.imgFont(context));
        txtImgnine.setTypeface(Constant.imgFont(context));
        txtImgTen.setTypeface(Constant.imgFont(context));
        txtImgEleven.setTypeface(Constant.imgFont(context));
        txtImgTwelve.setTypeface(Constant.imgFont(context));
        //txtImgFirst.setText("0xe803");


        dialog.show();

    }
    private void getUser() {
        progress.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("1")) {
                                progress.dismiss();
                                txtName.setText(jsonObject.getJSONObject("items").getString("name"));
                                txtMail.setText(jsonObject.getJSONObject("items").getString("email"));
                                txtMobile.setText(jsonObject.getJSONObject("items").getString("mobile"));
                               /* jsonObject.getJSONObject("items").getString("address_1");
                                jsonObject.getJSONObject("items").getString("address_2");
                                jsonObject.getJSONObject("items").getString("city");
                                jsonObject.getJSONObject("items").getString("country");
                                jsonObject.getJSONObject("items").getString("zip_code");
                                jsonObject.getJSONObject("items").getString("travel_class");*/
                                Toast.makeText(Profile.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } else {
                                progress.dismiss();

                                Toast.makeText(Profile.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            progress.dismiss();

                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();

                        Toast.makeText(Profile.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("log_id", Constant.getLoginId(context));

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
