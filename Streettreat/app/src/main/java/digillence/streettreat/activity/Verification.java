package digillence.streettreat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import digillence.streettreat.R;
import digillence.streettreat.util.Constant;

/**
 * Created by devashree on 22/6/16.
 */
public class Verification extends Activity  implements View.OnClickListener{
     Context context;
     TextInputLayout tilOTP;
    ImageView back_toolbar;
    TextView txtResend_OTP,txtImgNotification;
    EditText edtOTP;
    Button btnVerify_OTP;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification);
        init();
        setFont();

    }

    private void init()
    {
         tilOTP =(TextInputLayout) findViewById(R.id.tilOTP);
         edtOTP=(EditText) findViewById(R.id.edtOTP);
         txtResend_OTP= (TextView) findViewById(R.id.txtResend_OTP);
        txtImgNotification=(TextView) findViewById(R.id.txtImgNotification);
         txtResend_OTP.setOnClickListener(this);
         btnVerify_OTP =(Button) findViewById(R.id.btnVerify_OTP);

        back_toolbar=(ImageView) findViewById(R.id.back_toolbar);

        back_toolbar.setOnClickListener(this);
         btnVerify_OTP.setOnClickListener(this);
    }

    private void setFont()
    {
       // forgotpassimage.setTypeface(Constant.imgFont(this));
        txtImgNotification.setTypeface(Constant.imgFont(this));
    }
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txtResend_OTP:

                Constant.DisplayToast(this,"RESEND OTP");


               // finish();
                break;
            case R.id.back_toolbar:
                finish();
                break;

            case R.id.btnVerify_OTP:

                Constant.DisplayToast(this,"Verify OTP");
                Intent intent= new Intent(this,Contact.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
