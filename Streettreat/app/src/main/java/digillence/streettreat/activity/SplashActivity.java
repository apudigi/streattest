package digillence.streettreat.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import digillence.streettreat.R;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
                String logId= sharedPreferences.getString("log_id","NA");
                if(logId.equals("NA"))
                {
                    Intent i = new Intent(SplashActivity.this, Login.class);
               startActivity(i);
                }

                else
                {
                    Intent i = new Intent(SplashActivity.this, TestActivity.class);
                startActivity(i);
                }








                finish();
            }
        }, SPLASH_TIME_OUT);
    }








}
