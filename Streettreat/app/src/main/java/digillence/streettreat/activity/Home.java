package digillence.streettreat.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import digillence.streettreat.R;
import digillence.streettreat.adapter.MyPagerAdapter;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.CustomTypefaceSpan;

public class Home extends AppCompatActivity implements View.OnClickListener{

    ViewPager viewPager;
    LinearLayout lnrHome,lnrCollection,lnrNearMe;
    Context context;
    private ListView mDrawerList;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String[] mNavigationDrawerItemTitles;

    private NavigationView mNavigationView;
    private DrawerLayout drawerLayout;
    ImageView headerbar_back_toolbar;
    ImageView txtImgNotification;
    TextView Headerbartitle,aboutUsTitleTextView,Report,dashboard;
    TextView myCoupans_icon,Report_icon,Dashboard_icon,Review_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        Log.d("click1", "log _id " + Constant.getLoginId(context));
        changeTypeface(mNavigationView);

        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");

        txtImgNotification.setVisibility(View.VISIBLE);
        txtImgNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Home.this, NotificationList.class);
                startActivity(in);

            }
        });
        Review_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Home.this,"hgujdghs",Toast.LENGTH_SHORT).show();
            }
        });

        headerbar_back_toolbar=(ImageView)findViewById(R.id.headerbar_back_toolbar);
        headerbar_back_toolbar.setVisibility(View.INVISIBLE);


        Headerbartitle= (TextView)findViewById(R.id.Headerbartitle);


        Headerbartitle.setText(store_namestr);

        setFont();

    }


    private void setFont()
    {   Typeface font = Typeface.createFromAsset(getAssets(), "font/Raleway-Bold.ttf");
        Typeface font3 = Typeface.createFromAsset(getAssets(),"font/fontello.ttf");

//        aboutUsTitleTextView.setTypeface(font);
//        Report.setTypeface(font);
//        dashboard.setTypeface(font);
//        Review_Footer.setTypeface(font);

        myCoupans_icon.setTypeface(font3);
        myCoupans_icon.setText("\ue028");
        myCoupans_icon.setTextColor(Color.BLACK);

        Report_icon.setTypeface(font3);
        Report_icon.setText("\ue070");
        Report_icon.setTextColor(Color.BLACK);


        Dashboard_icon.setTypeface(font3);
        Dashboard_icon.setText("\ue030");
        Dashboard_icon.setTextColor(Color.BLACK);

        Review_icon.setTypeface(font3);
        Review_icon.setText("\ue071");
        Review_icon.setTextColor(Color.BLACK);


//        forgotpassimage.setTypeface(font3);
//        forgotpassimage.setText("\ue039");
    }

    private void init() {
        context=this;

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        lnrHome = (LinearLayout) findViewById(R.id.lnrHome);
        lnrCollection = (LinearLayout) findViewById(R.id.lnrCollection);
        lnrNearMe = (LinearLayout) findViewById(R.id.lnrNearMe);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        drawerLayout= (DrawerLayout)findViewById(R.id.drawer_layout);
        txtImgNotification = (ImageView)findViewById(R.id.headerbar_noti);

        drawerLayout.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);


        aboutUsTitleTextView= (TextView)findViewById(R.id.aboutUsTitleTextView);
     //   Report= (TextView)findViewById(R.id.Report);
//        dashboard= (TextView)findViewById(R.id.dashboard);
//        Review_Footer= (TextView)findViewById(R.id.Review_Footer);


        myCoupans_icon= (TextView)findViewById(R.id.myCoupans_icon);
        Report_icon= (TextView)findViewById(R.id.Report_icon);
        Dashboard_icon= (TextView)findViewById(R.id.Dashboard_icon);
        Review_icon= (TextView)findViewById(R.id.Review_icon);

//        TextView Report = (ImageView) findViewById(R.id.report);

       // mNavigationView.setItemIconTintList(null);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


                                                              @Override
                                                              public boolean onNavigationItemSelected(MenuItem item) {


                                                                  switch (item.getItemId()){

                                                                      case R.id.aboutus:
                                                                          Intent  in= new Intent(Home.this,AboutUs.class);
                                                                          startActivity(in);
                                                                          break;


                                                                      case R.id.sendNoti:
                                                                          Intent insend= new Intent(Home.this,Notificationo.class);
                                                                          startActivity(insend);
                                                                          break;
                                                                      case R.id.myfav:
                                                                          Intent fav= new Intent(Home.this,MyFav.class);
                                                                          startActivity(fav);
                                                                          break;


//                                                                      case R.id.myProfile:
//                                                                          Intent inprofile= new Intent(Home.this,MyProfile.class);
//                                                                          startActivity(inprofile);
//
//                                                                      case R.id.myCoupans:
//                                                                          Intent myCoupans= new Intent(Home.this,MyCoupans.class);
//                                                                          startActivity(myCoupans);
//
//                                                                      case R.id.myOffers:
//                                                                          Intent offers= new Intent(Home.this,MyOffers.class);
//                                                                          startActivity(offers);
//
//                                                                      case R.id.bucketCat:
//                                                                          Intent bucketCat= new Intent(Home.this,bucketCat.class);
//                                                                          startActivity(bucketCat);
//
//                                                                      case R.id.sendNoti:
//                                                                          Intent sendNoti= new Intent(Home.this,sendNoti.class);
//                                                                          startActivity(sendNoti);
//
//                                                                      case R.id.viewFav:
//                                                                          Intent viewFav= new Intent(Home.this,viewFav.class);
//                                                                          startActivity(viewFav);
//
                                                                      case R.id.faqs:
                                                                          Intent faqs= new Intent(Home.this,ActivityFAQ.class);
                                                                          startActivity(faqs);
                                                                          break;

                                                                      case R.id.tc:
                                                                          Intent tc= new Intent(Home.this,TermsAndConditions.class);
                                                                          startActivity(tc);
                                                                          break;

                                                                      case R.id.contactUs:
                                                                          Intent contactus= new Intent(Home.this,ContactUs.class);
                                                                          startActivity(contactus);
                                                                          break;

//                                                                      case R.id.News_and_Events:
//                                                                          Intent newsAndEvents= new Intent(Home.this,NewsandEvents.class);
//                                                                          startActivity(newsAndEvents);
//                                                                          break;

                                                                      case R.id.lnrHome:
                                                                          Toast.makeText(getApplicationContext(), "Stared Selected", Toast.LENGTH_SHORT).show();
                                                                          return true;

//                                                                      case R.id.Report:
//                                                                          Intent report= new Intent(Home.this,ActivityReport.class);
//                                                                          startActivity(report);
//                                                                          break;



//                                                                      case R.id.aboutus:
//                                                                          Intent contactUs= new Intent(Home.this,contactUs.class);
//                                                                          startActivity(contactUs);
//
//                                                                      case R.id.rateapp:
//                                                                          Intent rateapp= new Intent(Home.this,rateapp.class);
//                                                                          startActivity(rateapp);
//
//                                                                      case R.id.help:
//                                                                          Intent help= new Intent(Home.this,help.class);
//                                                                          startActivity(help);
//
//
//
//                                                                      case R.id.logout:
//                                                                          Intent logout= new Intent(Home.this,logout.class);
//                                                                          startActivity(logout);


                                                                        //  break;

                                                                  }


                                                                  return false;
                                                              }


                                                          } );

        MyPagerAdapter adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        //viewPager.setAdapter(adapterViewPager);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        lnrHome.setOnClickListener(this);
        lnrCollection.setOnClickListener(this);
        lnrNearMe.setOnClickListener(this);
    }


    private void changeTypeface(NavigationView navigationView) {
        Constant fontTypeface = new Constant(this);
        Typeface typeface = fontTypeface.getTypefaceAndroid();

        MenuItem item;

        item = navigationView.getMenu().findItem(R.id.aboutus);
       // item.setTitle("Camera");
        applyFontToItem(item, typeface);
    }

    private void applyFontToItem(MenuItem item, Typeface font) {
        SpannableString mNewTitle = new SpannableString(item.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font, 16), 0,
                mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        item.setTitle(mNewTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
//            case R.id.Report:
//                Intent report= new Intent(Home.this,ActivityReport.class);
//                startActivity(report);
//                break;
            case R.id.lnrCollection:
                viewPager.setCurrentItem(1);
                break;
            case R.id.lnrNearMe:
                viewPager.setCurrentItem(2);
                break;
        }
    }

    @Override
    public void onBackPressed() {
            Intent in= new Intent();
        finish();

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent in= new Intent(Home.this,Login.class);
//        startActivity(in);
//        closeNavDrawer();
//
//    }


//
//    @Override
//    public void onBackPressed() {
//        if (isNavDrawerOpen()) {
//            closeNavDrawer();
//            Intent in=new Intent(Home.this,Login.class);
//            startActivity(in);
//
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    protected boolean isNavDrawerOpen() {
//        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
//    }
//







    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }



}









































