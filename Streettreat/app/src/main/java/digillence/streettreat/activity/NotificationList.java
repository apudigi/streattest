package digillence.streettreat.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.NotificationListAdapter;
import digillence.streettreat.model.NotificationsGet;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class NotificationList extends AppCompatActivity
{

    ListView notificationList;
    NotificationListAdapter notificationListAdapter;
    JSONArray jsonNoti;
    TextView noOfCounts;
   ImageView headerbar_back_toolbar;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        notificationList= (ListView)findViewById(R.id.notificationList);
        noOfCounts= (TextView)findViewById(R.id.noOfCounts);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);

        new getNotificationsPreviuously().execute();
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NotificationList.this, Home.class);
                startActivity(in);
            }
        });
    }
    class getNotificationsPreviuously extends AsyncTask<String, String, JSONArray> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NotificationList.this);
            pDialog.setMessage("Please Wait ...");

            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
       String     logId= sharedPreferences.getString("log_id", "NA");

            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));


            jsonNoti = jParser.getJSONFromUrl(Urls.getNotifications, params);

            return jsonNoti;
        }
        String description, id,sendDate;
        ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();

        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                  pDialog.dismiss();

                for (int i = 0; i < jsonNoti.length(); i++) {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if (status.equals("1")) {
                        SharedPreferences sharedPrefStoreName = getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName = getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName = sharedPrefUserName.getString("user_name", "");

                        JSONArray js = obj.getJSONArray("items");
                        for (int j = 0; j < js.length(); j++) {

                            JSONObject objj = js.getJSONObject(j);
                            NotificationsGet noti = new NotificationsGet();
                            description = objj.getString("description");
                            sendDate=objj.getString("send_date");
                            id = objj.getString("id");
                            noti.setCode(id);
                            noti.setsendDate(sendDate);
                            String replaced;
                            if (description.contains("{user}") || description.contains("{store}")) {
                                replaced = description.replace("{user}", userName);
                                replaced = replaced.replace("{store}", storeName);
                                noti.setName(replaced);
                            } else if (description.contains("{store}")) {
                                replaced = description.replace("{store}", storeName);

                                noti.setName(replaced);
                            } else {
                                description = description;
                                noti.setName(description);
                            }
                            //noti.setName(description);
                            sendPrevious.add(noti);

                        }

                    } else {
                        Toast.makeText(NotificationList.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                }

                int noOfCounts_=   sendPrevious.size();
                String noOfCountsstr= String.valueOf(noOfCounts_+"Notifications");

                noOfCounts.setText(noOfCountsstr);
                notificationListAdapter = new NotificationListAdapter(NotificationList.this, sendPrevious);
                notificationList.setAdapter(notificationListAdapter);


                notificationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String id_ = sendPrevious.get(position).getCode().toString();

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch(IllegalArgumentException ds)
            {}

        }
    }

}
