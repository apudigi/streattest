package digillence.streettreat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class ConfirmPwdActivity extends AppCompatActivity {

    EditText newpwd,edtconfrmpwd;
    Button btnchangepwd;
    String token,confirmpwd, newPwd;
    JSONArray jsonchangePassword;
    TextView forgotpassingimage,forgotpassimage;
    TextView Headerbartitle,textView2;
    ImageView headerbar_back_toolbar;
    private static Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pwd);
        newpwd= (EditText)findViewById(R.id.newpwd);
        edtconfrmpwd=(EditText) findViewById(R.id.edtconfrmpwd);
        btnchangepwd=(Button) findViewById(R.id.btnchangepwd);

        forgotpassimage=(TextView) findViewById(R.id.forgotpassimage);
        textView2 = (TextView) findViewById(R.id.textView2);
        toast = new Toast(this);


        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);
        Headerbartitle=(TextView) findViewById(R.id.Headerbartitle);
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(ConfirmPwdActivity.this,ForgotPassword.class);
                startActivity(in);
                if (toast != null)
                    toast.cancel();

            }
        });
        Headerbartitle.setVisibility(View.INVISIBLE);
        forgotpassingimage= (TextView)findViewById(R.id.forgotpassimage);
        SharedPreferences sh= getSharedPreferences("Token", Context.MODE_PRIVATE);
        token=sh.getString("Token", "NA");

        Log.d("digi",token);

        btnchangepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard(ConfirmPwdActivity.this);
                confirmpwd=edtconfrmpwd.getText().toString();
                newPwd= newpwd.getText().toString();

                if(confirmpwd.equals(newPwd))
                {
                    new changePassword().execute();
                }
                else
                {
                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getBaseContext(),"password does not match", Toast.LENGTH_LONG);
                    toast.show();

                }

            }
        });
        setFont();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void setFont()
    {
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(),"font/Raleway-Regular.ttf");
        Typeface  font3 = Typeface.createFromAsset(getAssets(),"font/fontello.ttf");
        btnchangepwd.setTypeface(font);
        textView2.setTypeface(font);
        forgotpassimage.setTypeface(font3);
        forgotpassimage.setText("\ue039");
        newpwd.setTypeface(font2);
        edtconfrmpwd.setTypeface(font2);
    }

    class changePassword extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(ConfirmPwdActivity.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("token", token));
            params.add(new BasicNameValuePair("password", confirmpwd));
            jsonchangePassword = jParser.getJSONFromUrl(Urls.ChangePassword, params);

            return jsonchangePassword;

        }
        protected void onPostExecute(JSONArray jsonchangePassword) {
            try {

                for(int i=0; i<jsonchangePassword.length(); i++)
                {
                    JSONObject obj = jsonchangePassword.getJSONObject(i);

                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        pDialog.dismiss();
                        Intent in= new Intent(ConfirmPwdActivity.this,Login.class);
                        startActivity(in);
                    }
                    else
                    {
                        pDialog.dismiss();
                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getBaseContext(),obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {

            }

        }
    }

}