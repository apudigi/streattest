package digillence.streettreat.activity;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import digillence.streettreat.R;

/**
 * Created by devashree on 28/12/16.
 */
public class ActivityReport extends ListActivity {


        ListView list;
    TextView Headerbartitle;
    ImageView headerbar_back_toolbar;
        String[] itemname ={
                "REPORT 0",
                "REPORT 1",
                "REPORT 2",
                "REPORT 3",
                "REPORT 4",
                "REPORT 5",
                "REPORT 6",
                "REPORT 7"
        };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        headerbar_back_toolbar=(ImageView) findViewById(R.id.headerbar_back_toolbar);
        headerbar_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(ActivityReport.this,Home.class);
                startActivity(in);
            }

        });

        Headerbartitle= (TextView)findViewById(R.id.Headerbartitle);
        SharedPreferences sharedPrefStoreName= getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");
        Headerbartitle.setText(store_namestr);


        this.setListAdapter(new ArrayAdapter<String>(this, R.layout.row_list_item, R.id.REPORT_TEXT_VIEW,itemname));

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in= new Intent(ActivityReport.this,Home.class);
        startActivity(in);
    }
}
