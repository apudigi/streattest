package digillence.streettreat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.ListRecyclerAdapter;
import digillence.streettreat.model.Reviews;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

/**
 * Created by devashree on 27/12/16.
 */
public class All extends Activity  {
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
////        return super.onCreateView(inflater, container, savedInstanceState);
//        return inflater.inflate(R.layout.fragment_all,container,false);
//
////        ArrayList<String> arrayList = new ArrayList<String>();

    TextView post_view;
    EditText post_reply;
    Context context;
   // ListView lv;

    RecyclerView rvDeals;

    JSONArray jsonNoti;
    ListRecyclerAdapter _SendNotiAdapter;
    Button post_btn;
    JSONArray jsonSendNoti;
    String strDate,logId,templateId,storeName,replyId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_all);

        rvDeals = (RecyclerView) findViewById(R.id.rvDeals_);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rvDeals.setLayoutManager(mLayoutManager);
        rvDeals.setItemAnimator(new DefaultItemAnimator());

        SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId= sharedPreferences.getString("log_id", "NA");
        SharedPreferences sharePreferences= getSharedPreferences("id", Context.MODE_PRIVATE);
        replyId= sharePreferences.getString("id", "NA");
        new NotificationTemplates().execute();
    }

    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(All.this);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            SharedPreferences sharedPreferences= getSharedPreferences("log_id", Context.MODE_PRIVATE);
         String   logId= sharedPreferences.getString("log_id", "NA");


            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("log_id", logId));
            jsonNoti = jParser.getJSONFromUrl(Urls.ViewReviews, params);
            return jsonNoti;
        }
        ArrayList<Reviews> al= new ArrayList<Reviews>();
        String  description,id,reply;
        Integer rate;

        
        protected void onPostExecute(JSONArray jsonNoti) {

            try {
                //  pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");

                    if(status.equals("1") )
                    {
                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Reviews noti= new Reviews();
                            rate = Integer.parseInt(objj.getString("rating"));

                                description =  objj.getString("comments");
                                reply =  objj.getString("reply_text");
                                noti.setName(description);
                                noti.setComment(reply);
                                al.add(noti);


                        }

                    }

                    else
                    {
                        Toast.makeText(All.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

              _SendNotiAdapter = new ListRecyclerAdapter(All.this,al);
                rvDeals.setAdapter(_SendNotiAdapter);





            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }



    }



}
