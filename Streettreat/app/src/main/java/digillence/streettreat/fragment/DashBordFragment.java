package digillence.streettreat.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import digillence.streettreat.R;
import digillence.streettreat.adapter.CollectionAdapter;
import digillence.streettreat.adapter.DealsAdapter;
import digillence.streettreat.adapter.ExibhitionAdapter;
import digillence.streettreat.model.CollectionModel;
import digillence.streettreat.model.DealsModel;
import digillence.streettreat.model.ExhibitionModel;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.GPSTracker;

/**
 * Created by nayan on 22/6/16.
 */
public class DashBordFragment extends android.support.v4.app.Fragment {
    // Store instance variables

    int[] mResources;
    ViewPager viewPager, vpExibhistion;
    int currentPage;
    Timer swipeTimer;
    Context context;
    ImageView txtImgNotification;
    TextView txtLocation;
    RecyclerView rvDeals, rvCollection;
    DealsAdapter dealsAdapter;
    TextView HighStreet,Headerbartitle;
    ImageView headerbar_back_toolbar;
    CollectionAdapter collectionAdapter;
    ArrayList<DealsModel> dealList = new ArrayList<>();
    private ArrayList<CollectionModel> collectionList = new ArrayList<>();
    private ArrayList<ExhibitionModel> exhibitionList = new ArrayList<>();
    private ProgressDialog progress;

    // newInstance constructor for creating fragment with arguments
    public static DashBordFragment newInstance(int page, String title) {
        DashBordFragment fragmentFirst = new DashBordFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }
    public DashBordFragment()
    {
    }



    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashbord, container, false);
        init(view);
        SharedPreferences sharedPrefStoreName= getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String store_namestr= sharedPrefStoreName.getString("store_name","NA");


        //setFont();
//        getLocation();


//
        return view;
    }

    private void getLocation() {
        GPSTracker gpsTracker= new GPSTracker(context);
        double lat = gpsTracker.getLatitude();
        double lang = gpsTracker.getLongitude();
        if(lat>0  || lang>0 )
        {
//            getLocality(lat,lang);
        }

    }


    private void setFont() {

      //  txtImgNotification.setTypeface(Constant.imgFont(context));
    }

    private void init(View view) {
        context = getActivity();
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        vpExibhistion = (ViewPager) view.findViewById(R.id.vpExibhistion);
        txtImgNotification = (ImageView) view.findViewById(R.id.headerbar_noti);
        rvDeals = (RecyclerView) view.findViewById(R.id.rvDeals);
        txtLocation = (TextView) view.findViewById(R.id.txtLocation);
        rvCollection = (RecyclerView) view.findViewById(R.id.rvCollection);
        progress=Constant.progress(context);
        setSlideShow(view);
        setRv();
        setRvCollection();
        setExibhition();

    }

    private void setExibhition() {
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        exhibitionList.add(new ExhibitionModel(R.drawable.promo1, "Fashion Expo", "Monday, 4July 2016"));
        ExibhitionAdapter exibhitionAdapter = new ExibhitionAdapter(context, exhibitionList);
        vpExibhistion.setAdapter(exibhitionAdapter);
    }

    private void setRv() {
        dealsAdapter = new DealsAdapter(dealList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvDeals.setLayoutManager(mLayoutManager);
        rvDeals.setItemAnimator(new DefaultItemAnimator());
        rvDeals.setAdapter(dealsAdapter);
        callWebDeals();
    }

    private void setRvCollection() {
        collectionAdapter = new CollectionAdapter(collectionList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        rvCollection.setLayoutManager(gridLayoutManager);
        rvCollection.setHasFixedSize(true);
        rvCollection.setItemAnimator(new DefaultItemAnimator());
        rvCollection.setAdapter(collectionAdapter);
        callWebCollection();
    }

    private void callWebCollection() {
        collectionList.add(new CollectionModel("WINTER COLLECTION"));
        collectionList.add(new CollectionModel("HOLI COLLECTION"));
        collectionList.add(new CollectionModel("SUMMUR COLLECTION"));
        collectionList.add(new CollectionModel("WINTER COLLECTION"));
        collectionList.add(new CollectionModel("SUMMUR SALES"));
        collectionAdapter.notifyDataSetChanged();
    }

    private void callWebDeals() {
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealList.add(new DealsModel("20", "Mens T-Shirt"));
        dealList.add(new DealsModel("35", "Kids jeans"));
        dealList.add(new DealsModel("15", "Girls T-Shirt"));
        dealsAdapter.notifyDataSetChanged();

    }

    private void setSlideShow(View view) {
        mResources = new int[]{
                R.drawable.promo1,
                R.drawable.promo2,
                R.drawable.promo3,
                R.drawable.promo4,

        };
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(context);
        viewPager.setAdapter(customPagerAdapter);
        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.titles);
        circlePageIndicator.setViewPager(viewPager);
        slideShow();
    }

    private void slideShow() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                currentPage = viewPager.getCurrentItem();
                if (currentPage == 3) {
                    viewPager.setCurrentItem(0, true);
                } else {
                    //viewPager.setCurrentItem(currentPage + 1, true);
                }
            }
        };

        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 4000);
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_items, container, false);

           // ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
           // imageView.setImageResource(mResources[position]);

            //container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
    public void getLocality(double latitude,double longitude)
    {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
            //String state = addresses.get(0).getAdminArea();
        //String country = addresses.get(0).getCountryName();
        //String postalCode = addresses.get(0).getPostalCode();
        //String knownName = addresses.get(0).getFeatureName();
           // txtLocation.setText(address+","+city);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


