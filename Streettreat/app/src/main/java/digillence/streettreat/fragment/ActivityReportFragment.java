package digillence.streettreat.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import digillence.streettreat.R;


public class ActivityReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ImageView headerbar_back_toolbar;
    String[] itemname ={
            "REPORT 0",
            "REPORT 1",
            "REPORT 2",
            "REPORT 3",
            "REPORT 4",
            "REPORT 5",
            "REPORT 6",
            "REPORT 7"
    };

    View rootview;
    ListView list;


    public ActivityReportFragment() {
    }

    public static ActivityReportFragment newInstance(String param1, String param2) {
        ActivityReportFragment fragment = new ActivityReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview= inflater.inflate(R.layout.activity_report, container, false);

        list=(ListView) rootview.findViewById(R.id.list);

        ArrayAdapter<String> test = new ArrayAdapter<String>(getActivity(), R.layout.row_list_item, R.id.REPORT_TEXT_VIEW, itemname);
        list.setAdapter(test);


        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View   myCoupans_icon_=  getActivity().findViewById(R.id.myCoupans_icon);

      View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
      View Review_icon_= getActivity().findViewById(R.id.Review_icon);


        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.WHITE);
        }
        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);

        }

       if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
         if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;

            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }

        return  rootview;

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setVisible(true);
        menu.findItem(R.id.Notificationbell).setVisible(true);
        menu.findItem(R.id.menu_item_back).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
    private FragmentActivity myContext;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//                    } else {
//                        // error in creating fragment
//                    }

                    android.support.v4.app.FragmentManager fragmentManager = myContext.getSupportFragmentManager();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    HomeFragment fragment = new HomeFragment();
                    fragmentTransaction.replace(R.id.content_frame, fragment).addToBackStack("");
                    fragmentTransaction.commit();



                    return true;
                }

                return false;

            }
        });
    }




}
