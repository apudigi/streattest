package digillence.streettreat.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import digillence.streettreat.R;
import digillence.streettreat.util.Urls;


public class AboutUsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView tv;

    private OnFragmentInteractionListener mListener;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

            @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_aboutus, container, false);
        tv = (TextView)rootView.findViewById(R.id.aboutustext);
        aboutUs();

                View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
                View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
                View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
                View Review_icon_= getActivity().findViewById(R.id.Review_icon);

                if( myCoupans_icon_ instanceof TextView) {
                    TextView myCoupans_icon = (TextView) myCoupans_icon_;
                    Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
                    myCoupans_icon.setTypeface(font3);
                    myCoupans_icon.setText("\ue028");
                    myCoupans_icon.setTextColor(Color.GRAY);
                }
                if( Report_icon_ instanceof TextView) {
                    TextView Report_icon = (TextView) Report_icon_;
                    Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
                    Report_icon.setTypeface(font3);
                    Report_icon.setText("\ue070");
                    Report_icon.setTextColor(Color.GRAY);
                }
                if(Dashboard_icon_ instanceof TextView )
                {
                    TextView Dashboard_icon = (TextView) Dashboard_icon_;
                    Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
                    Dashboard_icon.setTypeface(font3);
                    Dashboard_icon.setText("\ue030");
                    Dashboard_icon.setTextColor(Color.GRAY);
                }
                if(Review_icon_ instanceof TextView)
                {
                    TextView Review_icon = (TextView) Review_icon_;
                    Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
                    Review_icon.setTypeface(font3);
                    Review_icon.setText("\ue071");
                    Review_icon.setTextColor(Color.GRAY);
                }




                return  rootView;

    }

    private void aboutUs(){
        StringRequest sr = new StringRequest(Request.Method.POST, Urls.aboutus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        String content=  jsonObject.getJSONObject("items").getString("content");

                        tv.setText(Html.fromHtml(content));

                    } else {
                        //  progress.dismiss();
                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    //  progress.dismiss();

                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();

                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title","about us");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(sr);

    }

    public void onBackPressed() {
        new AlertDialog.Builder(getActivity()).setMessage("Are you sure you want to exit?").setCancelable(false).setPositiveButton("YES"
                , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();


            }
        }).setNegativeButton("No", null).show();
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                return false;
            }
        });
        menu.findItem(R.id.Notificationbell).setIcon(null);
      //  menu.findItem(R.id.menu_title).setTitle("                       ");
        menu.findItem(R.id.menu_title).setTitle("                                               ");

        super.onPrepareOptionsMenu(menu);
    }

//    @Override
//    public void onResume() {
//
//        super.onResume();
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
//
//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//                    } else {
//                        // error in creating fragment
//
//
//                    }
//
//                    return true;
//                }
//
//                return false;
//
//            }
//        });
//    }























    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
