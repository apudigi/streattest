package digillence.streettreat.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import digillence.streettreat.R;
import digillence.streettreat.util.Urls;


public class ContactUsFragment extends Fragment {

    View rootView;
    WebView webView;
    TextView Text_View_contact,Text_phone,Text_fax,Text_email,Text_website;
    Typeface font;
    ProgressDialog prDialog;

    public ContactUsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       rootView  =inflater.inflate(R.layout.activity_contact_us, container, false);

        webView = (WebView) rootView.findViewById(R.id.contactuswebview1);
        Text_View_contact = (TextView)rootView. findViewById(R.id.Text_View_contact);
        Text_phone = (TextView)rootView. findViewById(R.id.Text_phone);
        Text_fax = (TextView) rootView.findViewById(R.id.Text_fax);
        Text_email = (TextView) rootView.findViewById(R.id.Text_email);
        Text_website = (TextView) rootView.findViewById(R.id.Text_website);

        font = Typeface.createFromAsset(getActivity().getAssets(),"font/fontello.ttf");

        TextView telephone = (TextView)rootView. findViewById(R.id.Telephone_TV);
        telephone.setTypeface(font);
        telephone.setText("\ue027");
        telephone.setTextColor(Color.BLACK);

        TextView fax = (TextView) rootView.findViewById(R.id.Fax_Tv);
        fax.setTypeface(font);
        fax.setText("\ue037");
        fax.setTextColor(Color.BLACK);

        TextView email = (TextView) rootView.findViewById(R.id.Email_Tv);
        email.setTypeface(font);
        email.setText("\ue035");
        email.setTextColor(Color.BLACK);
        String html = "<iframe  width=\"100%\" height=\"300\" style=\"border: 1px solid #cccccc;\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d241197.78692788864!2d72.81099994921877!3d19.163930307959962!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xacafb7b209ebc48a!2sSTREET+TREAT+TECHNOWORKS+PVT+LTD!5e0!3m2!1sen!2sin!4v1479374442422\" ></iframe>";
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadData(html, "text/html", null);
        contactUs();


        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }
        return  rootView;
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            prDialog = new ProgressDialog(getActivity());
            prDialog.setMessage("Please wait ...");
            prDialog.show();
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(prDialog!=null){
                prDialog.dismiss();
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                return false;
            }
        });
        menu.findItem(R.id.Notificationbell).setIcon(null);
  //      menu.findItem(R.id.menu_title).setTitle("                       ");
        menu.findItem(R.id.menu_title).setTitle("                                               ");
        super.onPrepareOptionsMenu(menu);
    }

    private void contactUs() {

        StringRequest str = new StringRequest(Request.Method.POST, Urls.Contact_Us, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {

                        // Toast.makeText(ContactUs.this, jsonObject.getString("Message"), Toast.LENGTH_LONG).show();
                        String address = jsonObject.getJSONObject("items").getString("address");
                        String telephone = jsonObject.getJSONObject("items").getString("telephone");
                        String fax = jsonObject .getJSONObject("items").getString("fax");
                        String email = jsonObject .getJSONObject("items").getString("email");
                        String website = jsonObject .getJSONObject("items").getString("website");
                        Text_View_contact.setText(address);
                        Text_phone.setText(telephone);
                        Text_fax.setText(fax);
                        Text_email.setText(email);
                        Text_website.setText(website);
                    } else {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();

                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put("title", "about us");
                return params;
            }
        };
        RequestQueue requestq = Volley.newRequestQueue(getActivity());
        requestq.add(str);
    }
//    @Override
//    public void onResume() {
//
//        super.onResume();
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
//
//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//                    } else {
//                        // error in creating fragment
//
//
//                    }
//
//                    return true;
//                }
//
//                return false;
//
//            }
//        });
//    }
//




}
