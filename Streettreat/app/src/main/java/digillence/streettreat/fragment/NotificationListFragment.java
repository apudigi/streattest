package digillence.streettreat.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.DataAdapter;
import digillence.streettreat.model.NotificationsGet;
import digillence.streettreat.util.Constant;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.OnLoadMoreListener;
import digillence.streettreat.util.Urls;


public class NotificationListFragment extends Fragment {
    View rootview;
  //  ListView notificationList;
    RecyclerView  notificationList;
    private DataAdapter mAdapter;
    TextView noOfCounts;
    JSONArray jsonNoti;
    private LinearLayoutManager mLayoutManager;
    protected Handler handler;

    View view;
   // NotificationListAdapter notificationListAdapter;
    LinearLayout footer;
    int startlimit =0;
    int starttimeconcat=0;
    NotificationListFragment frag;
    String     logId;


    public NotificationListFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        setHasOptionsMenu(true);
       frag = new NotificationListFragment();
        SharedPreferences sharedPreferences= getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
       logId = sharedPreferences.getString("log_id", "NA");
        Constant.saveLogInId=logId;



        SharedPreferences sharedPrefStoreName = getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
        String storeName = sharedPrefStoreName.getString("store_name", "");
        Constant.storeName=storeName;


        SharedPreferences sharedPrefUserName = getActivity().getSharedPreferences("user_name", Context.MODE_PRIVATE);
        String userName = sharedPrefUserName.getString("user_name", "");
        Constant.userName=userName;
        noOfCounts= (TextView) getActivity().findViewById(R.id.noOfCounts);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //rootview= inflater.inflate(R.layout.activity_notification_list, container, false);

        rootview= inflater.inflate(R.layout.activity_notification_listtest, container, false);

        //notificationList= (ListView)rootview.findViewById(R.id.notificationList);

        notificationList= (RecyclerView)rootview.findViewById(R.id.my_recycler_view);
        noOfCounts= (TextView)rootview.findViewById(R.id.noOfCounts);


        notificationList.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        // use a linear layout manager
        notificationList.setLayoutManager(mLayoutManager);

        // create an Object for Adapter

        view   = getActivity().findViewById(R.id.footer);
        view.setVisibility(View.INVISIBLE);
        new getNotificationsPreviuously().execute();
        //webserviceCall();
        return rootview;
    }

    class getNotificationsPreviuously extends AsyncTask<String, String, JSONArray>

    {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait ...");

        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

        @Override
        protected JSONArray doInBackground(String... args) {
        JSONParser jParser = new JSONParser();


        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

        params.add(new BasicNameValuePair("log_id", logId));



//            params.add(new BasicNameValuePair("limitstart", String.valueOf(startlimit)));
//
//            params.add(new BasicNameValuePair("limit", "10"));

        jsonNoti = jParser.getJSONFromUrl(Urls.getNotifications, params);

        return jsonNoti;
    }
        String description, id,sendDate;
        ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();

    protected void onPostExecute(JSONArray jsonNoti) {
        try {

            pDialog.dismiss();

            for (int i = 0; i < jsonNoti.length(); i++) {
                JSONObject obj = jsonNoti.getJSONObject(i);
                String status = obj.getString("status");
                if (status.equals("1")) {
                    SharedPreferences sharedPrefStoreName = getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
                    String storeName = sharedPrefStoreName.getString("store_name", "");

                    SharedPreferences sharedPrefUserName = getActivity().getSharedPreferences("user_name", Context.MODE_PRIVATE);
                    String userName = sharedPrefUserName.getString("user_name", "");

                    JSONArray js = obj.getJSONArray("items");
                    for (int j = 0; j < js.length(); j++) {

                        JSONObject objj = js.getJSONObject(j);
                        NotificationsGet noti = new NotificationsGet();
                        description = objj.getString("description");

                        sendDate=objj.getString("send_date");
                        id = objj.getString("id");


                        noti.setCode(id);
                        noti.setsendDate(sendDate);
                        String replaced;
                        if (description.contains("{user}") || description.contains("{store}")) {
                            replaced = description.replace("{user}", userName);
                            replaced = replaced.replace("{store}", storeName);
                            noti.setName(replaced);
                        } else if (description.contains("{store}")) {
                            replaced = description.replace("{store}", storeName);

                            noti.setName(replaced);
                        } else {
                            description = description;
                            noti.setName(description);
                        }
                        //noti.setName(description);
                        sendPrevious.add(noti);

                    }

                } else {
                    Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();
                }

            }

            int noOfCounts_=   sendPrevious.size();
            String noOfCountsstr= String.valueOf(noOfCounts_+" "+"Notifications");


            noOfCounts.setText(noOfCountsstr);
//            notificationListAdapter = new NotificationListAdapter(getActivity(), sendPrevious);
//            notificationList.setAdapter(notificationListAdapter);

            mAdapter = new DataAdapter(sendPrevious, notificationList,frag);

            // set the adapter object to the Recyclerview
            notificationList.setAdapter(mAdapter);

            mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add null , so the adapter will check view_type and show progress bar at bottom
                    sendPrevious.add(null);
                    mAdapter.notifyItemInserted(sendPrevious.size() - 1);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //   remove progress item
                            sendPrevious.remove(sendPrevious.size() - 1);
                            mAdapter.notifyItemRemoved(sendPrevious.size());
                            //add items one by one
                            int start = sendPrevious.size();
                            int end = start +5;

                            for (int i = start + 1; i <= end; i++) {
                               // studentList.add(new Student("Student " + i, "AndroidStudent" + i + "@gmail.com"));
                                mAdapter.notifyItemInserted(sendPrevious.size());
                            }
                            mAdapter.setLoaded();
                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 500);

                }
            })
            ;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        catch(IllegalArgumentException ds)
        {}

    }
}





    class getNotificationsPreviuously_ extends AsyncTask<String, String, JSONArray>

    {

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();


            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();



            params.add(new BasicNameValuePair("log_id", Constant.saveLogInId));

     int startlimit_=     startlimit+11;

            params.add(new BasicNameValuePair("limitstart", String.valueOf(startlimit_)));

            params.add(new BasicNameValuePair("limit", "10"));

            jsonNoti = jParser.getJSONFromUrl(Urls.getNotifications, params);

            return jsonNoti;
        }
        String description, id,sendDate;
       // ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();


        protected void onPostExecute(JSONArray jsonNoti) {
            try {

               // pDialog.dismiss();
                final ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();
              TextView  noOfCounts= (TextView)getActivity().findViewById(R.id.noOfCounts);


                for (int i = 0; i < jsonNoti.length(); i++) {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if (status.equals("1")) {

                        String storeName=  Constant.storeName;
                        String userName = Constant.userName;

                        JSONArray js = obj.getJSONArray("items");
                        for (int j = 0; j < js.length(); j++) {

                            JSONObject objj = js.getJSONObject(j);
                            NotificationsGet noti = new NotificationsGet();
                            description = objj.getString("description");

                            sendDate=objj.getString("send_date");
                            id = objj.getString("id");


                            noti.setCode(id);
                            noti.setsendDate(sendDate);
                            String replaced;
                            if (description.contains("{user}") || description.contains("{store}")) {
                                replaced = description.replace("{user}", userName);
                                replaced = replaced.replace("{store}", storeName);
                                noti.setName(replaced);
                            } else if (description.contains("{store}")) {
                                replaced = description.replace("{store}", storeName);

                                noti.setName(replaced);
                            } else {
                                description = description;
                                noti.setName(description);
                            }
                            //noti.setName(description);
                            sendPrevious.add(noti);

                        }

                    } else {
                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                }

                int noOfCounts_=   sendPrevious.size();
                String noOfCountsstr= String.valueOf(noOfCounts_+" "+"Notifications");


                noOfCounts.setText(noOfCountsstr);

                mAdapter = new DataAdapter(sendPrevious, notificationList,frag);

                // set the adapter object to the Recyclerview
                mAdapter.notifyDataSetChanged();
              //  notificationList.setAdapter(mAdapter);

                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        //add null , so the adapter will check view_type and show progress bar at bottom
                        sendPrevious.add(null);
                        mAdapter.notifyItemInserted(sendPrevious.size() - 1);

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //   remove progress item
                                sendPrevious.remove(sendPrevious.size() - 1);
                                mAdapter.notifyItemRemoved(sendPrevious.size());
                                //add items one by one
                                int start = sendPrevious.size();
                                int end = start + 5;

                                for (int i = start + 1; i <= end; i++) {
                                    // studentList.add(new Student("Student " + i, "AndroidStudent" + i + "@gmail.com"));
                                    mAdapter.notifyItemInserted(sendPrevious.size());
                                }
                                mAdapter.setLoaded();
                                //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                            }
                        }, 2000);

                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();

            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch(IllegalArgumentException ds)
            {}

        }
    }


    public void webserviceCall()
    {
        new	getNotificationsPreviuously_().execute();
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                view.setVisibility(View.VISIBLE);
                return false;
            }
        });
        menu.findItem(R.id.Notificationbell).setIcon(null);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {

        super.onResume();

        SharedPreferences sharedPreferences= getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId = sharedPreferences.getString("log_id", "NA");


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fragmentManager = getFragmentManager();
                    getFragmentManager().popBackStack();
                    view.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });
    }

}
