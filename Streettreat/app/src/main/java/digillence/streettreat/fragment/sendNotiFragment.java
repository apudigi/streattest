package digillence.streettreat.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.SendPrevNotiAdapter;
import digillence.streettreat.adapter.SingleListAdapter;
import digillence.streettreat.model.NotificationsGet;
import digillence.streettreat.model.Notificattion;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class sendNotiFragment extends Fragment {


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    ListView lv ,lvprevNoti;
    Button sendNotiBtn;
    TextView sgstNwTmplt;
    Dialog suggestdialog ;
    SingleListAdapter _SendNotiAdapter;
    SendPrevNotiAdapter _SendPrevNotiAdapter;
    protected Handler handler;

    String strDate,logId;
    private static Toast toast;


    public sendNotiFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        handler=new Handler();
        toast = new Toast(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.activity_notification, container, false);
        lv= (ListView)rootView.findViewById(R.id.lv);
        lvprevNoti= (ListView)rootView.findViewById(R.id.lvprevNoti);
        sendNotiBtn=(Button)rootView.findViewById(R.id.sendNotiBtn);
        sgstNwTmplt= (TextView)rootView.findViewById(R.id.sgstNwTmplt);

        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }
        sgstNwTmplt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (toast != null)
                    toast.cancel();


                customDialogue();

            }
        });
        SharedPreferences sharedPreferences= getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId= sharedPreferences.getString("log_id", "NA");


        new NotificationTemplatesWithPrrogress().execute();

        sendNotiBtn.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                SharedPreferences templateIdPrefAl=getActivity().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                String templateId=templateIdPrefAl.getString("TemplateId","");

                if(templateId.equals(""))
                {
                   // Toast.makeText(getActivity(), "PLease select any template", Toast.LENGTH_LONG).show();


                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getActivity(), "PLease select any template", Toast.LENGTH_LONG);
                    toast.show();


                }
                else {
                    sendNotiBtn.setClickable(false);
                    new sendNotification().execute();

                }
            }

        });
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        strDate = sdfDate.format(now);
        return  rootView;
    }


    String enterTemp;
    JSONArray jsonNoti;
    JSONArray jsonSendNoti;
    ArrayList<Notificattion> al= new ArrayList<Notificattion>();


    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
          //  pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            jsonNoti = jParser.getJSONFromUrl(Urls.notificationTemplates, params);

            return jsonNoti;
        }
        String  description,id;
        protected void onPostExecute(JSONArray jsonNoti)
        {
            try {

              //  pDialog.dismiss();

                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                      new getNotificationsPreviuously().execute();

                        SharedPreferences sharedPrefStoreName=getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName= getActivity().getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName=sharedPrefUserName.getString("user_name","");

                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Notificattion noti= new Notificattion();
                            description =  objj.getString("description");
                            id= objj.getString("id");
                            noti.setCode(id);


                            String replaced;
                            if(description.contains("{user}") || description.contains("{store}"))
                            {
                                replaced   =  description.replace("{user}",userName);
                                replaced=  replaced.replace("{store}",storeName);
                                noti.setName(replaced);
                            }
                            else if(description.contains("{store}"))
                            {
                                replaced=  description.replace("{store}",storeName);
                                noti.setName(replaced);
                            }

                            else
                            {
                                description=description;
                                noti.setName(description);
                            }
//                           noti.setName(description);
                            al.add(noti);
                        }

                    }

                    else
                    {
                      //  Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();


                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();




                    }
                }

//                _SendNotiAdapter = new SendNotiAdapter(getActivity(), al);
//                lv.setAdapter(_SendNotiAdapter);

//                _SendNotiAdapter= new  SendNotificationRecyAdapter(getActivity(),al);
//                lv.setAdapter(_SendNotiAdapter);

                _SendNotiAdapter= new SingleListAdapter(getActivity(),al);
                lv.setAdapter(_SendNotiAdapter);


                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        _SendNotiAdapter.setSelectedIndex(position);
                        _SendNotiAdapter.notifyDataSetChanged();

                        String id_ = al.get(position).getCode().toString();

                        SharedPreferences userCountrNameSharedPref=getActivity().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
                        editorUserCountryName.putString("TemplateId", id_);
                        editorUserCountryName.commit();

                    }
                });


//                _SendNotiAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//                    @Override
//                    public void onLoadMore() {
//                        //add null , so the adapter will check view_type and show progress bar at bottom
//                        al.add(null);
//                        _SendNotiAdapter.notifyItemInserted(al.size() - 1);
//
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                //   remove progress item
//                                al.remove(al.size() - 1);
//                                _SendNotiAdapter.notifyItemRemoved(al.size());
//                                //add items one by one
//                                int start = al.size();
//                                int end = start +5;
//
//                                for (int i = start + 1; i <= end; i++) {
//                                    // studentList.add(new Student("Student " + i, "AndroidStudent" + i + "@gmail.com"));
//                                    _SendNotiAdapter.notifyItemInserted(al.size());
//                                }
//                                _SendNotiAdapter.setLoaded();
//                                //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
//                            }
//                        }, 500);
//
//                    }
//                })
//                ;

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException iij)
            {}

        }
    }

    class NotificationTemplatesWithPrrogress extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            jsonNoti = jParser.getJSONFromUrl(Urls.notificationTemplates, params);

            return jsonNoti;
        }

        String  description,id;
        protected void onPostExecute(JSONArray jsonNoti)
        {
            try {

                  pDialog.dismiss();

                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        new getNotificationsPreviuously().execute();

                        SharedPreferences sharedPrefStoreName=getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName= getActivity().getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName=sharedPrefUserName.getString("user_name","");

                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Notificattion noti= new Notificattion();
                            description =  objj.getString("description");
                            id= objj.getString("id");
                            noti.setCode(id);


                            String replaced;
                            if(description.contains("{user}") || description.contains("{store}"))
                            {
                                replaced   =  description.replace("{user}",userName);
                                replaced=  replaced.replace("{store}",storeName);
                                noti.setName(replaced);
                            }
                            else if(description.contains("{store}"))
                            {
                                replaced=  description.replace("{store}",storeName);
                                noti.setName(replaced);
                            }

                            else
                            {
                                description=description;
                                noti.setName(description);
                            }
//                           noti.setName(description);
                            al.add(noti);
                        }

                    }

                    else
                    {
                       // Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();
                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();



                    }
                }

                _SendNotiAdapter= new SingleListAdapter(getActivity(),al);
                lv.setAdapter(_SendNotiAdapter);


                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        _SendNotiAdapter.setSelectedIndex(position);
                        _SendNotiAdapter.notifyDataSetChanged();

                        String id_ = al.get(position).getCode().toString();

                        SharedPreferences userCountrNameSharedPref=getActivity().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
                        editorUserCountryName.putString("TemplateId", id_);
                        editorUserCountryName.commit();

                    }
                });


//                _SendNotiAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//                    @Override
//                    public void onLoadMore() {
//                        //add null , so the adapter will check view_type and show progress bar at bottom
//                        al.add(null);
//                        _SendNotiAdapter.notifyItemInserted(al.size() - 1);
//
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                //   remove progress item
//                                al.remove(al.size() - 1);
//                                _SendNotiAdapter.notifyItemRemoved(al.size());
//                                //add items one by one
//                                int start = al.size();
//                                int end = start +5;
//
//                                for (int i = start + 1; i <= end; i++) {
//                                    // studentList.add(new Student("Student " + i, "AndroidStudent" + i + "@gmail.com"));
//                                    _SendNotiAdapter.notifyItemInserted(al.size());
//                                }
//                                _SendNotiAdapter.setLoaded();
//                                //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
//                            }
//                        }, 500);
//
//                    }
//                })
//                ;


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException iij)
            {}

        }
    }

    class getNotificationsPreviuously extends AsyncTask<String, String, JSONArray> {


        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            // pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("orderdir", "DESC"));
            params.add(new BasicNameValuePair("orderby", "a.created_on"));
            params.add(new BasicNameValuePair("limit", "5"));

            jsonNoti = jParser.getJSONFromUrl(Urls.getNotifications, params);

            return jsonNoti;
        }
        String description, id;
        ArrayList<NotificationsGet> sendPrevious = new ArrayList<>();

        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                 // pDialog.dismiss();

                for (int i = 0; i < jsonNoti.length(); i++) {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if (status.equals("1")) {
                        SharedPreferences sharedPrefStoreName = getActivity().getSharedPreferences("store_name", Context.MODE_PRIVATE);
                        String storeName = sharedPrefStoreName.getString("store_name", "");

                        SharedPreferences sharedPrefUserName =getActivity(). getSharedPreferences("user_name", Context.MODE_PRIVATE);
                        String userName = sharedPrefUserName.getString("user_name", "");

                        JSONArray js = obj.getJSONArray("items");
                        for (int j = 0; j < js.length(); j++) {

                            JSONObject objj = js.getJSONObject(j);
                            NotificationsGet noti = new NotificationsGet();
                            description = objj.getString("description");
                            id = objj.getString("id");
                            noti.setCode(id);
                            String replaced;
                            if (description.contains("{user}") || description.contains("{store}")) {
                                replaced = description.replace("{user}", userName);
                                replaced = replaced.replace("{store}", storeName);
                                noti.setName(replaced);
                            } else if (description.contains("{store}")) {
                                replaced = description.replace("{store}", storeName);

                                noti.setName(replaced);
                            } else {
                                description = description;
                                noti.setName(description);
                            }
                            //noti.setName(description);
                            sendPrevious.add(noti);

                        }

                    } else {
                       // Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();




                    }
                }

                _SendPrevNotiAdapter = new SendPrevNotiAdapter(getActivity(), sendPrevious);
                lvprevNoti.setAdapter(_SendPrevNotiAdapter);

                lvprevNoti.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String id_ = sendPrevious.get(position).getCode().toString();
                        //Toast.makeText(Notificationo.this,id_,Toast.LENGTH_LONG).show();

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            catch (IllegalArgumentException sd)
            {

            }

        }
    }
    class suggestNotificationTemplate extends AsyncTask<String, String, JSONArray> {



        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("template", enterTemp));

            jsonNoti = jParser.getJSONFromUrl(Urls.suggestNotificationTemplate, params);

            return jsonNoti;
        }

        protected void onPostExecute(JSONArray jsonNoti) {
            try {

                 pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {
                        suggestdialog.dismiss();
                        new NotificationTemplates().execute();


                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                toast.cancel();
                            }
                        }, 10000);// for half second


//                        if (toast != null)
//                            toast.cancel();
//                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
//                        toast.show();


                    }

                    else
                    {

                   //     Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();


                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    class sendNotification extends AsyncTask<String, String, JSONArray> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {

            SharedPreferences templateIdPrefAl=getActivity().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
            String templateId=templateIdPrefAl.getString("TemplateId","");


            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

            params.add(new BasicNameValuePair("log_id", logId));
            params.add(new BasicNameValuePair("date_time", strDate));
            params.add(new BasicNameValuePair("templateid", templateId));
            jsonSendNoti = jParser.getJSONFromUrl(Urls.sendNotifi, params);
            return jsonSendNoti;
        }

        protected void onPostExecute(JSONArray jsonSendNoti) {
            try {

                pDialog.dismiss();
                for(int i=0; i<jsonSendNoti.length(); i++)
                {
                    JSONObject obj = jsonSendNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if(status.equals("1"))
                    {

                      new  NotificationTemplates().execute();
                        sendNotiBtn.setClickable(true);

                      //  Toast.makeText(getActivity(),"Notification has been sent to favourite customers",Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(),"Notification has been sent to favourite customers", Toast.LENGTH_LONG);
                        toast.show();



                    }

                    else
                    {
                        //Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();

                        if (toast != null)
                            toast.cancel();
                        toast = Toast.makeText(getActivity(),obj.getString("message"), Toast.LENGTH_LONG);
                        toast.show();




                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }

        }
    }

    public void customDialogue()
    {
        final EditText enterTemplt;
        suggestdialog = new Dialog(getActivity());
        suggestdialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
       getActivity(). getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        suggestdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        suggestdialog.setContentView(R.layout.suggest_noti_dialogue);
        suggestdialog.setCanceledOnTouchOutside(false);

        final ImageView cancel = (ImageView) suggestdialog.findViewById(R.id.cancel);
        enterTemplt= (EditText)suggestdialog.findViewById(R.id.enterTemplt);

        Button submit = (Button) suggestdialog.findViewById(R.id.verify);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggestdialog.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterTemp=   enterTemplt.getText().toString();
                if (enterTemplt.getText().toString().length() < 1) {
                   // Toast.makeText(getActivity(), "PLease Enter", Toast.LENGTH_LONG).show();


                    if (toast != null)
                        toast.cancel();
                    toast = Toast.makeText(getActivity(),"PLease Enter", Toast.LENGTH_LONG);
                    toast.show();




                }
                else{
                    new suggestNotificationTemplate().execute();
                }

            }
        });

        suggestdialog.show();

    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                return false;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }

//
//    @Override
//    public void onResume() {
//
//        super.onResume();
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//
//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//                    } else {
//                        // error in creating fragment
//
//
//                    }
//
//                    return true;
//                }
//
//                return false;
//
//            }
//        });
//    }

}
