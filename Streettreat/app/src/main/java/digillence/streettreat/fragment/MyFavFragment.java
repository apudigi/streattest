package digillence.streettreat.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.FavAdapter;
import digillence.streettreat.model.FavModel;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;

public class MyFavFragment extends Fragment {

    ListView lv;
    ArrayList<FavModel> al = new ArrayList<FavModel>();



    View rootView;
    ImageView imageView;
    String strDate, logId, templateId, storeName;
    JSONArray jsonNoti;
    FavAdapter Fav_Adapter;
    String description, image;


    public MyFavFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_fav, container, false);
        lv = (ListView) rootView.findViewById(R.id.lv);
        imageView = (ImageView) rootView.findViewById(R.id.imageView);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId = sharedPreferences.getString("log_id", "NA");
        new NotificationTemplates().execute();

        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }


        return rootView;

    }

    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("log_id", logId));

            jsonNoti = jParser.getJSONFromUrl(Urls.ViewFav, params);

            return jsonNoti;
        }
        protected void onPostExecute(JSONArray jsonNoti) {

            try {
                //  pDialog.dismiss();
                for (int i = 0; i < jsonNoti.length(); i++) {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");
                    if (status.equals("1")) {
                        JSONArray js = obj.getJSONArray("items");

                        for (int j = 0; j < js.length(); j++) {
                            JSONObject objj = js.getJSONObject(j);
                            FavModel noti = new FavModel();
                            description = objj.getString("user_name");
                            image = objj.getString("profile_pic");
                            noti.setName(description);
                            noti.setIamge(image);
                            al.add(noti);

                        }

                    } else {
                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                Fav_Adapter = new FavAdapter(getActivity(), al);
                lv.setAdapter(Fav_Adapter);


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        }


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                return false;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }


}
