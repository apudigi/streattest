package digillence.streettreat.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import digillence.streettreat.R;


public class PlaceHolderFragment extends Fragment {
    public PlaceHolderFragment() {

    }

    public static final PlaceHolderFragment newInstance(String title)
    {
        PlaceHolderFragment f = new PlaceHolderFragment();

        f.setArguments(getBundle(title));
        return f;
    }

    public static Bundle getBundle(String title){
        Bundle bdl = new Bundle(1);
        bdl.putString("title", title);
        return bdl;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_place_holder, container,false);

        String title = null;

        if(getArguments()!=null){
            title = getArguments().getString("title");
        }

        if(title == null){
            title = "No Title Found!!";
        }

        TextView tv = (TextView)rootView.findViewById(R.id.title);
        tv.setText(title);

        return rootView;
    }

}