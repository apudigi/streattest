package digillence.streettreat.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import digillence.streettreat.R;

public class NewsandEventsFragment extends Fragment {

    View rootView;
    ProgressDialog prDialog;
    private WebView webView;




    public NewsandEventsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.newsandevents, container, false);

        webView = (WebView) rootView.findViewById(R.id.webView1);
        webView.setWebViewClient(new MyWebViewClient());
        String url = "http://www.web.streettreat.in/news-events?tmpl=component";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(url);

        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }
        return rootView;
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            prDialog = new ProgressDialog(getActivity());
            prDialog.setMessage("Please wait ...");
            prDialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(prDialog!=null){
                prDialog.dismiss();
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                return false;
            }
        });
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_title).setTitle("                                               ");
        super.onPrepareOptionsMenu(menu);
    }



//    @Override
//    public void onResume() {
//
//        super.onResume();
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//
//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_frame, fragment).commit();
//                    } else {
//                        // error in creating fragment
//
//                    }
//
//                    return true;
//                }
//
//                return false;
//
//            }
//        });
//    }
















}
