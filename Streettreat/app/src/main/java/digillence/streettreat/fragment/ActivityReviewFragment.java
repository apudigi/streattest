package digillence.streettreat.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import digillence.streettreat.R;


public class ActivityReviewFragment extends android.support.v4.app.Fragment
{
   View rootView;
    private static final String All_SPEC = "All";

    private static final String HAPPY_SPEC = "HAPPY";
    private static final String NOT_SO_HAPPY_SPEC = "NOT SO HAPPY";

    private FragmentTabHost tabHost;
    private TabWidget tabs;

    public ActivityReviewFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        rootView= inflater.inflate(R.layout.activity_review, container, false);
        tabHost = (FragmentTabHost)rootView.findViewById(android.R.id.tabhost);
        tabHost.setup(getActivity(), getChildFragmentManager(), R.id.tabFrameLayout);
        tabs 	= (TabWidget)rootView.findViewById(android.R.id.tabs);

        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.WHITE);
        }

        createTabs();
        return rootView;

    }

    private void createTabs(){

        String title = "All";
        addTab(title, AllFragment.class, PlaceHolderFragment.getBundle(title));


        title = "HAPPY";
        addTab(title, AllFragment.class, PlaceHolderFragment.getBundle(title));

        title="NOT SO HAPPY";
        addTab(title, AllFragment.class, PlaceHolderFragment.getBundle(title));


    }

    private void addTab(String title, Class<?> tabClass, Bundle bundle){
        TabHost.TabSpec spec = tabHost.newTabSpec(title);
        spec.setIndicator(title);
        tabHost.addTab(spec, tabClass, bundle);
    }


    View view;
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                getFragmentManager().popBackStack();
                view.setVisibility(View.VISIBLE);
                return false;
            }
        });
        menu.findItem(R.id.Notificationbell).setIcon(null);

        super.onPrepareOptionsMenu(menu);
    }







}
