package digillence.streettreat.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import digillence.streettreat.R;


public class HomeFragment extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

View rootview;
    TextView myCoupans_icon,Report_icon,Dashboard_icon,Review_icon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }



    private void setFont()
    {   Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "font/Raleway-Bold.ttf");
        Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");

        myCoupans_icon.setTypeface(font3);
        myCoupans_icon.setText("\ue028");
        myCoupans_icon.setTextColor(Color.GRAY);

        Report_icon.setTypeface(font3);
        Report_icon.setText("\ue070");
        Report_icon.setTextColor(Color.GRAY);

        Dashboard_icon.setTypeface(font3);
        Dashboard_icon.setText("\ue030");
        Dashboard_icon.setTextColor(Color.GRAY);

        Review_icon.setTypeface(font3);
        Review_icon.setText("\ue071");
        Review_icon.setTextColor(Color.GRAY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootview= inflater.inflate(R.layout.fragment_home, container, false);

      View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);

        if( myCoupans_icon_ instanceof TextView ) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.WHITE);
        }


        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);


        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }


        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.GRAY);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;

            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }

        return rootview;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setVisible(true);
        menu.findItem(R.id.Notificationbell).setVisible(true);
        menu.findItem(R.id.menu_item_back).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }



    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getActivity().finish();

                    return true;
                }

                return false;

            }
        });
    }

    }
