package digillence.streettreat.fragment;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import digillence.streettreat.R;


public class DashBoardFragment extends Fragment

{

    View rootView;
    public DashBoardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_dash_board, container, false);

        View myCoupans_icon_ = getActivity().findViewById(R.id.myCoupans_icon);
        View Report_icon_ = getActivity().findViewById(R.id.Report_icon);
        View  Dashboard_icon_= getActivity().findViewById(R.id.Dashboard_icon);
        View Review_icon_= getActivity().findViewById(R.id.Review_icon);

        if( myCoupans_icon_ instanceof TextView) {
            TextView myCoupans_icon = (TextView) myCoupans_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            myCoupans_icon.setTypeface(font3);
            myCoupans_icon.setText("\ue028");
            myCoupans_icon.setTextColor(Color.GRAY);
        }
        if( Report_icon_ instanceof TextView) {
            TextView Report_icon = (TextView) Report_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Report_icon.setTypeface(font3);
            Report_icon.setText("\ue070");
            Report_icon.setTextColor(Color.GRAY);
        }
        if(Dashboard_icon_ instanceof TextView )
        {
            TextView Dashboard_icon = (TextView) Dashboard_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Dashboard_icon.setTypeface(font3);
            Dashboard_icon.setText("\ue030");
            Dashboard_icon.setTextColor(Color.WHITE);
        }
        if(Review_icon_ instanceof TextView)
        {
            TextView Review_icon = (TextView) Review_icon_;
            Typeface font3 = Typeface.createFromAsset(getActivity().getAssets(), "font/fontello.ttf");
            Review_icon.setTypeface(font3);
            Review_icon.setText("\ue071");
            Review_icon.setTextColor(Color.GRAY);
        }
        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setVisible(true);
        menu.findItem(R.id.Notificationbell).setVisible(true);
        menu.findItem(R.id.menu_item_back).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }


}