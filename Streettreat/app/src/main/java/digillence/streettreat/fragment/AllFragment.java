package digillence.streettreat.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.adapter.ListRecyclerAdapter;
import digillence.streettreat.model.Reviews;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;



public class AllFragment extends Fragment
{
    JSONArray jsonNoti;
    ListRecyclerAdapter _SendNotiAdapter;
    RecyclerView rvDeals;
    View rootView;
    String strDate,logId,templateId,storeName,replyId;

    View view;

    public AllFragment() {

    }



//    @Override
//    public void onResume() {
//
//        super.onResume();
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//
//                    Fragment fragment = new HomeFragment();
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.tabFrameLayout, fragment).commit();
//                    } else {
//                        // error in creating fragment
//
//
//                    }
//
//                    return true;
//                }
//
//                return false;
//
//            }
//        });
//    }

    public static AllFragment newInstance(String param1, String param2) {
        AllFragment fragment = new AllFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        rootView= inflater.inflate(R.layout.fragment_all, container, false);

        rvDeals = (RecyclerView) rootView.findViewById(R.id.rvDeals_);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvDeals.setLayoutManager(mLayoutManager);
        rvDeals.setItemAnimator(new DefaultItemAnimator());

        SharedPreferences sharedPreferences= getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
        logId= sharedPreferences.getString("log_id", "NA");
        SharedPreferences sharePreferences= getActivity().getSharedPreferences("id", Context.MODE_PRIVATE);
        replyId= sharePreferences.getString("id", "NA");
        new NotificationTemplates().execute();
        return rootView;
    }
    private FragmentActivity myContext;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item).setIcon(null);
        menu.findItem(R.id.menu_itedfm_baiogfdp).setVisible(true);
        menu.findItem(R.id.Notificationbell).setIcon(null);
        menu.findItem(R.id.menu_item_back).setVisible(true);
        menu.findItem(R.id.menu_item_back).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {


                android.support.v4.app.FragmentManager fragmentManager = myContext.getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                HomeFragment fragment = new HomeFragment();
                fragmentTransaction.replace(R.id.content_frame, fragment).addToBackStack("");
                fragmentTransaction.commit();

                return false;
            }

        });
        menu.findItem(R.id.Notificationbell).setIcon(null);

        super.onPrepareOptionsMenu(menu);
    }




    class NotificationTemplates extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            try {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("log_id", Context.MODE_PRIVATE);
                String logId = sharedPreferences.getString("log_id", "NA");


                JSONParser jParser = new JSONParser();
                List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                params.add(new BasicNameValuePair("log_id", logId));
                jsonNoti = jParser.getJSONFromUrl(Urls.ViewReviews, params);


            }catch (NullPointerException fd)
            {

            }
            return jsonNoti;

        }
        ArrayList<Reviews> al= new ArrayList<Reviews>();
        String  description,id,reply,pic,reviewId,reply_text;
        Integer rate;


        protected void onPostExecute(JSONArray jsonNoti) {

            try {
                 pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");

                    if(status.equals("1") )
                    {
                        JSONArray js= obj.getJSONArray("items");
                        for(int j=0; j<js.length();j++)
                        {

                            JSONObject objj=js.getJSONObject(j);
                            Reviews noti= new Reviews();
                            rate = Integer.parseInt(objj.getString("rating"));

                            description =  objj.getString("comments");
                            reply =  objj.getString("reply_text");
                            pic= objj.getString("profile_pic");
                            reviewId=objj.getString("id");
                            reply_text= objj.getString("reply_text");

                            noti.setReply_text(reply_text);
                            noti.setReviewId(reviewId);
                            noti.setIamge(pic);
                            noti.setName(description);
                            noti.setComment(reply);
                            al.add(noti);
                        }
                    }

                    else
                    {
                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }

                _SendNotiAdapter = new ListRecyclerAdapter(getActivity(),al);
                rvDeals.setAdapter(_SendNotiAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getFragmentManager().popBackStack();
    }
}
