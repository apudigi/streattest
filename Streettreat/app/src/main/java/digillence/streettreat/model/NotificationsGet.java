package digillence.streettreat.model;

/**
 * Created by nayan on 29/12/16.
 */
public class NotificationsGet {

    String id = null;
    String Desc = null;
    boolean selected = false;
    String sendDate = null;


    public NotificationsGet() {
    }

    public NotificationsGet(String code, String name, boolean selected) {
        super();
        this.id = code;
        this.Desc = name;
        this.selected = selected;
    }

    public String getCode() {
        return id;
    }

    public void setCode(String code) {
        this.id = code;
    }

    public String getsendDate()

    {
        return sendDate;
    }

    public void setsendDate(String sendDate)
    {
        this.sendDate=sendDate;
    }



    public String getName() {
        return Desc;
    }

    public void setName(String name) {
        this.Desc = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}

