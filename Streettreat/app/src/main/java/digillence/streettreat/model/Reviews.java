package digillence.streettreat.model;

/**
 * Created by devashree on 30/12/16.
 */
public class Reviews {

    String code = null;
    String name = null;
    String iamge = null;
    boolean selected = false;
    String comment=null;
    String reviewId=null;
    String replyText;

    public Reviews()
    {}

    public Reviews(String code, String name, String iamge, boolean selected,String reviewId) {
        this.code = code;
        this.name = name;
        this.iamge = iamge;
        this.selected = selected;
        this.reviewId=reviewId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReply_text()
    {
        return  replyText;
    }

    public void setReply_text(String replyText)
    {
        this.replyText=replyText;
    }



    public String getReviewId()
    {
        return reviewId;
    }

    public void setReviewId(String reviewId)
    {
        this.reviewId=reviewId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }




    public String getIamge() {
        return iamge;
    }

    public void setIamge(String iamge) {
        this.iamge = iamge;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
