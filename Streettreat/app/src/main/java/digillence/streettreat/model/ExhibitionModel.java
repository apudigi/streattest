package digillence.streettreat.model;

/**
 * Created by nayan on 16/6/16.
 */
public class ExhibitionModel {
    private int img;
    private String name;
    private String time;


    public ExhibitionModel(int img, String name, String time) {
        this.img = img;
        this.name = name;
        this.time = time;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
