package digillence.streettreat.model;

/**
 * Created by nayan on 28/12/16.
 */
public class Notificattion {
    String code = null;
    String name = null;
    boolean selected = false;

    public Notificattion()
    {}

    public Notificattion(String code, String name, boolean selected) {
        super();
        this.code = code;
        this.name = name;
        this.selected = selected;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}