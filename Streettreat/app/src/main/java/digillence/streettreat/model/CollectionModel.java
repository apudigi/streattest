package digillence.streettreat.model;

/**
 * Created by nayan on 16/6/16.
 */
public class CollectionModel {
    private String collectionName;

    public CollectionModel(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
