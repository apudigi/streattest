package digillence.streettreat.model;

/**
 * Created by devashree on 5/1/17.
 */
public class FavModel {

    String code = null;
    String name = null;
    String iamge = null;
    boolean selected = false;

    public FavModel()
    {}

    public FavModel(String code, String name, String iamge, boolean selected) {
        this.code = code;
        this.name = name;
        this.iamge = iamge;
        this.selected = selected;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIamge() {
        return iamge;
    }

    public void setIamge(String iamge) {
        this.iamge = iamge;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
