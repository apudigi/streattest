package digillence.streettreat.model;

/**
 * Created by nayan on 16/6/16.
 */
public class DealsModel {
    public String getDealName() {
        return dealName;
    }

    public DealsModel(String dealName, String dealOff) {
        this.dealName = dealName;
        this.dealOff = dealOff;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDealOff() {
        return dealOff;
    }

    public void setDealOff(String dealOff) {
        this.dealOff = dealOff;
    }

    private String dealName;
    private String dealOff;
}
