package digillence.streettreat.util;

/**
 * Created by nayan on 22/6/16.
 */
public class Urls {
    public static String baseUrlCom="http://www.streettreat.digiroltest.com/";
    //public static String checkUniqueness="http://www.streettreat.digiroltest.com/index.php?option=com_konnect&task=checkUniqueness";
    public static String checkUniqueness=baseUrlCom+"index.php?option=com_konnect&task=checkUniqueness";

    public static String registration="http://www.web.streettreat.in/index.php?option=com_konnect&task=createPartner";
    public static String forgotPassword= "http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.forgotPasswordByMobile";
    public static String verifyOTP="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.verifyForgotPasswordOTP";
    public static String 	loginuser="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.userLogin";
    public static String 	profile="http://www.streettreat.digiroltest.com/index.php?option=com_konnect&task=getProfileInfo";
    public static String 	bucketList="http://www.streettreat.digiroltest.com/index.php?option=com_konnect&task=getBucketList";
    public static String ChangePassword="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.changePassword";
    public static String aboutus="http://www.web.streettreat.in/index.php?option=com_konnect&task=connect.getArticleContent";
    public static String notificationTemplates="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.getNotificationtemplates";
    public static String sendNotifi="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.sendNotifications";
    public static String getNotifications ="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.getNotifications";
    public static String Contact_Us = "http://www.web.streettreat.in/index.php?option=com_konnect&task=connect.getContactInformation";
    public static String ViewReviews ="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.getReviews";
    public static String ViewFav ="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.getStoreFavorites";
    public static String replyToReview ="http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.replyToReview";


    public static String suggestNotificationTemplate = "http://www.web.streettreat.in/index.php?option=com_konnect&task=vendor.suggestNotificationTemplate";
}
