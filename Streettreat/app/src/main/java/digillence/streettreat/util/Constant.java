package digillence.streettreat.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.widget.Toast;

/**
 * Created by nayan on 14/6/16.
 */
public class Constant {


    public static String male="Male";
    public static String female="Female";
    public static String loginid="loginid";
    public static String saveLogInId="";
    public static String storeName="";
    public static String userName="";
    private Context context;



    public Constant(Context context){
        this.context = context;
    }



    public static Typeface imgFont(Context context)
    {

      return Typeface.createFromAsset(context.getAssets(),
              "font/FontAwesome.otf");
    }
    public static Typeface fontello(Context context)
    {

      return Typeface.createFromAsset(context.getAssets(),
              "font/fontello.ttf");
    }

    public Typeface getTypefaceAndroid(){
        Typeface typeFace = Typeface.DEFAULT;
        String strFont = "Assets/fonts/fontello.ttf";
        try {
            if (!strFont.equals("")){
                String strLeft = strFont.substring(0, 13);
                if (strLeft.equals("Assets/fonts/")){
                    typeFace = Typeface.createFromAsset(context.getAssets(), strFont.replace("Assets/", ""));
                } else {
                    typeFace = Typeface.createFromFile(strFont);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeFace;
    }












    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isInternet(Context context)
    {

        ConnectionDetector cd = new ConnectionDetector(context);
        return  cd.isConnectingToInternet();
    }

    public static void DisplayToast(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();;
    }
    public static String SavePrefrence(Context context, String key, String value)
    {
        SharedPreferences preferences = context.getSharedPreferences("USERDETAIL", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
        return key;
    }
    public static String LoadPrefrence(Context context, String key)
    {
        SharedPreferences preferences = context.getSharedPreferences("USERDETAIL", 0);
        String value = preferences.getString(key, "");
        return value;
    }
    public static void saveLoginId(Context context,String id)
    {
        SharedPreferences preferences = context.getSharedPreferences("login", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(loginid,id );
        editor.commit();
    }
    public static String getLoginId(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences("login", 0);
        return      preferences.getString(loginid,"");

    }
    public static ProgressDialog progress(Context context)
    {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Wait");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return progress;
    }
}
