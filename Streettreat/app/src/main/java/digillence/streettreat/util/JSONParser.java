package digillence.streettreat.util;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

@SuppressWarnings("deprecation")
public class JSONParser {
 
	static InputStream is = null;
	static JSONArray jArr = null;
	static String json = "";
	static JSONArray jArr_GET = null;
 
	// constructor
	public JSONParser() {
 
	}
	//public JSONObject getJSONFromUrl(String url, List params) {
	public JSONArray getJSONFromUrl(String url, List<BasicNameValuePair> params) {
		  System.out.println(" --- response ---- " + url);

		Log.e("url  ________", "" + url);

		Log.e("params  ________",""+ params);
		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			 httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();			
 
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line.toString() + "\n");
			}
			is.close();
			json = sb.toString();
			json="["+json+"]";
			
			 System.out.println(" --- json_response ---- " + json);
			Log.e("--------ApJson-------",json);// E/--------ApJson-------: [{"flag":"True","UserID":32}]
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
 
		// try parse the string to a JSON object
		try {

			jArr = new JSONArray(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
 
		 System.out.println(" --- response ---- " + jArr);

		// return JSON String
		return jArr;
 
	}

	public JSONArray getJSONFromUrl(String url)
	{
		System.out.println(" --- response ---- " + url);
		Log.e("AsyncTask", url);
		// Making HTTP request
		try {
			// defaultHttpClient
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpPo = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpPo);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line.toString() + "\n");
			}
			is.close();
			json = sb.toString();

			System.out.println(" --- json_response ---- " + json);
			Log.e("--------ApJson-------",json);// E/--------ApJson-------: [{"flag":"True","UserID":32}]
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());

		}
		// try parse the string to a JSON object
		try {
			jArr_GET = new JSONArray(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		System.out.println(" --- response ---- " + jArr);
		return jArr_GET;

	}



	public String getJSONFromUrl_(String url) {
		System.out.println(" --- response ---- " + url);
		Log.e("AsyncTask", url);
		// Making HTTP request
		try {
			// defaultHttpClient
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpPo = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpPo);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line.toString() + "\n");
			}
			is.close();
			json = sb.toString();

			System.out.println(" --- json_response ---- " + json);
			Log.e("--------ApJson-------",json);// E/--------ApJson-------: [{"flag":"True","UserID":32}]
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		return json;

	}

	public String getJSONFromUrl_WithParams(String url, List<BasicNameValuePair> params)
	{
		System.out.println(" --- response ---- " + url);
		Log.e("AsyncTask", url);
		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}




		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line.toString() + "\n");
			}
			is.close();
			json = sb.toString();

			System.out.println(" --- apppppsheeeetal ---- " + json);
			//Log.e("--------ApJson-------",json);// E/--------ApJson-------: [{"flag":"True","UserID":32}]
		} catch (Exception e) {
			//Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		Log.e("%%%%%%%%%%%%", json);
		return json;

	}





	/*public String GET(String url) {
		InputStream inputStream = null;
		String result = "";
		try {

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// convert inputstream to string
			if (inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

		return result;
	}*/

	// convert inputstream to String
	private static String convertInputStreamToString(InputStream inputStream) throws IOException
	{
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	//
}


