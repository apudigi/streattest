package digillence.streettreat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import digillence.streettreat.R;

/**
 * Created by devashree on 28/12/16.
 */
public class ReportAdapter extends ArrayAdapter<String> {

    private String[] report;
    private Activity context;

    public ReportAdapter(Activity context,String[] report){
        super(context, R.layout.row_list_item, report);
        this.report =report;
        this.context =context;
    }
    public View getView(){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowview = inflater.inflate(R.layout.row_list_item,null,true);
        TextView textView1 = (TextView) rowview.findViewById(R.id.REPORT_TEXT_VIEW);
        return rowview;
    };




}
