package digillence.streettreat.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.model.Reviews;
import digillence.streettreat.util.JSONParser;
import digillence.streettreat.util.Urls;


@SuppressWarnings("static-access")
public class ListRecyclerAdapter extends
        RecyclerView.Adapter<ListRecyclerAdapter.MyViewHolder> {

    private final ArrayList<Reviews> mLists;
    static Context mContext;
    public static final String LOCAL_IMAGES_FOLDER_PATH = Environment.getExternalStorageDirectory() + "/.demo_app";

    public static final String DOCTOR_IMAGES_FOLDER_PATH = LOCAL_IMAGES_FOLDER_PATH + "/images";

    String comment;
    public ListRecyclerAdapter( Context mContext2, ArrayList<Reviews>  lists) {
        // TODO Auto-generated constructor stub
        this.mLists = lists;
        this.mContext = mContext2;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements
            OnClickListener {

        public TextView tv_articleTitle ,post_Reply,postedComment;
        public LinearLayout POST_Layout,display_reply;
        public EditText postComment;
        public Button post_btn;
        public ImageView profilepic;

        @SuppressLint("NewApi")
        public MyViewHolder(View view, int ViewType) {
            super(view);

            try {

                tv_articleTitle = (TextView) view.findViewById(R.id.code);
                post_Reply = (TextView) view.findViewById(R.id.post_Reply);
                POST_Layout = (LinearLayout) view.findViewById(R.id.POST_Layout);
                postComment= (EditText)view.findViewById(R.id.postComment);
                post_btn=(Button)view.findViewById(R.id.post_btn);
                postedComment= (TextView)view.findViewById(R.id.postedComment);
                display_reply = (LinearLayout) view.findViewById(R.id.display_reply);
                profilepic=  (ImageView)view.findViewById(R.id.profilepic);


            } catch (Exception e) {

            }
        }

        @Override
        public void onClick(View v) {

            int position = getLayoutPosition();

        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        MyViewHolder vhUser = null;
        try {
            View view = null;

            view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.notificationrow_, parent, false);

            vhUser = new MyViewHolder(view, viewType);

        } catch (Exception e) {

        }
        return vhUser;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        try {

            holder.tv_articleTitle.setText(mLists.get(position).getName());

            Picasso.with(mContext)
                    .load(mLists.get(position).getIamge())
                    .into(holder.profilepic);


            String postedReply=mLists.get(position).getReply_text();
            if(!postedReply.equals("0"))
            {
                holder.display_reply.setVisibility(View.VISIBLE);
                holder.post_Reply.setVisibility(View.GONE);
                holder.postedComment.setText(postedReply);

            }
else {

                holder.post_Reply.setVisibility(View.VISIBLE);
                holder.postedComment.setVisibility(View.GONE);
                holder.post_Reply.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String reviewId = mLists.get(position).getReviewId();

                        SharedPreferences reviewIdSP = mContext.getSharedPreferences("ReviewId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = reviewIdSP.edit();
                        edit.putString("ReviewId", reviewId);
                        edit.commit();


                        holder.POST_Layout.setVisibility(v.VISIBLE);

                        if (holder.POST_Layout.getVisibility() == v.VISIBLE) {
                            holder.post_Reply.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    holder.POST_Layout.setVisibility(v.GONE);
                                }
                            });

                        } else {
                            holder.post_Reply.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    holder.POST_Layout.setVisibility(v.VISIBLE);
                                }
                            });

                        }

                    }
                });
            }

            holder.post_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                     comment=      holder.postComment.getText().toString();
                    holder.post_Reply.setVisibility(View.GONE);
                    holder.POST_Layout.setVisibility(View.GONE);
                    holder.display_reply.setVisibility(View.VISIBLE);
                    holder.postedComment.setVisibility(View.VISIBLE);

                   if(comment.equals("")||comment.length()<0) {
                     Toast.makeText(mContext,"Please enter",Toast.LENGTH_LONG).show();
                       holder.post_Reply.setVisibility(View.VISIBLE);
                       holder.POST_Layout.setVisibility(View.VISIBLE);
                       holder.display_reply.setVisibility(View.GONE);
                       holder.postedComment.setVisibility(View.GONE);

                   }
                    else{

                       new ReplyToReview().execute();
                       holder.postedComment.setText("REPLIED: " + comment);
                   }

                }
            });

        } catch (Exception e) {

        }
    }


    @Override
    public int getItemCount() {
        // Number of items in the list will be +1 the patient including the
        // user.
        return mLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        // With the following method we check what type of view is being passed

        return position;
    }

    private boolean isPositionUser(int position) {
        return position == 0;
    }

JSONArray jsonNoti;




    class ReplyToReview extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Please Wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONArray doInBackground(String... args)
        {
            try {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("log_id", Context.MODE_PRIVATE);
                String logId = sharedPreferences.getString("log_id", "NA");

                SharedPreferences reviewIdSP= mContext.getSharedPreferences("ReviewId", Context.MODE_PRIVATE);
                String revID=   reviewIdSP.getString("ReviewId", "NA");

                JSONParser jParser = new JSONParser();
                List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                params.add(new BasicNameValuePair("log_id", logId));
                params.add(new BasicNameValuePair("reply_text",comment ));
                params.add(new BasicNameValuePair("review_id",revID ));

                jsonNoti = jParser.getJSONFromUrl(Urls.replyToReview, params);


            }catch (NullPointerException fd)
            {

            }
            return jsonNoti;

        }
        ArrayList<Reviews> al= new ArrayList<Reviews>();
        String  description,id,reply,pic,reviewId;
        Integer rate;


        protected void onPostExecute(JSONArray jsonNoti) {

            try {
                pDialog.dismiss();
                for(int i=0; i<jsonNoti.length(); i++)
                {
                    JSONObject obj = jsonNoti.getJSONObject(i);
                    String status = obj.getString("status");

                    if(status.equals("1") )
                    {
//                        JSONArray js= obj.getJSONArray("items");
//                        for(int j=0; j<js.length();j++)
//                        {
//
//                            JSONObject objj=js.getJSONObject(j);
//
//                        }

                        //Toast.makeText(mContext, obj.getString("message"), Toast.LENGTH_LONG).show();

                    }

                    else
                    {
                        Toast.makeText(mContext, obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                }



            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e)
            {
                e.printStackTrace();
            }
        }
    }



































}
