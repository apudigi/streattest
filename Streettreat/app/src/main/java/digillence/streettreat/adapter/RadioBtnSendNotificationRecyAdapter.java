package digillence.streettreat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import digillence.streettreat.R;
import digillence.streettreat.model.Notificattion;
import digillence.streettreat.util.OnLoadMoreListener;


@SuppressWarnings("static-access")
public class RadioBtnSendNotificationRecyAdapter extends
        RecyclerView.Adapter<RadioBtnSendNotificationRecyAdapter.MyViewHolder> {

    private final ArrayList<Notificattion> names;
    static Context mContext;
    ArrayList<Integer> selectedItems;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;

    ArrayList<String> selectedTemplatedIds;
    private boolean loading;

    private int visibleThreshold = 5;


    public static final String LOCAL_IMAGES_FOLDER_PATH = Environment.getExternalStorageDirectory() + "/.demo_app";

    public static final String DOCTOR_IMAGES_FOLDER_PATH = LOCAL_IMAGES_FOLDER_PATH + "/images";


    public RadioBtnSendNotificationRecyAdapter(Context mContext2, ArrayList<Notificattion> names) {
        // TODO Auto-generated constructor stub
        this.names = names;
        this.mContext = mContext2;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();

//
//        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager)
//        {
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
//                    .getLayoutManager();
//
//
//            recyclerView
//                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(RecyclerView recyclerView,
//                                               int dx, int dy) {
//                            super.onScrolled(recyclerView, dx, dy);
//
//                            totalItemCount = linearLayoutManager.getItemCount();
//                            lastVisibleItem = linearLayoutManager
//                                    .findLastVisibleItemPosition();
//                            if (!loading
//                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                                // End has been reached
//                                // Do something
//                                if (onLoadMoreListener != null) {
//                                    onLoadMoreListener.onLoadMore();
//                                }
//                                loading = true;
//                            }
//                        }
//                    });
        //}


    }
    public void setLoaded() {
        loading = false;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements
            OnClickListener {

        public TextView tv_articleTitle ,post_Reply;
        public LinearLayout POST_Layout;
        public EditText postComment;
        public Button post_btn;
        public Button checkBox1;
        public ProgressViewHolder ProgressViewHolder;

        @SuppressLint("NewApi")
        public MyViewHolder(View view, int ViewType) {
            super(view);

            try {
                tv_articleTitle = (TextView) view.findViewById(R.id.code);
                checkBox1= (Button) view.findViewById(R.id.checkBox1);


            } catch (Exception e) {

            }
        }

        @Override
        public void onClick(View v) {

            int position = getLayoutPosition();

        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        MyViewHolder vhUser = null;
        try {
            View view = null;

            view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.notificationrow, parent, false);

            vhUser = new MyViewHolder(view, viewType);

        } catch (Exception e) {

        }

        return vhUser;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        try {

           // holder.tv_articleTitle.setText(mLists.get(position).getName());
            holder.tv_articleTitle.setText(names.get(position).getName());
            holder.tv_articleTitle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = new Integer(position);
                    if (selectedItems.contains(new Integer(position))) {
                        selectedItems.remove(new Integer(position));
                        notifyDataSetChanged();
                    } else {
                        selectedItems.add(new Integer(position));
                        notifyDataSetChanged();
                    }


                    if (selectedItems.contains(new Integer(position))) {
                        //  holder.checkBox1.setBackgroundColor(Color.BLUE);
                        holder.checkBox1.setBackgroundResource(R.drawable.ldpi);

                        holder.tv_articleTitle.setTextColor(Color.BLACK);
                        String id = names.get(position).getCode().toString();

                        selectedTemplatedIds.add(id);
                        SharedPreferences templateIdPrefAl = mContext.getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

                        Set<String> set = new HashSet<String>();
                        set.addAll(selectedTemplatedIds);
                        editemplateIdPrefAl.putStringSet("TemplateId", set);
                        editemplateIdPrefAl.commit();

                        Set<String> set_ = new HashSet<String>();
                        set_ = templateIdPrefAl.getStringSet("TemplateId", null);

                        Toast.makeText(mContext, "" + set_, Toast.LENGTH_LONG).show();

                    } else {
                        // holder.checkBox1.setBackgroundColor(Color.GRAY);
                        holder.checkBox1.setBackgroundResource(R.drawable.ldpi_);

                        holder.tv_articleTitle.setTextColor(Color.GRAY);
                        String id = names.get(position).getCode().toString();
                        try {

                            selectedTemplatedIds.remove(position);
                            SharedPreferences templateIdPrefAl = mContext.getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

                            Set<String> set = new HashSet<String>();
                            set.addAll(selectedTemplatedIds);
                            editemplateIdPrefAl.putStringSet("TemplateId", set);
                            editemplateIdPrefAl.commit();


                            Set<String> set_ = new HashSet<String>();
                            set_ = templateIdPrefAl.getStringSet("TemplateId", null);
                            Toast.makeText(mContext, "" + set_, Toast.LENGTH_LONG).show();
                        } catch (IndexOutOfBoundsException ie) {
                        }

                    }

                }
            });
            holder.checkBox1.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
//                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/fontello.ttf");
//                holder.checkBox1.setTypeface(typeface);
                    //String id=  names.get(position).getCode().toString();
                    if (selectedItems.contains(new Integer(position)))
                    {
                        selectedItems.remove(new Integer(position));
                        notifyDataSetChanged();
                    } else {
                        selectedItems.add(new Integer(position));
                        notifyDataSetChanged();
                    }


                    if (selectedItems.contains(new Integer(position))) {
                        // holder.checkBox1.setBackgroundColor(Color.BLUE);
                        holder.checkBox1.setBackgroundResource(R.drawable.ldpi);

                        holder.tv_articleTitle.setTextColor(Color.BLACK);
                        String id=  names.get(position).getCode().toString();

                        selectedTemplatedIds.add(id);
                        SharedPreferences templateIdPrefAl=mContext.getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editemplateIdPrefAl= templateIdPrefAl.edit();

                        Set<String> set = new HashSet<String>();
                        set.addAll(selectedTemplatedIds);
                        editemplateIdPrefAl.putStringSet("TemplateId", set);
                        editemplateIdPrefAl.commit();

                        Set<String> set_ = new HashSet<String>();
                        set_ = templateIdPrefAl.getStringSet("TemplateId", null);




                    } else {
                        //holder.checkBox1.setBackgroundColor(Color.GRAY);
                        holder.checkBox1.setBackgroundResource(R.drawable.ldpi_);


                        holder.tv_articleTitle.setTextColor(Color.GRAY);

                        String id=  names.get(position).getCode().toString();
                        try {
                            selectedTemplatedIds.remove(position);

                            SharedPreferences templateIdPrefAl = mContext.getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

                            Set<String> set = new HashSet<String>();
                            set.addAll(selectedTemplatedIds);
                            editemplateIdPrefAl.putStringSet("TemplateId", set);
                            editemplateIdPrefAl.commit();

                            Set<String> set_ = new HashSet<String>();
                            set_ = templateIdPrefAl.getStringSet("TemplateId", null);


                        }
                        catch (IndexOutOfBoundsException r)
                        {}
                    }

                }


            });


            holder.post_Reply.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.POST_Layout.setVisibility(v.VISIBLE);
                }
            });

            holder.post_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String comment=      holder.postComment.getText().toString();
                    Toast.makeText(mContext,comment,Toast.LENGTH_SHORT).show();

                }
            });


            ((holder.ProgressViewHolder)).progressBar.setIndeterminate(true);  
            
            
            
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        // Number of items in the list will be +1 the patient including the
        // user.
        return names.size();
    }

    @Override
    public int getItemViewType(int position) {
        // With the following method we check what type of view is being passed

        return position;
    }

    private boolean isPositionUser(int position) {
        return position == 0;
    }
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {

            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
