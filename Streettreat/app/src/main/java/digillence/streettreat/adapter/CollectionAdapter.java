package digillence.streettreat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import digillence.streettreat.R;
import digillence.streettreat.model.CollectionModel;

/**
 * Created by nayan on 16/6/16.
 */
public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.MyViewHolder> {
    private ArrayList<CollectionModel> collectionList;

    public CollectionAdapter(ArrayList<CollectionModel> collectionList) {
        this.collectionList = collectionList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_collection, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CollectionModel collectionModel = collectionList.get(position);
        holder.txtCollection.setText(collectionModel.getCollectionName());


    }

    @Override
    public int getItemCount() {
        return collectionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtCollection;

        public MyViewHolder(View view) {
            super(view);
            txtCollection = (TextView) view.findViewById(R.id.txtCollection);

        }
    }
}
