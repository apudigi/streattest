package digillence.streettreat.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import digillence.streettreat.R;
import digillence.streettreat.model.DealsModel;
import digillence.streettreat.model.ExhibitionModel;

/**
 * Created by nayan on 16/6/16.
 */
public class ExibhitionAdapter extends PagerAdapter {
    Context mContext;
    private ArrayList<ExhibitionModel> exhibitionList;

    LayoutInflater mLayoutInflater;
    @Override
    public int getCount() {
        return exhibitionList.size();
    }

    public ExibhitionAdapter(Context mContext,ArrayList<ExhibitionModel> exhibitionList) {
        this.mContext = mContext;
        this.exhibitionList=exhibitionList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);

    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.row_exibhition, container, false);

        ImageView imgExibhistion = (ImageView) itemView.findViewById(R.id.imgExibhistion);
        TextView txtExhibhition = (TextView) itemView.findViewById(R.id.txtExhibhition);
        TextView txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        imgExibhistion.setImageResource(exhibitionList.get(position).getImg());
        txtExhibhition.setText(exhibitionList.get(position).getName());
        txtTime.setText(exhibitionList.get(position).getTime());
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

