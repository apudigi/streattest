package digillence.streettreat.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import digillence.streettreat.model.DealsModel;
import digillence.streettreat.R;

/**
 * Created by nayan on 16/6/16.
 */
public class DealsAdapter  extends RecyclerView.Adapter<DealsAdapter.MyViewHolder> {
    private ArrayList<DealsModel> dealList;
    Context context;
    public DealsAdapter(ArrayList<DealsModel> dealList) {
        this.dealList = dealList;
    }

    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_deals_horizontal, parent, false);

        context=parent.getContext();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(android.graphics.Color.TRANSPARENT));

                    dialog.setContentView(R.layout.discount_popup);

                    TextView txtDealOff = (TextView) dialog.findViewById(R.id.txtDealOff);
                    TextView txtDesciption_discount = (TextView) dialog.findViewById(R.id.txtDesciption_discount);
                    Button btnsubmit_register = (Button) dialog.findViewById(R.id.btnsubmit_register);
                    ImageView iv_close= (ImageView)dialog.findViewById(R.id.iv_close);


                    dialog.setCanceledOnTouchOutside(true);
                    dialog.setOnCancelListener(
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    Log.e("Discount Touch outside", "   in Dialog boxxx");


                                    //When you touch outside of dialog bounds,
                                    //the dialog gets canceled and this method executes.
                                }
                            }
                    );


                    iv_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                    Log.d("DealsAdapter", "onClick " + " ");
                }catch (Exception ex)
                {
                    Log.d("Catch DealsAdapter", "onClick " + " ");
                    ex.getMessage();
                }
            }
        });
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DealsModel dealsModel = dealList.get(position);
        holder.txtDealOff.setText(dealsModel.getDealName()+"%");
        holder.txtDealName.setText(dealsModel.getDealOff());


    }

    @Override
    public int getItemCount() {
        return dealList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDealName, txtDealOff;

        public MyViewHolder(View view) {
            super(view);
            txtDealName = (TextView) view.findViewById(R.id.txtDealName);
            txtDealOff = (TextView) view.findViewById(R.id.txtDealOff);




        }


    }


}
