package digillence.streettreat.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import digillence.streettreat.R;
import digillence.streettreat.model.Notificattion;

/**
 * Created by nayan on 28/12/16.
 */
public class SendNotiAdapter extends ArrayAdapter<Notificattion>
{
    private final Activity context;
    private final ArrayList<Notificattion> names;
    Boolean isPressed=false ;
    ArrayList<Integer> selectedItems;
    ArrayList<String> selectedTemplatedIds;

    public SendNotiAdapter(Activity context, ArrayList<Notificattion> names) {
        super(context, R.layout.notificationrow, names);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.names = names;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();
    }

    static class ViewHolder {
        public TextView tv_articleTitle;
        public Button checkBox1;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.notificationrow, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_articleTitle = (TextView) rowView.findViewById(R.id.code);
            viewHolder.checkBox1= (Button)rowView.findViewById(R.id.checkBox1);


            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_articleTitle.setText(names.get(position).getName());

//        if (selectedItems.contains(new Integer(position))) {
//            holder.checkBox1.setBackgroundColor(Color.BLUE);
//            String id=  names.get(position).getCode().toString();
//          //  Toast.makeText(getContext(), id, Toast.LENGTH_LONG).show();
//
//            SharedPreferences userCountrNameSharedPref=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
//            editorUserCountryName.putString("TemplateId", id);
//            editorUserCountryName.commit();
//
//            String templateId=      userCountrNameSharedPref.getString("TemplateId","NA");
//
//            Toast.makeText(getContext(), templateId, Toast.LENGTH_LONG).show();
//
//
//        } else {
//            holder.checkBox1.setBackgroundColor(Color.GRAY);
//
//            SharedPreferences userCountrNameSharedPref=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
//            editorUserCountryName.remove("|TemplateId");
//            editorUserCountryName.commit();
//
//            String templateId=userCountrNameSharedPref.getString("TemplateId","NA");
//
//          //  Toast.makeText(getContext(), templateId, Toast.LENGTH_LONG).show();
//
//
//
//
//        }

        holder.tv_articleTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               Integer pos= new Integer(position);
                if (selectedItems.contains(new Integer(position)))
                {
                    selectedItems.remove(new Integer(position));
                    notifyDataSetChanged();
                } else {
                    selectedItems.add(new Integer(position));
                    notifyDataSetChanged();
                }


                if (selectedItems.contains(new Integer(position)))
                {
                  //  holder.checkBox1.setBackgroundColor(Color.BLUE);
                    holder.checkBox1.setBackgroundResource(R.drawable.ldpi);

                    holder.tv_articleTitle.setTextColor(Color.BLACK);
                    String id=  names.get(position).getCode().toString();

                    selectedTemplatedIds.add(id);
                    SharedPreferences templateIdPrefAl=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editemplateIdPrefAl= templateIdPrefAl.edit();

                    Set<String> set = new HashSet<String>();
                    set.addAll(selectedTemplatedIds);
                    editemplateIdPrefAl.putStringSet("TemplateId", set);
                    editemplateIdPrefAl.commit();




//                    SharedPreferences userCountrNameSharedPref=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
//                    editorUserCountryName.putString("TemplateId", id);
//                    editorUserCountryName.commit();

                    Set<String> set_ = new HashSet<String>();
                    set_ = templateIdPrefAl.getStringSet("TemplateId", null);

                   // String templateId=      userCountrNameSharedPref.getString("TemplateId","NA");

                    Toast.makeText(getContext(), ""+set_, Toast.LENGTH_LONG).show();


                }
                else {
                   // holder.checkBox1.setBackgroundColor(Color.GRAY);
                    holder.checkBox1.setBackgroundResource(R.drawable.ldpi_);

                    holder.tv_articleTitle.setTextColor(Color.GRAY);
                    String id=  names.get(position).getCode().toString();
try {
    //selectedTemplatedIds.remove(position);


    SharedPreferences templateIdPrefAl = getContext().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
    SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

    Set<String> set = new HashSet<String>();
    set.addAll(selectedTemplatedIds);
    editemplateIdPrefAl.putStringSet("TemplateId", set);
    editemplateIdPrefAl.commit();


//                    SharedPreferences userCountrNameSharedPref=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
//                    editorUserCountryName.remove("TemplateId");
//                    editorUserCountryName.commit();


    Set<String> set_ = new HashSet<String>();
    set_ = templateIdPrefAl.getStringSet("TemplateId", null);
    Toast.makeText(getContext(), "" + set_, Toast.LENGTH_LONG).show();
}
catch(IndexOutOfBoundsException ie)
                    {}

                }

            }
        });

        holder.checkBox1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/fontello.ttf");
//                holder.checkBox1.setTypeface(typeface);
                //String id=  names.get(position).getCode().toString();
                if (selectedItems.contains(new Integer(position)))
                {
                    selectedItems.remove(new Integer(position));
                    notifyDataSetChanged();
                } else {
                    selectedItems.add(new Integer(position));
                    notifyDataSetChanged();
                }


                if (selectedItems.contains(new Integer(position))) {
                   // holder.checkBox1.setBackgroundColor(Color.BLUE);
                    holder.checkBox1.setBackgroundResource(R.drawable.ldpi);

                    holder.tv_articleTitle.setTextColor(Color.BLACK);
                    String id=  names.get(position).getCode().toString();

                    selectedTemplatedIds.add(id);
                    SharedPreferences templateIdPrefAl=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editemplateIdPrefAl= templateIdPrefAl.edit();

                    Set<String> set = new HashSet<String>();
                    set.addAll(selectedTemplatedIds);
                    editemplateIdPrefAl.putStringSet("TemplateId", set);
                    editemplateIdPrefAl.commit();




//                    SharedPreferences userCountrNameSharedPref=getContext().  getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editorUserCountryName= userCountrNameSharedPref.edit();
//                    editorUserCountryName.putString("TemplateId", id);
//                    editorUserCountryName.commit();

                    Set<String> set_ = new HashSet<String>();
                    set_ = templateIdPrefAl.getStringSet("TemplateId", null);

                    // String templateId=      userCountrNameSharedPref.getString("TemplateId","NA");

                    //Toast.makeText(getContext(), ""+set_, Toast.LENGTH_LONG).show();


                } else {
                    //holder.checkBox1.setBackgroundColor(Color.GRAY);
                    holder.checkBox1.setBackgroundResource(R.drawable.ldpi_);


                    holder.tv_articleTitle.setTextColor(Color.GRAY);


                    String id=  names.get(position).getCode().toString();
                try {
                        //selectedTemplatedIds.remove(position);


                SharedPreferences templateIdPrefAl = getContext().getSharedPreferences("TemplateId", Context.MODE_PRIVATE);
                     SharedPreferences.Editor editemplateIdPrefAl = templateIdPrefAl.edit();

                       Set<String> set = new HashSet<String>();
                     set.addAll(selectedTemplatedIds);
                       editemplateIdPrefAl.putStringSet("TemplateId", set);
                          editemplateIdPrefAl.commit();




                              Set<String> set_ = new HashSet<String>();
                                set_ = templateIdPrefAl.getStringSet("TemplateId", null);


                              }
                          catch (IllegalArgumentException r)
                        {}
                }

            }

            //}
        });


        return rowView;

    }





}



