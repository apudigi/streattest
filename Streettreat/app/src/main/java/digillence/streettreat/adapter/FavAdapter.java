package digillence.streettreat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import digillence.streettreat.R;
import digillence.streettreat.model.FavModel;

/**
 * Created by devashree on 5/1/17.
 */
public class FavAdapter extends ArrayAdapter <FavModel> {

    private final Activity context;
    private final ArrayList<FavModel> names;
    Boolean isPressed=false ;
    ArrayList<Integer> selectedItems;
    ArrayList<String> selectedTemplatedIds;


    public FavAdapter(Activity context, ArrayList<FavModel> names) {
        super(context, R.layout.notificationrow, names);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.names = names;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();
    }

    static class ViewHolder {
        public TextView tv_articleTitle;
        public ImageView imageView;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.row_fav, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_articleTitle = (TextView) rowView.findViewById(R.id.code);
            viewHolder.imageView = (ImageView) rowView.findViewById(R.id.imageView);
            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_articleTitle.setText(names.get(position).getName());
        holder.imageView = (ImageView) rowView.findViewById(R.id.imageView) ;
        Picasso.with(context).load(names.get(position).getIamge()).resize(60, 60).into(holder.imageView);
        return rowView;
    }

}
