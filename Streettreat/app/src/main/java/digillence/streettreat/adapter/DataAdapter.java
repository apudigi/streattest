package digillence.streettreat.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import digillence.streettreat.R;
import digillence.streettreat.fragment.NotificationListFragment;
import digillence.streettreat.model.NotificationsGet;
import digillence.streettreat.util.OnLoadMoreListener;

public class DataAdapter extends RecyclerView.Adapter {
	private final int VIEW_ITEM = 1;
	private final int VIEW_PROG = 0;
JSONArray jsonNoti;
	private ArrayList<NotificationsGet> sendPrevious;

	//private final ArrayList<NotificationsGet> names;


	// The minimum amount of items to have below your current scroll position
	// before loading more.
	private int visibleThreshold = 5;
	private int lastVisibleItem, totalItemCount;
	private boolean loading;
	private OnLoadMoreListener onLoadMoreListener;
	Context context;
	NotificationListFragment frag;

	public DataAdapter(Context context)
	{
		this.context = context;
	}

	public DataAdapter(List<NotificationsGet> students, RecyclerView recyclerView,NotificationListFragment fragment)
	{
		sendPrevious = (ArrayList<NotificationsGet>) students;
		frag=fragment;

		if (recyclerView.getLayoutManager() instanceof LinearLayoutManager)
		{

			final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
					.getLayoutManager();


					recyclerView
					.addOnScrollListener(new RecyclerView.OnScrollListener() {
						@Override
						public void onScrolled(RecyclerView recyclerView,
											   int dx, int dy) {
							super.onScrolled(recyclerView, dx, dy);

							totalItemCount = linearLayoutManager.getItemCount();
							lastVisibleItem = linearLayoutManager
									.findLastVisibleItemPosition();
							if (!loading
									&& totalItemCount <= (lastVisibleItem + visibleThreshold)) {
								// End has been reached
								// Do something
								if (onLoadMoreListener != null) {
									onLoadMoreListener.onLoadMore();
								}
								loading = true;
							}
						}
					});
		}
	}

	@Override
	public int getItemViewType(int position) {
		return sendPrevious.get(position) != null ? VIEW_ITEM : VIEW_PROG;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
			int viewType) {
		RecyclerView.ViewHolder vh;
		if (viewType == VIEW_ITEM) {
			View v = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.notificationlistrow, parent, false);

			vh = new StudentViewHolder(v);
		} else {
			View v = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.progress_item, parent, false);

			vh = new ProgressViewHolder(v);
		}
		return vh;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		if (holder instanceof StudentViewHolder)
		{

			NotificationsGet singleStudent= (NotificationsGet) sendPrevious.get(position);

//
//			sendPrevious.get(sendPrevious.size() - 1);
			String d=String.valueOf(sendPrevious.size());
			String e=String.valueOf(position);


			if (d.equals(e))
			{
			//frag.webserviceCall();
			}


			((StudentViewHolder) holder).tvName.setText(singleStudent.getName());

			String date= sendPrevious.get(position).getsendDate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date testDate = null;
			try {
				testDate = sdf.parse(date);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:a");
			String newFormat  = formatter.format(testDate);
			System.out.println(".....Date..." + newFormat);

			String partsDate[] = newFormat.split("\\s+");
			for(int i=0; i<partsDate.length; i++)
			{
				//	Log.e("article partsDate", partsDate[i]);
			}

			String dated = partsDate[0];//2016-12-12
			String time = partsDate[1];

			((StudentViewHolder) holder).tvEmailId.setText(dated);
			((StudentViewHolder) holder).time_.setText(time);
			
			((StudentViewHolder) holder).student= singleStudent;
			
		} else {
			((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

			frag.webserviceCall();
		}
	}


	public void setLoaded() {
		loading = false;
	}

	@Override
	public int getItemCount() {
		return sendPrevious.size();
	}

	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}



	public static class StudentViewHolder extends RecyclerView.ViewHolder {
		public TextView tvName;
		
		public TextView tvEmailId,time_;
		
		public NotificationsGet student;

		public StudentViewHolder(View v) {
			super(v);
			tvName = (TextView) v.findViewById(R.id.code);
			
			tvEmailId = (TextView) v.findViewById(R.id.dateandtime);

		time_= (TextView)v.findViewById(R.id.time_);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
//					st.makeText(v.getContext(),
//							"OnClToaick :" + student.getName() + " \n " + student.getName(),
//							Toast.LENGTH_SHORT).show();
					
				}
			});
		}
	}

	public static class ProgressViewHolder extends RecyclerView.ViewHolder {
		public ProgressBar progressBar;

		public ProgressViewHolder(View v) {

			super(v);
			progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		}
	}

}