package digillence.streettreat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import digillence.streettreat.R;
import digillence.streettreat.model.Notificattion;

public class SingleListAdapter extends BaseAdapter{
	Context context;
	LayoutInflater inflter;
	public static ArrayList<String> selectedAnswers;
	private final ArrayList<Notificattion> names_;


	public SingleListAdapter(Context applicationContext, ArrayList<Notificattion> names_) {

		this.context = context;
		this.names_ = names_;
		selectedAnswers = new ArrayList<>();
		for (int i = 0; i < names_.size(); i++) {
			selectedAnswers.add("Not Attempted");
		}
		inflter = (LayoutInflater.from(applicationContext));
	}

	@Override
	public int getCount() {
		return names_.size();
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}
	int selectedIndex = -1;

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		convertView = inflter.inflate(R.layout.content_radio_test, null);
		RadioButton rbSelect = (RadioButton) convertView
				.findViewById(R.id.yes);
		TextView question = (TextView) convertView.findViewById(R.id.question);
		if(selectedIndex == position){
			rbSelect.setChecked(true);
		}
		else{
			rbSelect.setChecked(false);
		}

		question.setText(names_.get(position).getName());
		return convertView;
	}
	public void setSelectedIndex(int index){
		selectedIndex = index;
	}

}