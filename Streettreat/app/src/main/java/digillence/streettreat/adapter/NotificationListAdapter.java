package digillence.streettreat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import digillence.streettreat.R;
import digillence.streettreat.model.NotificationsGet;

/**
 * Created by nayan on 28/12/16.
 */
public class NotificationListAdapter extends ArrayAdapter<NotificationsGet>
{
    private final Activity context;
    private final ArrayList<NotificationsGet> names;
    Boolean isPressed=false ;
    ArrayList<Integer> selectedItems;
    ArrayList<String> selectedTemplatedIds;

    public NotificationListAdapter(Activity context, ArrayList<NotificationsGet> names) {
        super(context, R.layout.notificationlistrow, names);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.names = names;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();
    }

    static class ViewHolder {
        public TextView tv_articleTitle,tv_DatTime,time_;



    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.notificationlistrow, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_articleTitle = (TextView) rowView.findViewById(R.id.code);
           viewHolder.tv_DatTime=(TextView)rowView.findViewById(R.id.dateandtime);
            viewHolder.time_= (TextView)rowView.findViewById(R.id.time_);


            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_articleTitle.setText(names.get(position).getName());
       String date= names.get(position).getsendDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:a");
        String newFormat  = formatter.format(testDate);
        System.out.println(".....Date..." + newFormat);

        String partsDate[] = newFormat.split("\\s+");
        for(int i=0; i<partsDate.length; i++)
        {
            //	Log.e("article partsDate", partsDate[i]);
        }

        String dated = partsDate[0];//2016-12-12
        String time = partsDate[1];

        holder.tv_DatTime.setText(dated);
        holder.  time_.setText(time);
        holder.tv_articleTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedItems.contains(new Integer(position))) {
                    selectedItems.remove(new Integer(position));
                    notifyDataSetChanged();
                } else {
                    selectedItems.add(new Integer(position));
                    notifyDataSetChanged();
                }

                if (selectedItems.contains(new Integer(position))) {

                } else {

                }

            }
        });
        return rowView;

    }

}



