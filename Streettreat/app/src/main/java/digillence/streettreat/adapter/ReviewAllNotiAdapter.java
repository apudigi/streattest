package digillence.streettreat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import digillence.streettreat.R;
import digillence.streettreat.model.Reviews;

/**
 * Created by devashree on 30/12/16.
 */
public class ReviewAllNotiAdapter extends ArrayAdapter<Reviews> {

    private final Activity context;
    private final ArrayList<Reviews> names;
    Boolean isPressed=false ;
    ArrayList<Integer> selectedItems;
    ArrayList<String> selectedTemplatedIds;


    public ReviewAllNotiAdapter(Activity context, ArrayList<Reviews> names) {
        super(context, R.layout.notificationrow_, names);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.names = names;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();
    }

    static class ViewHolder {
        public TextView tv_articleTitle ,post_Reply;
        public LinearLayout POST_Layout;
        public EditText postComment;
        public Button post_btn;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.notificationrow_, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_articleTitle = (TextView) rowView.findViewById(R.id.code);
            viewHolder.post_Reply = (TextView) rowView.findViewById(R.id.post_Reply);
            viewHolder.POST_Layout = (LinearLayout) rowView.findViewById(R.id.POST_Layout);
            viewHolder.postComment= (EditText)rowView.findViewById(R.id.postComment);
            viewHolder.post_btn=(Button)rowView.findViewById(R.id.post_btn);

            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_articleTitle.setText(names.get(position).getName());

        holder.post_Reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.POST_Layout.setVisibility(convertView.VISIBLE);
            }
        });

        holder.post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          String comment=      holder.postComment.getText().toString();
                Toast.makeText(getContext(),comment,Toast.LENGTH_SHORT).show();

            }
        });

        return rowView;
    }
}
