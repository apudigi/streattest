package digillence.streettreat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import digillence.streettreat.R;
import digillence.streettreat.model.NotificationsGet;

/**
 * Created by nayan on 28/12/16.
 */
public class SendPrevNotiAdapter extends ArrayAdapter<NotificationsGet>
{
    private final Activity context;
    private final ArrayList<NotificationsGet> names;
    Boolean isPressed=false ;
    ArrayList<Integer> selectedItems;
    ArrayList<String> selectedTemplatedIds;

    public SendPrevNotiAdapter(Activity context, ArrayList<NotificationsGet> names) {
        super(context, R.layout.notificationrow, names);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.names = names;
        selectedItems = new ArrayList<Integer>();
        selectedTemplatedIds= new ArrayList<String>();
    }

    static class ViewHolder {
        public TextView tv_articleTitle;


    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.prevnotificationrow, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_articleTitle = (TextView) rowView.findViewById(R.id.precode);
//            viewHolder.checkBox1= (Button)rowView.findViewById(R.id.checkBox1);


            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_articleTitle.setText(names.get(position).getName());



        holder.tv_articleTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (selectedItems.contains(new Integer(position)))
                {
                    selectedItems.remove(new Integer(position));
                    notifyDataSetChanged();
                } else {
                    selectedItems.add(new Integer(position));
                    notifyDataSetChanged();
                }


                if (selectedItems.contains(new Integer(position)))
                {

                  //  holder.tv_articleTitle.setTextColor(Color.BLACK);

                } else {
                    //holder.checkBox1.setBackgroundColor(Color.GRAY);
                  //  holder.tv_articleTitle.setTextColor(Color.GRAY);

                }

            }
        });




        return rowView;

    }





}



